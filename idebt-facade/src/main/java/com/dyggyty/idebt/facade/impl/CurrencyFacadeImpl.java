package com.dyggyty.idebt.facade.impl;

import com.dyggyty.idebt.bd.CurrencyBd;
import com.dyggyty.idebt.facade.CurrencyFacade;
import com.dyggyty.idebt.model.Currency;

import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 10.10.12
 * Time: 9:53
 */
public class CurrencyFacadeImpl implements CurrencyFacade {

    private CurrencyBd currencyBd;

    public void setCurrencyBd(CurrencyBd currencyBd) {
        this.currencyBd = currencyBd;
    }

    @Override
    public List<Currency> getAll() {
        return currencyBd.getAll();
    }
}
