package com.dyggyty.idebt.facade;

import com.dyggyty.idebt.model.Currency;

import java.util.List;

/**
 * User: mobius
 * Date: 09.10.12
 * Time: 15:01
 */
public interface CurrencyFacade {

    /**
     * Retrieve list of currencies.
     *
     * @return List of currencies.
     */
    public List<Currency> getAll();
}
