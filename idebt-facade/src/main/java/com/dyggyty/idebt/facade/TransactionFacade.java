package com.dyggyty.idebt.facade;

import com.dyggyty.idebt.model.Debt;
import com.dyggyty.idebt.model.Transaction;

import java.util.Date;
import java.util.List;

/**
 * User: mobius
 * Date: 10.08.12
 * Time: 14:07
 */
public interface TransactionFacade {

    /**
     * Create new transaction.
     *
     * @param userSession    Transaction owner session.
     * @param targetPersonId Target person database identifier.
     * @param amount         Transaction amount value.
     * @param currencyCode   Transaction currency.
     * @param finalDate      Terms limitation.
     * @param comment        Transaction comment string
     */
    public void addTransaction(String userSession, Long targetPersonId, Double amount, String currencyCode,
                               Date finalDate, String comment);

    /**
     * Get user transactions.
     *
     * @param userSession Transaction owner session.
     * @return Transactions list.
     */
    public List<Transaction> getTransactions(String userSession);

    /**
     * Get user transaction.
     *
     * @param userSession   Transaction owner session.
     * @param transactionId Transaction database identifier.
     * @return Status of the operation.
     */
    public Transaction getTransaction(String userSession, Long transactionId);

    /**
     * Get user debts.
     *
     * @param userSession Debts owner session.
     * @return List of debts.
     */
    public List<Debt> getDebts(String userSession);

    /**
     * Confirm transaction.
     *
     * @param confirmationCode Transaction confirmation code.
     */
    public void confirmTransaction(String confirmationCode);

    /**
     * Confirm transaction.
     *
     * @param personSession Transaction target person session identifier.
     * @param transactionId Transaction database identifier.
     */
    public void confirmTransaction(String personSession, Long transactionId);

    /**
     * Delete user transaction
     *
     * @param userSession   Transaction owner user session.
     * @param transactionId Transaction database identifier.
     */
    public void deleteTransaction(String userSession, Long transactionId);

    /**
     * Update user transaction.
     *
     * @param userSession    Transaction owner session.
     * @param transactionId  Transaction database identifier.
     * @param targetPersonId Target person database identifier.
     * @param amount         Transaction amount value.
     * @param currencyCode   Transaction currency.
     * @param finalDate      Terms limitation.
     * @param comment        Transaction comment string
     */
    public void updateTransaction(String userSession, Long transactionId, Long targetPersonId, Double amount,
                                  String currencyCode, Date finalDate, String comment);
}
