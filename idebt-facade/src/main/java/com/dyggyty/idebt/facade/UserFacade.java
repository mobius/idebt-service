package com.dyggyty.idebt.facade;

import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.User;

import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 23.07.12
 * Time: 21:59
 */
public interface UserFacade {

    /**
     * Login user.
     *
     * @param userEmail User name
     * @param password Unencrypted user password.
     * @return User session.
     */
    public String loginUser(String userEmail, String password);

    /**
     * Creates new user.
     *
     * @param password  Unencrypted user password.
     * @param userEmail User email.
     * @param nickName  User nickName.
     * @param phone     User phone.
     */
    public void registerUser(String userEmail, String password, String nickName, String phone);

    /**
     * Check if user exists.
     *
     * @param userName User name
     * @return TRUE if user exists anf FALSE otherwise.
     */
    public Boolean checkUserExists(String userName);

    /**
     * Retrieve user contacts by user session.
     *
     * @param userSession User session identifier.
     * @return List of contacts.
     */
    public List<PersonContact> getUserContacts(String userSession);

    /**
     * Change user information
     *
     * @param userSession User session ID.
     * @param nickName    User new nick name
     * @param email       User email information.
     * @param phone       User phone information.
     */
    public void changeUserInfo(String userSession, String nickName, String email, String phone);

    /**
     * Change user password.
     *
     * @param userSession      Current user session.
     * @param confirmationCode Confirmation code.
     * @param oldPassword      Real user password.
     * @param newPassword      New user password.
     */
    public void changeUserPassword(String userSession, String confirmationCode, String oldPassword, String newPassword);

    /**
     * Confirm user email contact.
     *
     * @param confirmationCode Email confirmation code.
     */
    public void confirmEmail(String confirmationCode);

    /**
     * Retrieve current user.
     *
     * @param userSession Current user session.
     * @return User entity.
     */
    public User getCurrentUser(String userSession);

    /**
     * Delete current user.
     *
     * @param userSession Current user session.
     */
    public void deleteCurrentUser(String userSession);

    /**
     * Generates message to change password.
     *
     * @param email Email,
     */
    public void forgotUserPassword(String email);
}
