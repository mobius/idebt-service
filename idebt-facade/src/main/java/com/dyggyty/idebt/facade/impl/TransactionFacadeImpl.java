package com.dyggyty.idebt.facade.impl;

import com.dyggyty.idebt.bd.CurrencyBd;
import com.dyggyty.idebt.bd.DebtBd;
import com.dyggyty.idebt.bd.MailBd;
import com.dyggyty.idebt.bd.PersonContactBd;
import com.dyggyty.idebt.bd.TransactionBd;
import com.dyggyty.idebt.bd.UserBd;
import com.dyggyty.idebt.facade.TransactionFacade;
import com.dyggyty.idebt.model.Currency;
import com.dyggyty.idebt.model.Debt;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Transaction;
import com.dyggyty.idebt.model.User;
import com.dyggyty.idebt.utils.ErrorMessages;
import com.dyggyty.idebt.utils.MessageException;

import java.util.Date;
import java.util.List;

/**
 * User: mobius
 * Date: 10.08.12
 * Time: 14:16
 */
public class TransactionFacadeImpl implements TransactionFacade {

    private TransactionBd transactionBd;
    private CurrencyBd currencyBd;
    private UserBd userBd;
    private PersonContactBd personContactBd;
    private DebtBd debtBd;
    //private MailBd mailBd;

    @Override
    public void addTransaction(String userSession, Long targetPersonId, Double amount, String currencyCode,
                               Date finalDate, String comment) {

        User ownerUser = userBd.getUserBySession(userSession);
        if (ownerUser == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        PersonContact personContact = personContactBd.getById(targetPersonId);
        if (personContact == null) {
            throw new MessageException(ErrorMessages.CONTACT_NOT_FOUND);
        }

        Currency currency = currencyBd.getByCode(currencyCode);
        if (currencyCode == null) {
            throw new MessageException(ErrorMessages.CURRENCY_IS_MISSING);
        }

        Transaction transaction = new Transaction();

        transaction.setAmount(amount);
        transaction.setVerified(Boolean.FALSE);
        transaction.setCreated(new Date());
        transaction.setCurrency(currency);
        transaction.setFinalDate(finalDate);
        transaction.setOwner(ownerUser);
        transaction.setPersonContact(personContact);
        transaction.setComment(comment);

        transactionBd.createTransaction(transaction);

        //mailBd.sendTransactionConfirmationMessage(transaction);
    }

    @Override
    public List<Transaction> getTransactions(String userSession) {
        User user = userBd.getUserBySession(userSession);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        return transactionBd.getUserTransactions(userSession);
    }

    @Override
    public Transaction getTransaction(String userSession, Long transactionId) {
        User user = userBd.getUserBySession(userSession);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        return transactionBd.getTransaction(userSession, transactionId);
    }

    @Override
    public List<Debt> getDebts(String userSession) {
        User user = userBd.getUserBySession(userSession);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        return debtBd.getUserDebts(userSession);
    }

    @Override
    public void confirmTransaction(String confirmationCode) {
        transactionBd.confirmTransaction(confirmationCode);
    }

    @Override
    public void confirmTransaction(String personSession, Long transactionId) {
        User user = userBd.getUserBySession(personSession);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        transactionBd.confirmTransaction(personSession, transactionId);
    }

    @Override
    public void deleteTransaction(String userSession, Long transactionId) {
        User user = userBd.getUserBySession(userSession);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        transactionBd.deleteTransaction(userSession, transactionId);
    }

    @Override
    public void updateTransaction(String userSession, Long transactionId, Long targetPersonId, Double amount,
                                  String currencyCode, Date finalDate, String comment) {
        User user = userBd.getUserBySession(userSession);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        transactionBd.
                updateTransaction(userSession, transactionId, targetPersonId, amount, currencyCode, finalDate, comment);
        //mailBd.sendTransactionConfirmationMessage(transactionId);
    }

    public void setTransactionBd(TransactionBd transactionBd) {
        this.transactionBd = transactionBd;
    }

    public void setUserBd(UserBd userBd) {
        this.userBd = userBd;
    }

    public void setPersonContactBd(PersonContactBd personContactBd) {
        this.personContactBd = personContactBd;
    }

    public void setCurrencyBd(CurrencyBd currencyBd) {
        this.currencyBd = currencyBd;
    }

    public void setDebtBd(DebtBd debtBd) {
        this.debtBd = debtBd;
    }

    public void setMailBd(MailBd mailBd) {
        //this.mailBd = mailBd;
    }
}
