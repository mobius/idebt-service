package com.dyggyty.idebt.facade.impl;

import com.dyggyty.idebt.bd.PersonContactBd;
import com.dyggyty.idebt.bd.UserBd;
import com.dyggyty.idebt.facade.PersonContactFacade;
import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Phone;
import com.dyggyty.idebt.model.User;
import com.dyggyty.idebt.utils.ErrorMessages;
import com.dyggyty.idebt.utils.MessageException;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * User: vitaly.rudenya
 * Date: 09.10.12
 * Time: 2:05
 */
public class PersonContactFacadeImpl implements PersonContactFacade {

    private PersonContactBd personContactBd;
    private UserBd userBd;

    public void setPersonContactBd(PersonContactBd personContactBd) {
        this.personContactBd = personContactBd;
    }

    public void setUserBd(UserBd userBd) {
        this.userBd = userBd;
    }

    @Transactional
    @Override
    public Long saveOrUpdatePersonContact(String session, Long personId, String personName, String personEmail,
                                          String personPhone, String contactExternalId) {

        User user = userBd.getUserBySession(session);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        PersonContact personContact = null;

        if (personId != null) {
            personContact = personContactBd.getBySessionAndId(session, personId);

            if (personContact == null) {
                throw new MessageException(ErrorMessages.USER_CONTACT_IS_MISSING);
            }
        }

        if (personContact == null) {

            User ownerUser = userBd.getUserBySession(session);
            if (ownerUser == null) {
                throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
            }

            personContact = new PersonContact();
            personContact.setName(personName);
            personContact.setOwner(ownerUser);
            personContact.setExternalId(contactExternalId);

            if (personPhone != null && personPhone.trim().length() > 0) {
                Phone phone = new Phone();
                phone.setContactInfo(personPhone);
                phone.setPerson(personContact);
                phone.setVerified(Boolean.FALSE);

                Set<Phone> phones = new HashSet<Phone>();
                phones.add(phone);

                personContact.setPhones(phones);
            }

            if (personEmail != null && personEmail.trim().length() > 0) {
                Email email = new Email();
                email.setContactInfo(personEmail);
                email.setPerson(personContact);
                email.setVerified(Boolean.FALSE);

                Set<Email> emails = new HashSet<Email>();
                emails.add(email);

                personContact.setEmails(emails);
            }

            personContactBd.savePersonContact(personContact);
        } else {
            personContact.setName(personName);
            personContact.setExternalId(contactExternalId);

            Set<Email> emails = personContact.getEmails();
            if (personEmail != null && personEmail.trim().length() > 0) {
                if (emails == null || emails.size() == 0) {
                    emails = new HashSet<Email>();

                    Email email = new Email();
                    email.setContactInfo(personEmail);
                    email.setPerson(personContact);
                    email.setVerified(Boolean.FALSE);

                    emails.add(email);

                    personContact.setEmails(emails);
                } else {
                    emails.iterator().next().setContactInfo(personEmail);
                }
            } else if (emails != null) {
                for (Email email : emails) {
                    personContactBd.deleteEmail(email);
                }
                emails.clear();
            }

            Set<Phone> phones = personContact.getPhones();
            if (personPhone != null && personPhone.trim().length() > 0) {
                if (phones == null || phones.size() == 0) {
                    phones = new HashSet<Phone>();

                    Phone phone = new Phone();
                    phone.setContactInfo(personPhone);
                    phone.setPerson(personContact);
                    phone.setVerified(Boolean.FALSE);

                    phones.add(phone);

                    personContact.setPhones(phones);
                } else {
                    phones.iterator().next().setContactInfo(personPhone);
                }
            } else if (phones != null) {
                for (Phone phone : phones) {
                    personContactBd.deletePhone(phone);
                }
                phones.clear();
            }

            personContactBd.savePersonContact(personContact);
        }

        return personContact.getId();
    }

    @Transactional
    @Override
    public void deletePerson(String session, Long personId) {
        User user = userBd.getUserBySession(session);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        PersonContact contactToDelete = personContactBd.getBySessionAndId(session, personId);

        if (contactToDelete == null) {
            throw new MessageException(ErrorMessages.USER_CONTACT_IS_MISSING);
        }

        //todo we can't to delete contact with debts

        contactToDelete.setDeleted(Boolean.TRUE);
        personContactBd.savePersonContact(contactToDelete);
    }

    @Override
    public PersonContact getPersonContact(String session, Long personContactId) {
        User user = userBd.getUserBySession(session);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        return personContactBd.getBySessionAndId(session, personContactId);
    }
}
