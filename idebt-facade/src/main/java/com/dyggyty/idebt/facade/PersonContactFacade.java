package com.dyggyty.idebt.facade;

import com.dyggyty.idebt.model.PersonContact;

/**
 * User: vitaly.rudenya
 * Date: 09.10.12
 * Time: 2:14
 */
public interface PersonContactFacade {

    /**
     * Create new or update existing one Person object.
     *
     * @param session           Current user session.
     * @param personId          Person database identifier.
     * @param personName        Person real name.
     * @param personEmail       Person email.
     * @param personPhone       Person contact phone.
     * @param contactExternalId Contact external identifier.
     * @return Person's database identifier.
     */
    public Long saveOrUpdatePersonContact(String session, Long personId, String personName, String personEmail,
                                          String personPhone, String contactExternalId);

    /**
     * Delete user contact
     *
     * @param session  Current user session
     * @param personId ID of person to delete.
     */
    public void deletePerson(String session, Long personId);

    /**
     * Retrieve person contact by ID.
     *
     * @param session         Current user session.
     * @param personContactId Contact ID.
     * @return Person contact instance.
     */
    public PersonContact getPersonContact(String session, Long personContactId);
}
