package com.dyggyty.idebt.facade.impl;

import com.dyggyty.idebt.bd.MailBd;
import com.dyggyty.idebt.bd.PersonContactBd;
import com.dyggyty.idebt.bd.UserBd;
import com.dyggyty.idebt.facade.UserFacade;
import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Phone;
import com.dyggyty.idebt.model.User;
import com.dyggyty.idebt.utils.ErrorMessages;
import com.dyggyty.idebt.utils.MessageException;
import org.mindrot.jbcrypt.BCrypt;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: vitaly.rudenya
 * Date: 23.07.12
 * Time: 22:11
 */
public class UserFacadeImpl implements UserFacade {

    private UserBd userBd;
    private PersonContactBd personContactBd;
    private MailBd mailBd;

    public void setUserBd(UserBd userBd) {
        this.userBd = userBd;
    }

    public void setPersonContactBd(PersonContactBd personContactBd) {
        this.personContactBd = personContactBd;
    }

    public void setMailBd(MailBd mailBd) {
        this.mailBd = mailBd;
    }

    @Override
    public String loginUser(String userEmail, String password) {
        String salt = userBd.getUserSalt(userEmail);
        PwdDetails pwdDetails = encryptPassword(password, salt);
        return userBd.loginUser(userEmail, pwdDetails.getPwdHash());
    }

    @Override
    public void registerUser(String userEmail, String password, String nickName, String phoneNum) {
        PwdDetails pwdDetails = encryptPassword(password);

        User userEntity = new User();
        userEntity.setCreated(new Date());
        userEntity.setPassword(pwdDetails.getPwdHash());
        userEntity.setSalt(pwdDetails.getSalt());
        userEntity.setUsername(userEmail);
        userEntity.setName(nickName);
        userEntity.setVerified(Boolean.FALSE);

        Email emailToConfirm = null;
        if (userEmail != null && userEmail.trim().length() > 0) {
            Set<Email> emails = new HashSet<Email>();
            Email email = new Email();
            email.setContactInfo(userEmail);
            email.setPerson(userEntity);
            email.setVerified(Boolean.FALSE);
            emails.add(email);

            userEntity.setEmails(emails);

            emailToConfirm = email;
        }

        if (phoneNum != null && phoneNum.trim().length() > 0) {
            Set<Phone> phones = new HashSet<Phone>();
            Phone phone = new Phone();
            phone.setContactInfo(phoneNum);
            phone.setPerson(userEntity);
            phone.setVerified(Boolean.FALSE);
            phones.add(phone);

            userEntity.setPhones(phones);
        }

        userBd.registerUser(userEntity);

        if (emailToConfirm != null) {
            //mailBd.sendEmailConfirmationMessage(emailToConfirm);
        }
    }

    @Override
    public Boolean checkUserExists(String userName) {
        return userBd.checkUserExists(userName);
    }

    @Override
    public List<PersonContact> getUserContacts(String userSession) {
        User user = userBd.getUserBySession(userSession);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        return personContactBd.getUserContacts(userSession);
    }

    @Override
    public void changeUserInfo(String userSession, String nickName, String userEmail, String userPhone) {
        User user = userBd.getUserBySession(userSession);
        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
        }

        user.setName(nickName);

        boolean sendVerification = true;

        Set<Email> emails = user.getEmails();
        if (userEmail != null && userEmail.trim().length() > 0) {
            if (emails == null) {
                emails = new HashSet<Email>();

                Email email = new Email();
                email.setContactInfo(userEmail);
                email.setPerson(user);
                email.setVerified(Boolean.FALSE);

                emails.add(email);

                user.setEmails(emails);
            } else {
                Email currentEmail = emails.iterator().next();
                sendVerification = !currentEmail.getContactInfo().equalsIgnoreCase(userEmail.trim());
                currentEmail.setContactInfo(userEmail);
            }
        } else if (emails != null) {
            emails.clear();
        }

        Set<Phone> phones = user.getPhones();
        if (userPhone != null && userPhone.trim().length() > 0) {
            if (phones == null) {
                phones = new HashSet<Phone>();

                Phone phone = new Phone();
                phone.setContactInfo(userPhone);
                phone.setPerson(user);
                phone.setVerified(Boolean.FALSE);

                phones.add(phone);

                user.setPhones(phones);
            } else {
                phones.iterator().next().setContactInfo(userPhone);
            }
        } else if (phones != null) {
            phones.clear();
        }

        userBd.saveUser(user);

        if (sendVerification && emails != null && emails.size() > 1) {
            //mailBd.sendEmailConfirmationMessage(emails.iterator().next());
        }
    }

    @Override
    public void changeUserPassword(String userSession, String confirmationCode, String oldPassword, String newPassword) {

        User user;
        if (confirmationCode == null || confirmationCode.trim().length() == 0) {
            user = userBd.getUserBySession(userSession);
            if (user == null) {
                throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
            }

            PwdDetails pwdDetails = encryptPassword(oldPassword, user.getSalt());
            if (!pwdDetails.getPwdHash().equals(user.getPassword())) {
                throw new MessageException(ErrorMessages.INVALID_USER_PASSWORD);
            }

            String salt = user.getSalt();
            pwdDetails = encryptPassword(oldPassword, salt);

            if (!pwdDetails.getPwdHash().equals(user.getPassword())) {
                throw new MessageException(ErrorMessages.INVALID_USER_PASSWORD);
            }
        } else {
            user = userBd.getUserByConfirmationKey(confirmationCode);
            if (user == null) {
                throw new MessageException(ErrorMessages.INVALID_USER_SESSION);
            }
        }

        PwdDetails pwdDetails = encryptPassword(newPassword);
        user.setPassword(pwdDetails.getPwdHash());
        user.setSalt(pwdDetails.getSalt());
        user.setVerificationCode(null);

        userBd.saveUser(user);
    }

    @Override
    public void confirmEmail(String confirmationCode) {
        userBd.confirmEmail(confirmationCode);
    }

    @Override
    public User getCurrentUser(String userSession) {
        return userBd.getUserBySession(userSession);
    }

    @Override
    public void deleteCurrentUser(String userSession) {
        userBd.deleteCurrentUser(userSession);
    }

    @Override
    public void forgotUserPassword(String email) {
        User user = userBd.getVerifiedUser(email);

        if (user == null) {
            throw new MessageException(ErrorMessages.INVALID_USER_LOGIN_AND_EMAIL);
        }

        mailBd.sendForgotPasswordMessage(user);
    }

    private static PwdDetails encryptPassword(String password) {
        return encryptPassword(password, null);
    }

    private static PwdDetails encryptPassword(String password, String salt) {
        if (password == null) {
            return new PwdDetails();
        }

        String pwdSalt;
        if (salt == null) {
            pwdSalt = BCrypt.gensalt();
        } else {
            pwdSalt = salt;
        }
        String hashed = BCrypt.hashpw(password, pwdSalt);

        return new PwdDetails(pwdSalt, hashed);
    }

    private static final class PwdDetails {
        private String salt;
        private String pwdHash;

        private PwdDetails() {
        }

        private PwdDetails(String salt, String pwdHash) {
            this.salt = salt;
            this.pwdHash = pwdHash;
        }

        public String getSalt() {
            return salt;
        }

        public String getPwdHash() {
            return pwdHash;
        }
    }
}
