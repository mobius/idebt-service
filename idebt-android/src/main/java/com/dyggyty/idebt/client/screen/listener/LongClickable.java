package com.dyggyty.idebt.client.screen.listener;

import java.util.Map;

/**
 * User: vitaly.rudenya
 * Date: 01.04.13
 * Time: 11:37
 */
public interface LongClickable {
    public void onLongItemClick(Map<String, ?> item);
}
