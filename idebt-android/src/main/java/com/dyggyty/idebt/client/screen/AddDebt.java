package com.dyggyty.idebt.client.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.dyggyty.idebt.client.Const;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.async.SQLiteDataProvider;
import com.dyggyty.idebt.client.contacts.Contact;
import com.dyggyty.idebt.client.model.Currency;
import com.dyggyty.idebt.client.screen.transaction.DatePickupFragmentActivity;
import com.dyggyty.idebt.utils.ErrorMessages;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class AddDebt extends BaseEditableActivity {

    public static final String CURRENCY_IND = "currencyInd";
    public static final String TRANSACTION_HAS_BEEN_SAVED = "message_transaction_has_been_saved";

    private Spinner currenciesSpinner;
    private EditText amountEdit;
    private RadioButton giveMoneyButton;
    private EditText userNameEdit;
    private Spinner currencyCodeSpinner;
    private TextView dueDateText;
    private ImageButton removeReturnDate;
    private Button addReturnDate;

    private Contact contact;
    private Date finalDate;
    private int selectedInd = 0;

    private SQLiteDataProvider dataProvider;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currenciesSpinner = (Spinner) findViewById(R.id.et_currencies);
        amountEdit = (EditText) findViewById(R.id.et_amount);
        giveMoneyButton = (RadioButton) findViewById(R.id.check_give_money);
        userNameEdit = (EditText) findViewById(R.id.et_username);
        currencyCodeSpinner = (Spinner) findViewById(R.id.et_currencies);
        dueDateText = (TextView) findViewById(R.id.due_date_text);
        removeReturnDate = (ImageButton) findViewById(R.id.remove_return_date);
        addReturnDate = (Button) findViewById(R.id.add_return_date);

        dataProvider = ActivityHelper.getDataProvider(getApplicationContext());

        initializeForm(getIntent().getExtras());
    }

    @Override
    protected int getContentView() {
        return R.layout.add_debt;
    }

    private void initializeForm(Bundle data) {
        setupCurrency(data);
        setupContact(data);
        setupReturnDate(data);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            int activityType = data.getIntExtra(Const.ACTIVITY_IND, -1);
            switch (activityType) {
                case Const.CONTACT_LIST_ACTIVITY: {
                    setupContact(data.getExtras());
                    break;
                }

                case Const.DEBT_DETAILS_DATE_PERIOD_ACTIVITY: {
                    setupReturnDate(data.getExtras());
                    break;
                }
            }
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        initializeForm(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        if (finalDate != null) {
            savedInstanceState.putSerializable(DatePickupFragmentActivity.FINISH_DATE, finalDate);
        }

        if (contact != null) {
            savedInstanceState.putSerializable(ContactListActivity.CONTACT, contact);
        }

        savedInstanceState.putInt(CURRENCY_IND, selectedInd);
    }

    private void setupCurrency(Bundle data) {

        List<Currency> currencies = dataProvider.getCurrencies();
        Collections.sort(currencies);

        if (data != null && data.containsKey(CURRENCY_IND)) {
            selectedInd = data.getInt(CURRENCY_IND);
        } else {
            int currentInd = 0;
            for (Currency currency : currencies) {
                if (currency.getUseDefault()) {
                    selectedInd = currentInd;
                    break;
                }
                currentInd++;
            }
        }

        ArrayAdapter<Currency> dataAdapter =
                new ArrayAdapter<Currency>(this, android.R.layout.simple_spinner_item, currencies);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currenciesSpinner.setAdapter(dataAdapter);
        currenciesSpinner.setSelection(selectedInd, true);
    }

    private void setupContact(Bundle data) {
        if (data != null && data.containsKey(ContactListActivity.CONTACT)) {
            contact = (Contact) data.getSerializable(ContactListActivity.CONTACT);
            userNameEdit.setText(contact.getDisplayName());
        }
    }

    private void setupReturnDate(Bundle data) {
        if (data != null && data.containsKey(DatePickupFragmentActivity.FINISH_DATE)) {
            finalDate = (Date) data.getSerializable(DatePickupFragmentActivity.FINISH_DATE);
        }

        if (finalDate != null) {
            addReturnDate.setVisibility(View.GONE);
            dueDateText.setText(ActivityHelper.OUT_SDF.format(finalDate));
            dueDateText.setVisibility(View.VISIBLE);
            removeReturnDate.setVisibility(View.VISIBLE);
        }
    }

    public void addDate(View view) {
        Intent i = new Intent(this, DatePickupFragmentActivity.class);
        startActivityForResult(i, Const.DEBT_DETAILS_DATE_PERIOD_ACTIVITY);
    }

    public void deleteReturnDate(View view) {
        finalDate = null;

        addReturnDate.setVisibility(View.VISIBLE);
        dueDateText.setText("");
        dueDateText.setVisibility(View.GONE);
        removeReturnDate.setVisibility(View.GONE);

        Intent data = getIntent();
        if (data.hasExtra(DatePickupFragmentActivity.FINISH_DATE)) {
            data.removeExtra(DatePickupFragmentActivity.FINISH_DATE);
        }
    }

    @Override
    protected String getSaveSuccessMessage() {
        return TRANSACTION_HAS_BEEN_SAVED;
    }

    @Override
    protected boolean saveEntity() {
        String amountStr = amountEdit.getText().toString();

        if (amountStr.trim().length() == 0) {
            ActivityHelper.showNotification(this, ErrorMessages.VALIDATION_EMPTY_AMOUNT);
            return false;
        }

        final Currency currency = (Currency) currencyCodeSpinner.getSelectedItem();
        Double amount;
        try {
            amount = Double.parseDouble(amountStr);
        } catch (NumberFormatException ex) {
            ActivityHelper.showNotification(this, amountStr + " " +
                    getResources().getString(R.string.message_is_not_number));
            return false;
        }

        if (amount < 0) {
            ActivityHelper.showNotification(this, ErrorMessages.VALIDATION_ZERO_AMOUNT);
            return false;
        }

        final Double amountToSave = giveMoneyButton.isChecked() ? amount : -amount;

        if (contact == null) {
            ActivityHelper.showNotification(this, ErrorMessages.USER_CONTACT_IS_MISSING);
            return false;
        }

        if (currency == null) {
            ActivityHelper.showNotification(this, ErrorMessages.CURRENCY_IS_MISSING);
            return false;
        }

        dataProvider.addTransaction(contact.getId(), amountToSave, currency.getId(), finalDate, "");

        return true;
    }

    public void onContactLookup(View view) {
        Intent i = new Intent(this, ContactListActivity.class);
        startActivityForResult(i, Const.CONTACT_LIST_ACTIVITY);
    }
}