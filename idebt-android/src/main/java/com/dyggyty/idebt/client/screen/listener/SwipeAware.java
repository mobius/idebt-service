package com.dyggyty.idebt.client.screen.listener;

import android.view.View;

/**
 * User: mobius
 * Date: 27.03.13
 * Time: 14:20
 */
public interface SwipeAware {

    public View getView();

    public void onRightToLeftSwipe();

    public void onLeftToRightSwipe();

    public void onTopToBottomSwipe();

    public void onBottomToTopSwipe();
}
