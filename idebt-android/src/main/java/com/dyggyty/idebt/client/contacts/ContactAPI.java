package com.dyggyty.idebt.client.contacts;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: mobius
 * Date: 04.03.13
 * Time: 15:03
 */
public class ContactAPI {
    private ContactList contactList;
    private ContentResolver cr;

    public ContactAPI(ContentResolver cr) {
        this.cr = cr;
    }

    public ContactList getContactList() {
        if (contactList == null) {
            synchronized (this) {
                if (contactList == null) {
                    contactList = newContactList();
                }
            }
        }
        return contactList.clone();
    }

    private ContactList newContactList() {
        ContactList contacts = new ContactList();
        String id;

        try {
            Cursor cur = this.cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                    ContactsContract.Contacts.IN_VISIBLE_GROUP + "=?", new String[]{"1"}, null);
            if (cur.getCount() > 0) {
                List<String> contactIds = new ArrayList<String>();
                while (!cur.isLast() && cur.moveToNext()) {
                    Contact c = new Contact();
                    id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    contactIds.add(id);
                    c.setAddressBookId(id);

                    String contactDisplayName = null;
                    int contactDisplayNameInd = cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                    if (!cur.isNull(contactDisplayNameInd)) {
                        contactDisplayName = cur.getString(contactDisplayNameInd);
                        if (contactDisplayName == null) {
                            contactDisplayNameInd = cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY);
                            if (!cur.isNull(contactDisplayNameInd)) {
                                contactDisplayName = cur.getString(contactDisplayNameInd);
                            }
                        }
                    }

                    if (contactDisplayName != null) {
                        c.setDisplayName(contactDisplayName);
                        contacts.addContact(c);
                    }
                }

                if (contactIds.size() > 0) {
                    Map<String, List<Phone>> phoneMap = getPhoneNumbers(contactIds);
                    Map<String, List<Email>> emailMap = getEmailAddresses(contactIds);

                    for (Contact contact : contacts.getContacts()) {
                        List<Phone> phones = phoneMap.get(contact.getAddressBookId());
                        contact.setPhone(phones);

                        List<Email> emails = emailMap.get(contact.getAddressBookId());
                        contact.setEmail(emails);
                    }

                }
            }
        } catch (Throwable tw) {
            Log.e(getClass().getName(), "Can't load contacts", tw);
        }
        return contacts;
    }

    private Map<String, List<Phone>> getPhoneNumbers(List<String> ids) {
        Map<String, List<Phone>> phones = new HashMap<String, List<Phone>>();

        Cursor pCur = this.cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " IN (" + getParams(ids.size()) + ")",
                ids.toArray(new String[ids.size()]), null);
        while (pCur.moveToNext()) {
            String contactId = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            List<Phone> phoneList = phones.get(contactId);
            if (phoneList == null) {
                phoneList = new ArrayList<Phone>();
                phones.put(contactId, phoneList);
            }

            phoneList.add(new Phone(
                    pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    , pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE))
            ));

        }
        pCur.close();
        return phones;
    }

    private Map<String, List<Email>> getEmailAddresses(List<String> ids) {
        Map<String, List<Email>> emails = new HashMap<String, List<Email>>();

        Cursor emailCur = this.cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " IN (" + getParams(ids.size()) + ")",
                ids.toArray(new String[ids.size()]), null);
        while (emailCur.moveToNext()) {
            String contactId =
                    emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
            List<Email> emailList = emails.get(contactId);
            if (emailList == null) {
                emailList = new ArrayList<Email>();
                emails.put(contactId, emailList);
            }
            // This would allow you get several email addresses
            Email e = new Email(emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                    , emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE))
            );
            emailList.add(e);
        }
        emailCur.close();
        return emails;
    }

    private static String getParams(int count) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < count; i++) {
            if (result.length() > 0) {
                result.append(", ");
            }

            result.append("?");
        }

        return result.toString();
    }
}
