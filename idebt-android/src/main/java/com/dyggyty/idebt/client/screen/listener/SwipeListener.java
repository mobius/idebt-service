package com.dyggyty.idebt.client.screen.listener;

import android.view.MotionEvent;
import android.view.View;

/**
 * User: mobius
 * Date: 27.03.13
 * Time: 14:16
 */
public class SwipeListener implements View.OnTouchListener {

    static final int MIN_DISTANCE = 100;
    private float downX, downY, upX, upY;
    private SwipeAware swipeAware;

    public SwipeListener(SwipeAware swipeAware) {
        this.swipeAware = swipeAware;
        swipeAware.getView().setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                downX = event.getX();
                downY = event.getY();
                return true;
            }
            case MotionEvent.ACTION_UP: {
                upX = event.getX();
                upY = event.getY();

                float deltaX = downX - upX;
                float deltaY = downY - upY;

                // listener horizontal?
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    // left or right
                    if (deltaX < 0) {
                        swipeAware.onLeftToRightSwipe();
                        return true;
                    }
                    if (deltaX > 0) {
                        swipeAware.onRightToLeftSwipe();
                        return true;
                    }
                } else if (Math.abs(deltaY) > MIN_DISTANCE) {
                    // top or down
                    if (deltaY < 0) {
                        swipeAware.onTopToBottomSwipe();
                        return true;
                    }
                    if (deltaY > 0) {
                        swipeAware.onBottomToTopSwipe();
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }
        return false;
    }
}
