package com.dyggyty.idebt.client.model;

/**
 * User: mobius
 * Date: 24.08.13
 * Time: 9:08
 */
public abstract class FinancialRecord implements Comparable<FinancialRecord> {

    private Long contactId;
    private Double amount;
    private Long currencyId;
    private String currencyCode;
    private String contactName;

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Override
    public int compareTo(FinancialRecord t){
        String contactName1 = t.contactName;

        if (contactName == null && contactName1 == null) {
            return 0;
        } else if (contactName == null) {
            return -1;
        } else if (contactName1 == null) {
            return 1;
        }

        return contactName1.compareTo(contactName1);
    }
}
