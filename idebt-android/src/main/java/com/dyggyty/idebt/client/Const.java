package com.dyggyty.idebt.client;

/**
 * Created by Pavel on 29.07.12 19:32
 */
public class Const {
    public static final String ACTIVITY_IND = "activityIndex";
    public static final int DEBT_LIST_ACTIVITY = 1;
    public static final int ADD_DEBT_ACTIVITY = 2;
    public static final int SETTINGS_ACTIVITY = 3;
    public static final int CONTACT_LIST_ACTIVITY = 4;
    public static final int DEBT_DETAILS_ACTIVITY = 5;
    public static final int CONTACT_DETAILS_ACTIVITY = 6;
    public static final int DEBT_DETAILS_DATE_PERIOD_ACTIVITY = 7;
    public static final int CURRENCY_DETAILS_ACTIVITY = 8;
}
