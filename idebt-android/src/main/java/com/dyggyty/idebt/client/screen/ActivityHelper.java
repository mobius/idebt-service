package com.dyggyty.idebt.client.screen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import com.actionbarsherlock.view.MenuItem;
import com.dyggyty.idebt.client.Const;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.async.SQLiteDataProvider;
import com.dyggyty.idebt.client.contacts.Contact;
import com.dyggyty.idebt.client.contacts.Email;
import com.dyggyty.idebt.client.contacts.Phone;
import com.dyggyty.idebt.client.screen.settings.SettingsActivity;
import com.dyggyty.idebt.utils.MessageException;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * User: mobius
 * Date: 16.03.13
 * Time: 14:10
 */
public class ActivityHelper {

    public static final NumberFormat CURRENCY_FORMAT = NumberFormat.getInstance();
    public static final SimpleDateFormat OUT_SDF = new SimpleDateFormat("yyyy-MM-dd");

    public static final String CONTACT_ID = "contactId";
    public static final String CREATED_DATE = "createdDate";
    public static final String CURRENCY = "currency";
    public static final String CURRENCY_ID = "currencyId";
    public static final String CURRENCY_TITLE = "currencyTitle";
    public static final String CURRENCY_DEFAULT_FLAG = "currencyDefaultFlag";
    public static final String CURRENCY_HAS_TRANSACTIONS = "currencyHasTransactions";
    public static final String COMMENT = "comment";
    public static final String AMOUNT = "amount";
    public static final String AMOUNT_INT = "amountIntValue";
    public static final String FIELD_USER_NAME = "username";

    public static final String MESSAGE = "message";

    private ActivityHelper() {
    }

    public static void showNotification(Activity activity, String message) {
        String messageValue;
        try {
            String messageKey = message.replaceAll("\\.", "_");
            messageValue = getLocalizedValue(activity, messageKey);
        } catch (Exception re) {
            messageValue = message;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(messageValue)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alert = builder.create();
        if (!activity.isFinishing()) {
            alert.show();
        }
    }

    public static void processException(Activity activity, MessageException dse) {
        String messageKey = dse.getMessage();
        showNotification(activity, messageKey);
    }

    public static String getLocalizedValue(Activity activity, String key) {
        Resources resources = activity.getResources();
        try {
            int i = resources.getIdentifier(key.replaceAll("\\.", "_"), "string", activity.getPackageName());
            return resources.getString(i);
        } catch (Exception re) {
            Log.e(ActivityHelper.class.getName(), "Can't find resource " + key, re);
            return null;
        }
    }

    public static Contact findContact(SQLiteDataProvider dataProvider, Contact contact) {

        Contact foundContact = null;

        List<Phone> contactPhones = contact.getPhone();
        for (Contact userContact : dataProvider.getUserContacts()) {
            //trying to find by phone
            List<Phone> phones = userContact.getPhone();
            if (phones != null && phones.size() > 0 && contactPhones != null && contactPhones.size() > 0) {
                String number = getPhoneNumbers(phones.get(0).getNumber());

                for (Phone phone : contactPhones) {
                    if (number.equals(getPhoneNumbers(phone.getNumber()))) {
                        foundContact = userContact;
                        break;
                    }
                }

                if (foundContact != null) {
                    break;
                }
            }

            //trying to find by email
            List<Email> emails = userContact.getEmail();
            List<Email> contactEmails = contact.getEmail();
            if (emails != null && emails.size() > 0 && contactEmails != null && contactEmails.size() > 0) {
                String emailStr = emails.get(0).getAddress().toUpperCase();

                for (Email email : contactEmails) {
                    if (emailStr.equals(email.getAddress())) {
                        foundContact = userContact;
                        break;
                    }
                }

                if (foundContact != null) {
                    break;
                }
            }

            //trying to find by name;
            String contactName = contact.getDisplayName();
            if (contactName != null && userContact.getDisplayName() != null
                    && contactName.trim().toUpperCase().equals(userContact.getDisplayName().trim().toUpperCase())) {
                foundContact = userContact;
                break;
            }
        }

        return foundContact;
    }

    private static String getPhoneNumbers(String phone) {
        return phone.replaceAll("\\D", "");
    }

    public static SQLiteDataProvider getDataProvider(Context applicationContext) {
        return new SQLiteDataProvider(applicationContext);
    }

    public static boolean onOptionsItemSelected(Activity activity, MenuItem item) {
        if (item.getItemId() == R.id.ic_menu_settings) {
            Intent i = new Intent(activity, SettingsActivity.class);
            activity.startActivityForResult(i, Const.SETTINGS_ACTIVITY);
            return true;
        }

        return false;
    }
}
