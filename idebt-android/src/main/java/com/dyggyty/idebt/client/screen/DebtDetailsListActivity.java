package com.dyggyty.idebt.client.screen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import com.actionbarsherlock.view.MenuItem;
import com.dyggyty.idebt.client.ColoredAdapter;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.async.SQLiteDataProvider;
import com.dyggyty.idebt.client.contacts.Contact;
import com.dyggyty.idebt.client.contacts.QuickContactHelper;
import com.dyggyty.idebt.client.model.Transaction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: mobius
 * Date: 16.03.13
 * Time: 16:10
 */
public class DebtDetailsListActivity extends BaseListActivity {

    private static final String DEBT_HAS_BEEN_CLOSED = "message_debt_has_been_closed";

    private TextView debtContactInfo;
    private TextView contactName;
    private TextView contacts;
    private QuickContactBadge contactBadge;
    private QuickContactHelper quickContactHelper;

    private Double amount;
    private Long contactId;
    private Long currencyId;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        debtContactInfo = (TextView) findViewById(R.id.debt_contact_info);
        contactName = (TextView) findViewById(R.id.text_contact_name);
        contacts = (TextView) findViewById(R.id.text_contact_info);
        contactBadge = (QuickContactBadge) findViewById(R.id.debt_contact_badge);
        quickContactHelper = new QuickContactHelper(getApplicationContext());
    }

    protected void onResume() {
        super.onResume();

        amount = getIntent().getDoubleExtra(ActivityHelper.AMOUNT, 0);
        contactId = getIntent().getLongExtra(ActivityHelper.CONTACT_ID, -1);
        currencyId = getIntent().getLongExtra(ActivityHelper.CURRENCY_ID, -1);

        String currencyCode = getIntent().getStringExtra(ActivityHelper.CURRENCY);
        debtContactInfo.setText(ActivityHelper.CURRENCY_FORMAT.format(amount) + " " + currencyCode);

        Contact contact = ActivityHelper.getDataProvider(getApplicationContext()).getContact(contactId);
        if (contact != null) {
            contactName.setText(contact.getDisplayName());

            StringBuilder contactsText = new StringBuilder();
            if (contact.getEmail() != null && contact.getEmail().size() > 0) {
                contactsText.append(contact.getEmail().get(0).getAddress());
            }

            if (contact.getPhone() != null && contact.getPhone().size() > 0) {
                if (contactsText.length() > 0) {
                    contactsText.append("\n");
                }
                String phone = contact.getPhone().get(0).getNumber();
                contactsText.append(phone);
                quickContactHelper.populateCard(contactBadge, phone);
            }

            if (contactsText.length() > 0) {
                contacts.setText(contactsText.toString());
            } else {
                contacts.setVisibility(TextView.INVISIBLE);
            }
        }
    }

    @Override
    protected ColoredAdapter createAdapter(SQLiteDataProvider dataProvider) {

        List<Transaction> transactions = getTransactions(dataProvider);
        Collections.sort(transactions);

        List<Map<String, Object>> transactionsMap = new ArrayList<Map<String, Object>>();
        for (Transaction transaction : transactions) {
            transactionsMap.add(getTransaction(transaction));
        }

        return new ColoredAdapter(
                this,
                transactionsMap,
                R.layout.debt_details_transactions_row,
                new String[]{ActivityHelper.FIELD_USER_NAME, ActivityHelper.AMOUNT, ActivityHelper.CURRENCY, "date"},
                new int[]{R.id.li_username, R.id.li_amount, R.id.li_currency, R.id.li_date}
        ) {
            @Override
                         public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                ImageView direction = (ImageView) view.findViewById(R.id.transaction_direction_indicator);
                Map<String, Object> row = (Map) getItem(position);

                Double amount = (Double) row.get(ActivityHelper.AMOUNT_INT);
                if (amount < 0) {
                    direction.setImageResource(R.drawable.bullet_triangle_red);
                } else {
                    direction.setImageResource(R.drawable.bullet_triangle_green);
                }

                return view;
            }

            public boolean isEnabled(int position) {
                return false;
            }
        };
    }

    private static Map<String, Object> getTransaction(Transaction transaction) {

        Long contactId = transaction.getContactId();

        Map<String, Object> line = new HashMap<String, Object>();
        line.put("id", transaction.getId());
        line.put(ActivityHelper.FIELD_USER_NAME, transaction.getContactName());
        line.put(ActivityHelper.CONTACT_ID, contactId);
        line.put(ActivityHelper.AMOUNT, ActivityHelper.CURRENCY_FORMAT.format(Math.abs(transaction.getAmount())));
        line.put(ActivityHelper.AMOUNT_INT, transaction.getAmount());
        line.put(ActivityHelper.CURRENCY, transaction.getCurrencyCode());
        line.put(ActivityHelper.CREATED_DATE, transaction.getCreated());

        if (transaction.getFinalDate() != null) {
            line.put("date", ActivityHelper.OUT_SDF.format(transaction.getFinalDate()));
        }
        if (transaction.getTransactionComment() != null) {
            line.put(ActivityHelper.COMMENT, transaction.getTransactionComment());
        }

        return line;
    }

    protected List<Transaction> getTransactions(SQLiteDataProvider dataProvider) {
        Long contactId = getIntent().getLongExtra(ActivityHelper.CONTACT_ID, -1);
        Long currencyId = getIntent().getLongExtra(ActivityHelper.CURRENCY_ID, -1);

        return dataProvider.getTransactions(contactId, currencyId);
    }

    protected int getContentViewResId() {
        return R.layout.debt_details;
    }

    protected Integer getMenuId() {
        return R.menu.debt_details_activity_actions;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_menu_close_debt: {
                closeDebt();
                return true;
            }

            default: {
                return ActivityHelper.onOptionsItemSelected(this, item);
            }
        }
    }

    private void closeDebt() {
        final DebtDetailsListActivity activity = this;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.message_confirm))
                .setCancelable(false)
                .setPositiveButton(R.string.label_yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SQLiteDataProvider dataProvider =
                                        ActivityHelper.getDataProvider(getApplicationContext());
                                dataProvider.addTransaction(contactId, -amount, currencyId, null, null);

                                Intent intent = new Intent();
                                intent.putExtra(ActivityHelper.MESSAGE, DEBT_HAS_BEEN_CLOSED);
                                activity.setResult(RESULT_OK, intent);
                                activity.finish();

                                dialog.dismiss();
                            }
                        })
                .setNegativeButton(R.string.label_no,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    protected boolean hideTitle() {
        return false;
    }
}
