package com.dyggyty.idebt.client.screen.transaction;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import com.dyggyty.idebt.client.R;

/**
 * User: mobius
 * Date: 28.08.13
 * Time: 15:11
 */
public class PeriodFragment extends Fragment {

    private static final String KEY_DATE_PERIOD = "datePeriod";
    private static final String KEY_DATE_PERIOD_TYPE = "datePeriodType";

    private Integer datePeriodValue;
    private Integer datePeriodTypeValue;

    private Spinner datePeriod;
    private Spinner datePeriodType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_DATE_PERIOD)) {
                datePeriodValue = savedInstanceState.getInt(KEY_DATE_PERIOD);
            }

            if (savedInstanceState.containsKey(KEY_DATE_PERIOD_TYPE)) {
                datePeriodTypeValue = savedInstanceState.getInt(KEY_DATE_PERIOD_TYPE);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout linearLayout =
                (LinearLayout) inflater.inflate(R.layout.add_debt_period_picker_period_page, container, false);

        datePeriod = (Spinner) linearLayout.findViewById(R.id.date_period);
         datePeriodType = (Spinner) linearLayout.findViewById(R.id.date_period_type);

        if (datePeriodValue != null) {
            datePeriod.setSelection(datePeriodValue);
        }

        if (datePeriodTypeValue != null) {
            datePeriodType.setSelection(datePeriodTypeValue);
        }

        DatePickupFragmentActivity parent = (DatePickupFragmentActivity) getActivity();
        parent.setDatePeriod(datePeriod);
        parent.setDatePeriodType(datePeriodType);

        return linearLayout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_DATE_PERIOD, datePeriod.getSelectedItemPosition());
        outState.putInt(KEY_DATE_PERIOD_TYPE, datePeriodType.getSelectedItemPosition());
    }
}
