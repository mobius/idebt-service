package com.dyggyty.idebt.client.screen.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.SimpleExpandableListAdapter;
import com.dyggyty.idebt.client.Const;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.async.SQLiteDataProvider;
import com.dyggyty.idebt.client.model.Debt;
import com.dyggyty.idebt.client.screen.ActivityHelper;
import com.dyggyty.idebt.client.screen.BaseFragmentActivity;
import com.dyggyty.idebt.client.screen.DebtDetailsListActivity;
import com.dyggyty.idebt.client.screen.ExpandableListFragment;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: mobius
 * Date: 23.09.13
 * Time: 16:05
 */
public class DebtListFragment extends ExpandableListFragment {

    private static final String TITLE_DEBTS_SIZE = "DEBTS_SIZE";
    private static final String DEBTS_ROW = "DEBTS_ROW";

    private SQLiteDataProvider dataProvider;
    private List<Map<String, Object>> titles = new ArrayList<Map<String, Object>>();
    private List<Debt> items = new ArrayList<Debt>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        updateData();
        setListAdapter(createAdapter());
        return inflater.inflate(R.layout.main_debt_list_fragment, container, false);
    }

    private void updateData() {
        if (dataProvider == null) {
            dataProvider = ActivityHelper.getDataProvider(getActivity().getApplicationContext());
        }

        items.clear();
        items.addAll(dataProvider.getDebts());

        titles.clear();
        titles.addAll(getGroupData(items));
    }

    protected ExpandableListAdapter createAdapter() {

        return new SimpleExpandableListAdapter(
                getActivity(),
                titles, // groupData describes the first-level entries
                R.layout.main_debts_fragment_group, // Layout for the first-level entries new
                new String[]{ActivityHelper.FIELD_USER_NAME, DEBTS_ROW}, // Key in the groupData maps to display
                new int[]{R.id.li_username, R.id.li_more}, // Data under "colorName" key goes into this TextView createChildList(), // childData describes second-level entries
                getChildData(titles, items),
                R.layout.main_debt_fragment_row, // Layout for second-level entries
                new String[]{ActivityHelper.AMOUNT, ActivityHelper.CURRENCY}, // Keys in childData maps to display
                new int[]{R.id.li_amount, R.id.li_currency} // Data under the keys above go into these TextViews
        ) {
            public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
                View groupView = super.getGroupView(groupPosition, isExpanded, convertView, parent);

                ImageView direction = (ImageView)groupView.findViewById(R.id.transaction_direction_indicator);
                Map<String, Object> row = (Map) getGroup(groupPosition);
                Double amount = (Double) row.get(ActivityHelper.AMOUNT_INT);
                if (amount < 0) {
                    direction.setImageResource(R.drawable.bullet_triangle_red);
                } else {
                    direction.setImageResource(R.drawable.bullet_triangle_green);
                }

                return groupView;
            }

            @Override
            public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                                     View convertView, ViewGroup parent) {
                View view = super.getChildView(groupPosition, childPosition, isLastChild, convertView, parent);

                ImageView direction = (ImageView) view.findViewById(R.id.transaction_direction_indicator);
                Map<String, Object> row = (Map) getChild(groupPosition, childPosition);

                Double amount = (Double) row.get(ActivityHelper.AMOUNT_INT);
                if (amount < 0) {
                    direction.setImageResource(R.drawable.bullet_triangle_red);
                } else {
                    direction.setImageResource(R.drawable.bullet_triangle_green);
                }

                return view;
            }
        };
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                int childPosition, long id) {
        super.onChildClick(parent, v, groupPosition, childPosition, id);
        Map item = (Map) getExpandableListAdapter().getChild(groupPosition, childPosition);

        Intent i = new Intent(getActivity(), DebtDetailsListActivity.class);
        i.putExtra(ActivityHelper.CONTACT_ID, (Long) item.get(ActivityHelper.CONTACT_ID));
        i.putExtra(ActivityHelper.AMOUNT, (Double) item.get(ActivityHelper.AMOUNT_INT));
        i.putExtra(ActivityHelper.CURRENCY, (String) item.get(ActivityHelper.CURRENCY));
        i.putExtra(ActivityHelper.CURRENCY_ID, (Long) item.get(ActivityHelper.CURRENCY_ID));

        startActivityForResult(i, Const.DEBT_DETAILS_ACTIVITY);
        return true;
    }

    private List<Map<String, Object>> getGroupData(List<Debt> items) {
        Map<Long, Map<String, Object>> titles = new HashMap<Long, Map<String, Object>>();

        for (Debt item : items) {
            Long contactId = item.getContactId();
            Map<String, Object> titleMap = titles.get(contactId);
            if (titleMap == null) {
                titleMap = new HashMap<String, Object>();
                titleMap.put(ActivityHelper.FIELD_USER_NAME, item.getContactName());
                titleMap.put(ActivityHelper.AMOUNT, ActivityHelper.CURRENCY_FORMAT.format(Math.abs(item.getAmount())));
                titleMap.put(ActivityHelper.AMOUNT_INT, item.getAmount());
                titleMap.put(ActivityHelper.CURRENCY, item.getCurrencyCode());
                titleMap.put(ActivityHelper.CONTACT_ID, contactId);
                titles.put(contactId, titleMap);
            }

            Long debtsSize = (Long) titleMap.get(TITLE_DEBTS_SIZE);
            if (debtsSize == null) {
                debtsSize = 0L;
            }
            titleMap.put(TITLE_DEBTS_SIZE, ++debtsSize);
        }

        List<Map<String, Object>> titleList = new ArrayList<Map<String, Object>>(titles.values());

        Collections.sort(titleList, new Comparator<Map<String, ?>>() {
            @Override
            public int compare(Map<String, ?> stringMap, Map<String, ?> stringMap1) {
                String userName = (String) stringMap.get(ActivityHelper.FIELD_USER_NAME);
                String userName1 = (String) stringMap1.get(ActivityHelper.FIELD_USER_NAME);

                if (userName == null && userName1 == null) {
                    return 0;
                } else if (userName == null) {
                    return -1;
                } else if (userName1 == null) {
                    return 1;
                }

                return userName.compareTo(userName1);
            }
        });

        for (Map<String, Object> title : titleList) {
            Long debtsSize = (Long) title.get(TITLE_DEBTS_SIZE);
            String moreValue = getResources().getString(R.string.label_more);
            String andValue = getResources().getString(R.string.label_and);
            title.put(DEBTS_ROW, title.get(ActivityHelper.AMOUNT) + " " + title.get(ActivityHelper.CURRENCY) +
                    ((debtsSize > 1) ? " " + andValue + " " + (debtsSize - 1) + " " + moreValue : ""));
        }

        return titleList;
    }

    private static List<List<Map<String, Object>>> getChildData(List<Map<String, Object>> titles, List<Debt> items) {
        List<List<Map<String, Object>>> result = new ArrayList<List<Map<String, Object>>>();

        for (Map<String, Object> title : titles) {
            List<Map<String, Object>> group = new ArrayList<Map<String, Object>>();
            Object key = title.get(ActivityHelper.CONTACT_ID);
            for (Debt item : items) {
                if (item.getContactId().equals(key)) {

                    Map<String, Object> mapItem = new HashMap<String, Object>();
                    mapItem.put(ActivityHelper.FIELD_USER_NAME, item.getContactName());
                    mapItem.put(ActivityHelper.AMOUNT, ActivityHelper.CURRENCY_FORMAT.format(Math.abs(item.getAmount())));
                    mapItem.put(ActivityHelper.AMOUNT_INT, item.getAmount());
                    mapItem.put(ActivityHelper.CURRENCY, item.getCurrencyCode());
                    mapItem.put(ActivityHelper.CURRENCY_ID, item.getCurrencyId());
                    mapItem.put(ActivityHelper.CONTACT_ID, item.getContactId());

                    group.add(mapItem);
                }
            }

            result.add(group);
        }

        return result;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateData();
        setListAdapter(createAdapter());
        ((BaseFragmentActivity) getActivity()).getAdapter().notifyDataSetChanged();
    }
}
