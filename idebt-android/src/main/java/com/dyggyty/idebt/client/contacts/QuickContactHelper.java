package com.dyggyty.idebt.client.contacts;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.widget.QuickContactBadge;

/**
 * User: mobius
 * Date: 26.08.13
 * Time: 15:43
 */
public class QuickContactHelper {

    private static final String[] PHOTO_ID_PROJECTION = new String[]{
            ContactsContract.Contacts.PHOTO_ID
    };

    private static final String[] PHOTO_BITMAP_PROJECTION = new String[]{
            ContactsContract.CommonDataKinds.Photo.PHOTO
    };

    private final ContentResolver contentResolver;

    public QuickContactHelper(final Context context) {
        contentResolver = context.getContentResolver();
    }

    public void populateCard(QuickContactBadge badge, String phoneNumber) {
        populateCard(contentResolver, badge, phoneNumber);
    }

    public static void populateCard(ContentResolver contentResolver, QuickContactBadge badge, String phoneNumber) {
        badge.assignContactFromPhone(phoneNumber, false);
        final Integer thumbnailId = fetchThumbnailId(contentResolver, phoneNumber);
        if (thumbnailId != null) {
            final Bitmap thumbnail = fetchThumbnail(contentResolver, thumbnailId);
            if (thumbnail != null) {
                badge.setImageBitmap(thumbnail);
            }
        }
    }

    private static Integer fetchThumbnailId(ContentResolver contentResolver, String phoneNumber) {

        final Uri uri =
                Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        final Cursor cursor =
                contentResolver.query(uri, PHOTO_ID_PROJECTION, null, null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");

        try {
            Integer thumbnailId = null;
            if (cursor.moveToFirst()) {
                thumbnailId = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));
            }
            return thumbnailId;
        } finally {
            cursor.close();
        }
    }

    private static Bitmap fetchThumbnail(ContentResolver contentResolver, final int thumbnailId) {

        final Uri uri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, thumbnailId);
        final Cursor cursor = contentResolver.query(uri, PHOTO_BITMAP_PROJECTION, null, null, null);

        try {
            Bitmap thumbnail = null;
            if (cursor.moveToFirst()) {
                final byte[] thumbnailBytes = cursor.getBlob(0);
                if (thumbnailBytes != null) {
                    thumbnail = BitmapFactory.decodeByteArray(thumbnailBytes, 0, thumbnailBytes.length);
                }
            }
            return thumbnail;
        } finally {
            cursor.close();
        }
    }
}
