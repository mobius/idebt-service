package com.dyggyty.idebt.client.screen.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Spinner;
import com.dyggyty.idebt.client.Const;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.screen.ActivityHelper;
import com.viewpagerindicator.PageIndicator;
import java.util.Calendar;

/**
 * @author vitaly.rudenya
 */
public class DatePickupFragmentActivity extends FragmentActivity {

    public static final String FINISH_DATE = "finishDate";

    private static final String[] TABS = new String[2];
    private DatePicker datePicker;
    private Spinner datePeriod;
    private Spinner datePeriodType;
    private ViewPager pager;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_debt_period_picker);

        TABS[0] = ActivityHelper.getLocalizedValue(this, "label_term");
        TABS[1] = ActivityHelper.getLocalizedValue(this, "label_date");

        FragmentPagerAdapter adapter = new PeriodPickerAdapter(getSupportFragmentManager());

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        PageIndicator indicator = (PageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(pager);
    }

    public void setDate(View view) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        switch (pager.getCurrentItem()) {
            case 0: {
                int date = datePeriod.getSelectedItemPosition() + 1;
                int type = datePeriodType.getSelectedItemPosition();
                switch (type) {
                    case 0: {
                        calendar.add(Calendar.DAY_OF_YEAR, date);
                        break;
                    }

                    case 1: {
                        calendar.add(Calendar.MONTH, date);
                        break;
                    }

                    case 2: {
                        calendar.add(Calendar.YEAR, date);
                        break;
                    }
                }
                break;
            }

            case 1: {
                calendar.set(Calendar.YEAR, datePicker.getYear());
                calendar.set(Calendar.MONTH, datePicker.getMonth());
                calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                break;
            }
        }

        Intent intent = new Intent(this, DatePickupFragmentActivity.class);
        intent.putExtra(FINISH_DATE, calendar.getTime());
        intent.putExtra(Const.ACTIVITY_IND, Const.DEBT_DETAILS_DATE_PERIOD_ACTIVITY);

        setResult(RESULT_OK, intent);
        finish();
    }

    void setDatePicker(DatePicker datePicker) {
        this.datePicker = datePicker;
    }

    void setDatePeriod(Spinner datePeriod) {
        this.datePeriod = datePeriod;
    }

    void setDatePeriodType(Spinner datePeriodType) {
        this.datePeriodType = datePeriodType;
    }

    class PeriodPickerAdapter extends FragmentPagerAdapter {

        public PeriodPickerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new PeriodFragment();
                }

                case 1: {
                    return new DateFragment();
                }
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TABS[position % TABS.length].toUpperCase();
        }

        @Override
        public int getCount() {
            return TABS.length;
        }
    }
}
