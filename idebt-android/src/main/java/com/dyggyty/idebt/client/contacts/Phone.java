package com.dyggyty.idebt.client.contacts;

import java.io.Serializable;

/**
 * User: mobius
 * Date: 04.03.13
 * Time: 15:02
 */
public class Phone implements Serializable {
    private Long id;
    private String number;
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Phone(String n, String t) {
        this.number = n;
        this.type = t;
    }

    public Phone(String number, Long id) {
        this.number = number;
        this.id = id;
    }

    public Phone(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone = (Phone) o;

        if (!number.equals(phone.number)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return number.hashCode();
    }
}
