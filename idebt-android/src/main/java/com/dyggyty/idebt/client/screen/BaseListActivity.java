package com.dyggyty.idebt.client.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.dyggyty.idebt.client.ColoredAdapter;
import com.dyggyty.idebt.client.async.SQLiteDataProvider;
import com.dyggyty.idebt.client.screen.listener.LongClickable;
import java.util.Map;

public abstract class BaseListActivity extends SherlockListActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (hideTitle()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        setContentView(getContentViewResId());

        if (this instanceof LongClickable) {
            final LongClickable longClickable = (LongClickable) this;
            this.getListView().setLongClickable(true);
            this.getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @SuppressWarnings("unchecked")
                public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                    longClickable.onLongItemClick((Map) getListAdapter().getItem(position));
                    return true;
                }
            });
        }
    }

    protected Integer getMenuId() {
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Integer menuId = getMenuId();
        if (menuId != null) {
            MenuInflater inflater = getSupportMenuInflater();
            inflater.inflate(menuId, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return ActivityHelper.onOptionsItemSelected(this, item);
    }

    protected boolean hideTitle() {
        return true;
    }

    protected abstract ColoredAdapter createAdapter(SQLiteDataProvider dataProvider);

    protected abstract int getContentViewResId();

    protected void onItemClick(Map<String, ?> item) {
    }

    public void refresh() {
        ColoredAdapter adapter = createAdapter(ActivityHelper.getDataProvider(getApplicationContext()));
        setListAdapter(adapter);
    }

    @SuppressWarnings("unchecked")
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        onItemClick((Map) getListAdapter().getItem(position));
    }

    protected void onResume() {
        super.onResume();
        refresh();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && data.hasExtra(ActivityHelper.MESSAGE)) {
            String messageKey = data.getStringExtra(ActivityHelper.MESSAGE);
            ActivityHelper.showNotification(this, messageKey);
        }
    }

    public View getView() {
        return this.getListView();
    }
}
