package com.dyggyty.idebt.client.screen.main;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.dyggyty.idebt.client.ColoredAdapter;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.async.SQLiteDataProvider;
import com.dyggyty.idebt.client.model.Transaction;
import com.dyggyty.idebt.client.screen.ActivityHelper;
import com.dyggyty.idebt.client.screen.BaseFragmentActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: mobius
 * Date: 23.09.13
 * Time: 16:05
 */
public class HistoryListFragment extends ListFragment {

    private SQLiteDataProvider dataProvider;
    private List<Map<String, Object>> transactionsMap = new ArrayList<Map<String, Object>>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        updateData();
        setListAdapter(createAdapter());
        return inflater.inflate(R.layout.main_history_list_fragment, container, false);
    }

    private ColoredAdapter createAdapter() {
        return new ColoredAdapter(
                getActivity(),
                transactionsMap,
                getDetailsRow(),
                new String[]{ActivityHelper.FIELD_USER_NAME, ActivityHelper.AMOUNT, ActivityHelper.CURRENCY, "date"},
                new int[]{R.id.li_username, R.id.li_amount, R.id.li_currency, R.id.li_date}
        ) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                ImageView direction = (ImageView) view.findViewById(R.id.transaction_direction_indicator);
                Map<String, Object> row = (Map) getItem(position);

                Double amount = (Double) row.get(ActivityHelper.AMOUNT_INT);
                if (amount < 0) {
                    direction.setImageResource(R.drawable.bullet_triangle_red);
                } else {
                    direction.setImageResource(R.drawable.bullet_triangle_green);
                }

                return view;
            }

            public boolean isEnabled(int position) {
                return false;
            }
        };
    }

    private void updateData() {
        if (dataProvider == null) {
            dataProvider = ActivityHelper.getDataProvider(getActivity().getApplicationContext());
        }

        List<Transaction> transactions = getTransactions(dataProvider);
        Collections.sort(transactions);

        transactionsMap.clear();
        for (Transaction transaction : transactions) {
            transactionsMap.add(getTransaction(transaction));
        }
    }

    private int getDetailsRow() {
        return R.layout.main_history_list_fragment_row;
    }

    private List<Transaction> getTransactions(SQLiteDataProvider dataProvider) {
        return dataProvider.getTransactions();
    }

    private static Map<String, Object> getTransaction(Transaction transaction) {

        Long contactId = transaction.getContactId();

        Map<String, Object> line = new HashMap<String, Object>();
        line.put("id", transaction.getId());
        line.put(ActivityHelper.FIELD_USER_NAME, transaction.getContactName());
        line.put(ActivityHelper.CONTACT_ID, contactId);
        line.put(ActivityHelper.AMOUNT, ActivityHelper.CURRENCY_FORMAT.format(Math.abs(transaction.getAmount())));
        line.put(ActivityHelper.AMOUNT_INT, transaction.getAmount());
        line.put(ActivityHelper.CURRENCY, transaction.getCurrencyCode());
        line.put(ActivityHelper.CREATED_DATE, transaction.getCreated());

        if (transaction.getFinalDate() != null) {
            line.put("date", ActivityHelper.OUT_SDF.format(transaction.getFinalDate()));
        }
        if (transaction.getTransactionComment() != null) {
            line.put(ActivityHelper.COMMENT, transaction.getTransactionComment());
        }

        return line;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateData();
        setListAdapter(createAdapter());
        ((BaseFragmentActivity) getActivity()).getAdapter().notifyDataSetChanged();
    }
}
