package com.dyggyty.idebt.client.screen.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import com.actionbarsherlock.view.MenuItem;
import com.dyggyty.idebt.client.Const;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.screen.BaseFragmentActivity;
import com.viewpagerindicator.IconPagerAdapter;
import com.viewpagerindicator.PageIndicator;

/**
 * Created by vitaly.rudenya on 07.09.13.
 */
public class SettingsActivity extends BaseFragmentActivity {

    private static final String[] TABS = new String[1];
    private PagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TABS[0] = getResources().getString(R.string.label_currency);

        adapter = new SettingsPageAdapter(getSupportFragmentManager());

        ViewPager pager = (ViewPager) findViewById(R.id.settings_pager);
        pager.setAdapter(adapter);

        PageIndicator indicator = (PageIndicator) findViewById(R.id.settings_indicator);
        indicator.setViewPager(pager);
    }

    @Override
    public PagerAdapter getAdapter() {
        return adapter;
    }

    @Override
    protected int getContentView() {
        return R.layout.settings;
    }

    @Override
    protected Integer getMenuId() {
        return R.menu.settings_activity_actions;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.ic_menu_add) {
            Intent i = new Intent(this, CurrencyDetailsActivity.class);
            startActivityForResult(i, Const.CURRENCY_DETAILS_ACTIVITY);
            return true;
        }
        return false;
    }

    class SettingsPageAdapter extends FragmentPagerAdapter implements IconPagerAdapter {

        public SettingsPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new CurrencySettingsFragment();
                }
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TABS[position % TABS.length].toUpperCase();
        }

        @Override
        public int getIconResId(int index) {
            switch (index) {
                case 0: {
                    return android.R.drawable.ic_menu_sort_by_size;
                }
            }
            return 0;
        }

        @Override
        public int getCount() {
            return TABS.length;
        }
    }
}
