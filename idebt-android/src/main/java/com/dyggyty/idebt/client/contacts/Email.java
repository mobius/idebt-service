package com.dyggyty.idebt.client.contacts;

import java.io.Serializable;

/**
 * User: mobius
 * Date: 04.03.13
 * Time: 15:00
 */
public class Email implements Serializable {
    private Long id;
    private String address;
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String t) {
        this.type = t;
    }

    public Email(String a, String t) {
        this.address = a;
        this.type = t;
    }

    public Email(String address, Long id) {
        this.address = address;
        this.id = id;
    }

    public Email(String address) {
        this.address = address;
    }
}
