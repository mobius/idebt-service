package com.dyggyty.idebt.client.async;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.dyggyty.idebt.client.contacts.Contact;
import com.dyggyty.idebt.client.contacts.Email;
import com.dyggyty.idebt.client.contacts.Phone;
import com.dyggyty.idebt.client.model.Currency;
import com.dyggyty.idebt.client.model.CurrencyDetail;
import com.dyggyty.idebt.client.model.Debt;
import com.dyggyty.idebt.client.model.FinancialRecord;
import com.dyggyty.idebt.client.model.Transaction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * User: mobius
 * Date: 29.07.13
 * Time: 11:55
 */
public class SQLiteDataProvider extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "debts";

    private static final String TABLE_CURRENCY = "currency";
    private static final String TABLE_CONTACT = "contact";
    private static final String TABLE_PHONE = "phone";
    private static final String TABLE_EMAIL = "email";
    private static final String TABLE_TRANSACTION = "transactions";

    private static final String FIELD_ID = "id";
    private static final String FIELD_CURRENCY_CODE = "currency_code";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_TITLE_KEY = "title_key";
    private static final String FIELD_USE_DEFAULT = "use_default";
    private static final String FIELD_CONTACT_ID = "contact_id";
    private static final String FIELD_CONTACT_NAME = "contact_name";
    private static final String FIELD_PHONE = "phone";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_ADDRESS_BOOK_ID = "address_book_id";
    private static final String FIELD_DELETED = "deleted";
    private static final String FIELD_AMOUNT = "amount";
    private static final String FIELD_CURRENCY_ID = "currency_id";
    private static final String FIELD_CREATED = "created";
    private static final String FIELD_UPDATED = "updated";
    private static final String FIELD_FINAL_DATE = "final_date";
    private static final String FIELD_TRANSACTION_COMMENT = "transaction_comment";
    private static final String ALIAS_PHONE_ID = "phoneId";
    private static final String ALIAS_EMAIL_ID = "emailId";

    // Currency table
    private static final String CREATE_CURRENCY = "CREATE TABLE " + TABLE_CURRENCY + "\n" +
            "(\n" +
            FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            FIELD_CURRENCY_CODE + " TEXT,\n" +
            FIELD_TITLE + " TEXT,\n" +
            FIELD_TITLE_KEY + " TEXT,\n" +
            FIELD_USE_DEFAULT + " INTEGER NOT NULL\n" +
            ")";

    // Transaction table
    private static final String CREATE_TRANSACTION = "CREATE TABLE " + TABLE_TRANSACTION + "\n" +
            "(\n" +
            FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            FIELD_AMOUNT + " REAL NOT NULL,\n" +
            FIELD_CONTACT_ID + " INTEGER NOT NULL,\n" +
            FIELD_CURRENCY_ID + " INTEGER NOT NULL,\n" +
            FIELD_CREATED + " INTEGER,\n" +
            FIELD_UPDATED + " INTEGER,\n" +
            FIELD_FINAL_DATE + " INTEGER,\n" +
            FIELD_TRANSACTION_COMMENT + " TEXT\n" +
            ")";

    // Contact table
    private static final String CREATE_CONTACT = "CREATE TABLE " + TABLE_CONTACT + "\n" +
            "(\n" +
            FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            FIELD_CONTACT_NAME + " TEXT NOT NULL,\n" +
            FIELD_ADDRESS_BOOK_ID + " TEXT,\n" +
            FIELD_DELETED + " INTEGER NOT NULL\n" +
            ")";

    // Phones table
    private static final String CREATE_PHONE = "CREATE TABLE " + TABLE_PHONE + "\n" +
            "(\n" +
            FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            FIELD_CONTACT_ID + " INTEGER NOT NULL,\n" +
            FIELD_PHONE + " TEXT\n" +
            ")";

    // Emails table
    private static final String CREATE_EMAIL = "CREATE TABLE " + TABLE_EMAIL + "\n" +
            "(\n" +
            FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            FIELD_CONTACT_ID + " INTEGER NOT NULL,\n" +
            FIELD_EMAIL + " TEXT\n" +
            ")";

    private static final String SQL_GET_CURRENCY_DETAILS_BASE = "SELECT " + TABLE_CURRENCY + ".*, trans." + FIELD_CURRENCY_ID + " from " + TABLE_CURRENCY + "\n" +
            "LEFT OUTER JOIN " + TABLE_TRANSACTION + " trans ON trans." + FIELD_CURRENCY_ID + "=" + TABLE_CURRENCY + "." + FIELD_ID + "\n";
    private static final String SQL_GET_CURRENCY_DETAILS = SQL_GET_CURRENCY_DETAILS_BASE +
            "GROUP BY " + TABLE_CURRENCY + "." + FIELD_ID;
    private static final String SQL_GET_CURRENCY_DETAIL = SQL_GET_CURRENCY_DETAILS_BASE +
            "WHERE " + TABLE_CURRENCY + "." + FIELD_ID + "=?\n" +
            "GROUP BY " + TABLE_CURRENCY + "." + FIELD_ID;
    private static final String SQL_GET_ALL_CURRENCIES = "SELECT * from " + TABLE_CURRENCY;
    private static final String SQL_GET_ALL_CONTACTS = "SELECT " + TABLE_CONTACT + ".*, " + FIELD_PHONE + ", " +
            FIELD_EMAIL + ", " + TABLE_PHONE + "." + FIELD_ID + " AS " + ALIAS_PHONE_ID + ", " +
            TABLE_EMAIL + "." + FIELD_ID + " AS " + ALIAS_EMAIL_ID + "\n" +
            "from " + TABLE_CONTACT + "\n" +
            "LEFT OUTER JOIN " + TABLE_PHONE + " ON " + TABLE_PHONE + "." + FIELD_CONTACT_ID + "=" + TABLE_CONTACT + "." + FIELD_ID + "\n" +
            "LEFT OUTER JOIN " + TABLE_EMAIL + " ON " + TABLE_EMAIL + "." + FIELD_CONTACT_ID + "=" + TABLE_CONTACT + "." + FIELD_ID + "\n" +
            "WHERE " + FIELD_DELETED + "=0";
    private static final String SQL_GET_CONTACT = SQL_GET_ALL_CONTACTS + " AND " + TABLE_CONTACT + "." + FIELD_ID + "=?";
    private static final String SQL_GET_DEBTS = "SELECT SUM(" + FIELD_AMOUNT + ") AS " + FIELD_AMOUNT + ", " + FIELD_CONTACT_ID + ", " + FIELD_CURRENCY_ID + " FROM " + TABLE_TRANSACTION + "\n" +
            "GROUP BY " + FIELD_CONTACT_ID + ", " + FIELD_CURRENCY_ID + "\n" +
            "HAVING SUM(" + FIELD_AMOUNT + ") <> 0";
    private static final String SQL_GET_DEBTS_COMPOSITE = "SELECT " + FIELD_AMOUNT + ", " + FIELD_CONTACT_ID + ", " +
            FIELD_CURRENCY_ID + ", " + FIELD_CONTACT_NAME + ", " + FIELD_CURRENCY_CODE + "\n" +
            "FROM (" + SQL_GET_DEBTS + ") AS DEBT\n" +
            "LEFT OUTER JOIN " + TABLE_CONTACT + " ON " + TABLE_CONTACT + "." + FIELD_ID + "=DEBT." + FIELD_CONTACT_ID + "\n" +
            "LEFT OUTER JOIN " + TABLE_CURRENCY + " ON " + TABLE_CURRENCY + "." + FIELD_ID + "=DEBT." + FIELD_CURRENCY_ID;
    private static final String SQL_GET_ALL_TRANSACTIONS = "SELECT * FROM " + TABLE_TRANSACTION + "\n" +
            "LEFT OUTER JOIN " + TABLE_CONTACT + " ON " + TABLE_CONTACT + "." + FIELD_ID + "=" + TABLE_TRANSACTION + "." + FIELD_CONTACT_ID + "\n" +
            "LEFT OUTER JOIN " + TABLE_CURRENCY + " ON " + TABLE_CURRENCY + "." + FIELD_ID + "=" + TABLE_TRANSACTION + "." + FIELD_CURRENCY_ID;
    private static final String SQL_GET_ALL_TRANSACTIONS_FOR_CONTACT_AND_CURRENCY = SQL_GET_ALL_TRANSACTIONS + "\n"
            + "WHERE " + FIELD_CONTACT_ID + "=? AND " + FIELD_CURRENCY_ID + "=?";
    private static final String SQL_GET_TRANSACTION = SQL_GET_ALL_TRANSACTIONS + "\n"
            + "WHERE " + TABLE_TRANSACTION + "." + FIELD_ID + "=?";

    public SQLiteDataProvider(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CURRENCY);
        db.execSQL(CREATE_TRANSACTION);
        db.execSQL(CREATE_CONTACT);
        db.execSQL(CREATE_PHONE);
        db.execSQL(CREATE_EMAIL);

        ContentValues values = new ContentValues();
        values.put(FIELD_CURRENCY_CODE, "BYR");
        values.put(FIELD_TITLE_KEY, "curr.byr");
        values.put(FIELD_USE_DEFAULT, 0);

        // Inserting Row
        db.insert(TABLE_CURRENCY, null, values);

        values = new ContentValues();
        values.put(FIELD_CURRENCY_CODE, "EUR");
        values.put(FIELD_TITLE_KEY, "curr.eur");
        values.put(FIELD_USE_DEFAULT, 0);

        // Inserting Row
        db.insert(TABLE_CURRENCY, null, values);

        values = new ContentValues();
        values.put(FIELD_CURRENCY_CODE, "RUB");
        values.put(FIELD_TITLE_KEY, "curr.rur");
        values.put(FIELD_USE_DEFAULT, 0);

        // Inserting Row
        db.insert(TABLE_CURRENCY, null, values);

        values = new ContentValues();
        values.put(FIELD_CURRENCY_CODE, "USD");
        values.put(FIELD_TITLE_KEY, "curr.usd");
        values.put(FIELD_USE_DEFAULT, 1);

        // Inserting Row
        db.insert(TABLE_CURRENCY, null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public List<CurrencyDetail> getCurrencyDetails() {
        List<CurrencyDetail> currencies = new ArrayList<CurrencyDetail>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_CURRENCY_DETAILS, null);

        try {
            // looping through all rows and adding to list
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    CurrencyDetail currencyDetail = getCurrencyDetail(cursor);

                    // Adding to list
                    currencies.add(currencyDetail);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        return currencies;
    }

    public CurrencyDetail getCurrencyDetail(Long currencyId) {
        CurrencyDetail currencyDetail = null;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_CURRENCY_DETAIL, new String[]{String.valueOf(currencyId)});

        try {
            // looping through all rows and adding to list
            if (cursor != null && cursor.moveToFirst()) {
                currencyDetail = getCurrencyDetail(cursor);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        return currencyDetail;
    }

    public List<Currency> getCurrencies() {

        List<Currency> currencies = new ArrayList<Currency>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_ALL_CURRENCIES, null);

        try {
            // looping through all rows and adding to list
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Currency currency = getCurrency(cursor);

                    // Adding to list
                    currencies.add(currency);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        return currencies;
    }

    public Currency getCurrencyByCode(String currencyCode) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_CURRENCY, new String[]{FIELD_ID, FIELD_CURRENCY_CODE, FIELD_TITLE, FIELD_TITLE_KEY, FIELD_USE_DEFAULT}, FIELD_CURRENCY_CODE + "=?",
                new String[]{String.valueOf(currencyCode)}, null, null, null, null);

        Currency currency = null;
        try {
            if (cursor != null && cursor.moveToFirst()) {

                currency = new Currency();
                currency.setId(cursor.getLong(cursor.getColumnIndex(FIELD_ID)));
                currency.setCurrencyCode(cursor.getString(cursor.getColumnIndex(FIELD_CURRENCY_CODE)));

                int titleIndex = cursor.getColumnIndex(FIELD_TITLE);
                if (!cursor.isNull(titleIndex)) {
                    currency.setTitle(cursor.getString(titleIndex));
                }

                int titleKeyIndex = cursor.getColumnIndex(FIELD_TITLE_KEY);
                if (!cursor.isNull(titleKeyIndex)) {
                    currency.setTitleKey(cursor.getString(cursor.getColumnIndex(FIELD_TITLE_KEY)));
                }
                currency.setUseDefault(cursor.getInt(cursor.getColumnIndex(FIELD_USE_DEFAULT)) == 1);

            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        return currency;
    }

    public Currency getCurrency(Long currencyId) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_CURRENCY, new String[]{FIELD_ID, FIELD_CURRENCY_CODE, FIELD_TITLE, FIELD_TITLE_KEY, FIELD_USE_DEFAULT}, FIELD_ID + "=?",
                new String[]{String.valueOf(currencyId)}, null, null, null, null);

        Currency currency = null;
        try {
            if (cursor != null && cursor.moveToFirst()) {
                currency = new Currency();
                currency.setId(cursor.getLong(cursor.getColumnIndex(FIELD_ID)));
                currency.setCurrencyCode(cursor.getString(cursor.getColumnIndex(FIELD_CURRENCY_CODE)));

                int titleIndex = cursor.getColumnIndex(FIELD_TITLE);
                if (!cursor.isNull(titleIndex)) {
                    currency.setTitle(cursor.getString(titleIndex));
                }

                int titleKeyIndex = cursor.getColumnIndex(FIELD_TITLE_KEY);
                if (!cursor.isNull(titleKeyIndex)) {
                    currency.setTitleKey(cursor.getString(cursor.getColumnIndex(FIELD_TITLE_KEY)));
                }
                currency.setUseDefault(cursor.getInt(cursor.getColumnIndex(FIELD_USE_DEFAULT)) == 1);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
        return currency;
    }

    public Contact getContact(Long contactId) {

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_CONTACT, new String[]{contactId.toString()});

        List<Contact> contacts;
        try {
            contacts = getContacts(cursor);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        if (contacts.size() > 0) {
            return contacts.get(0);
        }

        return null;
    }

    public List<Contact> getUserContacts() {

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_ALL_CONTACTS, null);

        List<Contact> contacts;
        try {
            contacts = getContacts(cursor);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        return contacts;
    }

    public List<Debt> getDebts() {

        List<Debt> debts = new ArrayList<Debt>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_DEBTS_COMPOSITE, null);

        try {
            // looping through all rows and adding to list
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Debt debt = new Debt();
                    populateFinancialRecord(cursor, debt);

                    debts.add(debt);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        return debts;
    }

    public List<Transaction> getTransactions(Long contactId, Long currencyId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_ALL_TRANSACTIONS_FOR_CONTACT_AND_CURRENCY,
                new String[]{contactId.toString(), currencyId.toString()});
        try {
            return getTransactions(cursor);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
    }

    public Transaction getTransaction(Long transactionId) {

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_TRANSACTION, new String[]{transactionId.toString()});

        try {
            List<Transaction> transactions = getTransactions(cursor);
            if (transactions.size() > 0) {
                return transactions.get(0);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        return null;
    }

    public List<Transaction> getTransactions() {

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_GET_ALL_TRANSACTIONS, null);

        try {
            return getTransactions(cursor);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
    }

    public void addTransaction(Long targetPersonId, Double amount, Long currencyId, Date finalDate, String comment) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FIELD_CONTACT_ID, targetPersonId);
        values.put(FIELD_AMOUNT, amount);
        values.put(FIELD_CURRENCY_ID, currencyId);
        values.put(FIELD_CREATED, System.currentTimeMillis());
        values.put(FIELD_UPDATED, System.currentTimeMillis());

        if (finalDate != null) {
            values.put(FIELD_FINAL_DATE, finalDate.getTime());
        }

        if (comment != null) {
            values.put(FIELD_TRANSACTION_COMMENT, comment);
        }

        // Inserting Row
        db.insert(TABLE_TRANSACTION, null, values);
        db.close(); // Closing database connection
    }

    public Long createCurrency(String currencyCode, String title) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = getCurrencyValues(currencyCode, title);

        // Inserting Row
        Long currencyId = db.insert(TABLE_CURRENCY, null, values);
        db.close(); // Closing database connection
        return currencyId;
    }

    public void deleteCurrency(Long currencyId) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_CURRENCY, FIELD_ID + " = ?", new String[]{String.valueOf(currencyId)});
        db.close();
    }

    public void updateCurrency(Long currencyId, String currencyCode, String title) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = getCurrencyValues(currencyCode, title);

        // updating row
        db.update(TABLE_CURRENCY, values, FIELD_ID + " = ?", new String[]{String.valueOf(currencyId)});
        db.close();
    }

    private static ContentValues getCurrencyValues(String currencyCode, String title) {
        ContentValues values = new ContentValues();
        values.put(FIELD_CURRENCY_CODE, currencyCode);
        values.put(FIELD_TITLE, title);
        values.put(FIELD_USE_DEFAULT, 0);

        return values;
    }

    public void resetDefaultCurrency(Long currencyId) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        ContentValues values = new ContentValues();
        values.put(FIELD_USE_DEFAULT, 0);
        db.update(TABLE_CURRENCY, values, null, null);

        values = new ContentValues();
        values.put(FIELD_USE_DEFAULT, 1);
        db.update(TABLE_CURRENCY, values, FIELD_ID + " = ?", new String[]{String.valueOf(currencyId)});

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void updateTransaction(Long transactionId, Long targetPersonId, Double amount, Long currencyId,
                                  Date finalDate, String comment) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FIELD_CONTACT_ID, targetPersonId);
        values.put(FIELD_AMOUNT, amount);
        values.put(FIELD_CURRENCY_ID, currencyId);
        values.put(FIELD_CREATED, System.currentTimeMillis());
        values.put(FIELD_UPDATED, System.currentTimeMillis());

        if (finalDate == null) {
            values.putNull(FIELD_FINAL_DATE);
        } else {
            values.put(FIELD_FINAL_DATE, finalDate.getTime());
        }

        if (comment == null) {
            values.putNull(FIELD_TRANSACTION_COMMENT);
        } else {
            values.put(FIELD_TRANSACTION_COMMENT, comment);
        }

        // updating row
        db.update(TABLE_TRANSACTION, values, FIELD_ID + " = ?", new String[]{String.valueOf(transactionId)});
        db.close();
    }

    public void deleteTransaction(Long transactionId) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_TRANSACTION, FIELD_ID + " = ?", new String[]{String.valueOf(transactionId)});
        db.close();
    }

    public void updateContact(Contact contact) {
        Long contactId = contact.getId();

        Contact dbContact = getContact(contactId);

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        List<Phone> phonesToDelete = new ArrayList<Phone>(getDifference(contact.getPhone(), dbContact.getPhone()));
        List<Phone> phonesToAdd = new ArrayList<Phone>(getDifference(dbContact.getPhone(), contact.getPhone()));
        String[] args = new String[1];
        for (Phone phone : phonesToDelete) {
            args[0] = phone.getId().toString();
            db.delete(TABLE_PHONE, FIELD_ID + "=?", args);
        }

        for (Phone phone : phonesToAdd) {
            ContentValues values = new ContentValues();
            values.put(FIELD_CONTACT_ID, contactId);
            values.put(FIELD_PHONE, phone.getNumber());
            db.insert(TABLE_PHONE, null, values);
        }

        List<Email> emailsToDelete = new ArrayList<Email>(getEmailDifference(contact.getEmail(), dbContact.getEmail()));
        List<Email> emailsToAdd = new ArrayList<Email>(getEmailDifference(dbContact.getEmail(), contact.getEmail()));
        for (Email email : emailsToDelete) {
            args[0] = email.getId().toString();
            db.delete(TABLE_EMAIL, FIELD_ID + "=?", args);
        }

        for (Email email : emailsToAdd) {
            ContentValues values = new ContentValues();
            values.put(FIELD_CONTACT_ID, contactId);
            values.put(FIELD_EMAIL, email.getAddress());
            db.insert(TABLE_EMAIL, null, values);
        }

        ContentValues values = new ContentValues();
        values.put(FIELD_CONTACT_NAME, contact.getDisplayName());
        values.put(FIELD_ADDRESS_BOOK_ID, contact.getAddressBookId());

        args[0] = contact.getId().toString();
        db.update(TABLE_CONTACT, values, FIELD_ID + "=?", args);

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public Long addContact(Contact contact) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        ContentValues values = new ContentValues();
        values.put(FIELD_CONTACT_NAME, contact.getDisplayName());
        values.put(FIELD_ADDRESS_BOOK_ID, contact.getAddressBookId());
        values.put(FIELD_DELETED, 0);

        // Inserting Row
        long contactId = db.insert(TABLE_CONTACT, null, values);

        List<Phone> phones = contact.getPhone();
        if (phones != null) {
            for (Phone phone : phones) {
                values = new ContentValues();
                values.put(FIELD_CONTACT_ID, contactId);
                values.put(FIELD_PHONE, phone.getNumber());
                db.insert(TABLE_PHONE, null, values);
            }
        }

        List<Email> emails = contact.getEmail();
        if (emails != null) {
            for (Email email : emails) {
                values = new ContentValues();
                values.put(FIELD_CONTACT_ID, contactId);
                values.put(FIELD_EMAIL, email.getAddress());
                db.insert(TABLE_EMAIL, null, values);
            }
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();

        return contactId;
    }

    public void deleteContact(Long contactId) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        db.delete(TABLE_EMAIL, FIELD_CONTACT_ID + "=?", new String[]{String.valueOf(contactId)});
        db.delete(TABLE_PHONE, FIELD_CONTACT_ID + "=?", new String[]{String.valueOf(contactId)});
        db.delete(TABLE_CONTACT, FIELD_ID + "=?", new String[]{String.valueOf(contactId)});

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    private static CurrencyDetail getCurrencyDetail(Cursor cursor) {
        CurrencyDetail currencyDetail = new CurrencyDetail();
        populateCurrency(cursor, currencyDetail);

        int currencyIdInd = cursor.getColumnIndex(FIELD_CURRENCY_ID);
        currencyDetail.setHasTransactions(!cursor.isNull(currencyIdInd));

        return currencyDetail;
    }

    private static Currency getCurrency(Cursor cursor) {
        Currency currency = new Currency();
        populateCurrency(cursor, currency);
        return currency;
    }

    private static void populateCurrency(Cursor cursor, Currency currency) {
        currency.setId(cursor.getLong(cursor.getColumnIndex(FIELD_ID)));
        currency.setCurrencyCode(cursor.getString(cursor.getColumnIndex(FIELD_CURRENCY_CODE)));

        int titleIndex = cursor.getColumnIndex(FIELD_TITLE);
        if (!cursor.isNull(titleIndex)) {
            currency.setTitle(cursor.getString(titleIndex));
        }

        int titleKeyIndex = cursor.getColumnIndex(FIELD_TITLE_KEY);
        if (!cursor.isNull(titleKeyIndex)) {
            currency.setTitleKey(cursor.getString(cursor.getColumnIndex(FIELD_TITLE_KEY)));
        }
        currency.setUseDefault(cursor.getInt(cursor.getColumnIndex(FIELD_USE_DEFAULT)) == 1);
    }

    private static List<Transaction> getTransactions(Cursor cursor) {
        List<Transaction> transactions = new ArrayList<Transaction>();

        if (cursor != null && cursor.moveToFirst()) {

            int idFieldInd = cursor.getColumnIndex(FIELD_ID);
            int createdFieldInd = cursor.getColumnIndex(FIELD_CREATED);
            int updatedFieldInd = cursor.getColumnIndex(FIELD_UPDATED);
            int finalDateIndex = cursor.getColumnIndex(FIELD_FINAL_DATE);
            int commentIndex = cursor.getColumnIndex(FIELD_TRANSACTION_COMMENT);

            do {
                Transaction transaction = new Transaction();
                populateFinancialRecord(cursor, transaction);

                transaction.setId(cursor.getLong(idFieldInd));
                transaction.setCreated(new Date(cursor.getLong(createdFieldInd)));
                transaction.setUpdated(new Date(cursor.getLong(updatedFieldInd)));

                if (!cursor.isNull(finalDateIndex)) {
                    transaction.setFinalDate(new Date(cursor.getLong(finalDateIndex)));
                }
                if (!cursor.isNull(commentIndex)) {
                    transaction.setTransactionComment(cursor.getString(commentIndex));
                }

                transactions.add(transaction);
            } while (cursor.moveToNext());
        }

        return transactions;
    }

    private static List<Contact> getContacts(Cursor cursor) {
        Map<Long, Contact> contactMap = new HashMap<Long, Contact>();
        // looping through all rows and adding to list
        if (cursor != null && cursor.moveToFirst()) {
            do {
                Long contactId = cursor.getLong(cursor.getColumnIndex(FIELD_ID));
                Contact contact = contactMap.get(contactId);

                if (contact == null) {
                    contact = new Contact();
                    contact.setId(contactId);
                    contact.setDisplayName(cursor.getString(cursor.getColumnIndex(FIELD_CONTACT_NAME)));
                    contact.setAddressBookId(cursor.getString(cursor.getColumnIndex(FIELD_ADDRESS_BOOK_ID)));
                    contactMap.put(contactId, contact);
                }

                int phoneIndex = cursor.getColumnIndex(FIELD_PHONE);
                int phoneIdIndex = cursor.getColumnIndex(ALIAS_PHONE_ID);
                if (!cursor.isNull(phoneIndex)) {
                    String phone = cursor.getString(phoneIndex);
                    Long phoneId = cursor.getLong(phoneIdIndex);

                    List<Phone> phones = contact.getPhone();
                    if (phones == null) {
                        phones = new ArrayList<Phone>();
                        contact.setPhone(phones);
                    }

                    phones.add(new Phone(phone, phoneId));
                }

                int emailIndex = cursor.getColumnIndex(FIELD_EMAIL);
                int emailIdIndex = cursor.getColumnIndex(ALIAS_EMAIL_ID);
                if (!cursor.isNull(emailIndex)) {
                    String email = cursor.getString(emailIndex);
                    Long emailId = cursor.getLong(emailIdIndex);
                    if (email != null) {
                        List<Email> emails = contact.getEmail();
                        if (emails == null) {
                            emails = new ArrayList<Email>();
                            contact.setEmail(emails);
                        }

                        emails.add(new Email(email, emailId));
                    }
                }
            } while (cursor.moveToNext());
        }

        return new ArrayList<Contact>(contactMap.values());
    }

    private static void populateFinancialRecord(Cursor cursor, FinancialRecord financialRecord) {
        final int fieldAmountInd = cursor.getColumnIndex(FIELD_AMOUNT);
        final int fieldContactInd = cursor.getColumnIndex(FIELD_CONTACT_ID);
        final int fieldContactNameInd = cursor.getColumnIndex(FIELD_CONTACT_NAME);
        final int fieldCurrencyIdInd = cursor.getColumnIndex(FIELD_CURRENCY_ID);
        final int fieldCurrencyCodeInd = cursor.getColumnIndex(FIELD_CURRENCY_CODE);

        financialRecord.setAmount(cursor.getDouble(fieldAmountInd));
        financialRecord.setContactId(cursor.getLong(fieldContactInd));
        financialRecord.setCurrencyId(cursor.getLong(fieldCurrencyIdInd));

        if (!cursor.isNull(fieldContactNameInd)) {
            financialRecord.setContactName(cursor.getString(fieldContactNameInd));
        }
        if (!cursor.isNull(fieldCurrencyCodeInd)) {
            financialRecord.setCurrencyCode(cursor.getString(fieldCurrencyCodeInd));
        }
    }

    private static List<Phone> getDifference(List<Phone> original, List<Phone> result) {

        if (original == null || result == null) {
            return Collections.emptyList();
        }

        List<Phone> difference = new ArrayList<Phone>();
        Set<Phone> phoneSet = new HashSet<Phone>(original);

        for (Phone phone : result) {
            if (!phoneSet.contains(phone)) {
                difference.add(phone);
            }
        }

        return difference;
    }

    private static List<Email> getEmailDifference(List<Email> original, List<Email> result) {

        if (original == null || result == null) {
            return Collections.emptyList();
        }

        List<Email> difference = new ArrayList<Email>();
        Set<Email> emailSet = new HashSet<Email>(original);

        for (Email email : result) {
            if (!emailSet.contains(email)) {
                difference.add(email);
            }
        }

        return difference;
    }
}
