package com.dyggyty.idebt.client.screen.settings;

import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.async.SQLiteDataProvider;
import com.dyggyty.idebt.client.model.Currency;
import com.dyggyty.idebt.client.model.CurrencyDetail;
import com.dyggyty.idebt.client.screen.ActivityHelper;
import com.dyggyty.idebt.client.screen.BaseEditableActivity;

/**
 * User: mobius
 * Date: 29.09.13
 * Time: 21:35
 */
public class CurrencyDetailsActivity extends BaseEditableActivity {

    private static final String SAVE_SUCCESS_MESSAGE = "message_currency_has_been_saved";
    private static final String DELETE_SUCCESS_MESSAGE = "message_currency_has_been_deleted";

    private boolean deleteButtonFlag;
    private EditText currencyCode;
    private EditText currencyLabel;
    private CheckBox currencyDefault;
    private SQLiteDataProvider dataProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currencyCode = (EditText) findViewById(R.id.currency_code);
        currencyLabel = (EditText) findViewById(R.id.currency_label);
        currencyDefault = (CheckBox) findViewById(R.id.currency_default);

        dataProvider = ActivityHelper.getDataProvider(getApplicationContext());
    }

    @Override
    protected int getContentView() {
        return R.layout.settings_currency_form;
    }

    @Override
    public void onStart() {
        super.onStart();

        Intent data = getIntent();

        if (data.hasExtra(ActivityHelper.CURRENCY_ID)) {
            Long currencyId = data.getLongExtra(ActivityHelper.CURRENCY_ID, -1);

            deleteButtonFlag = true;

            CurrencyDetail currency = dataProvider.getCurrencyDetail(currencyId);
            currencyCode.setText(currency.getCurrencyCode());
            if (currency.isHasTransactions() || (currency.getTitleKey() != null && currency.getTitleKey().length() > 0)) {
                deleteButtonFlag = false;
            }

            if (currency.getTitle() != null && currency.getTitle().length() > 0) {
                currencyLabel.setText(currency.getTitle());
            } else if (currency.getTitleKey() != null && currency.getTitleKey().length() > 0) {
                currencyLabel.setText(ActivityHelper.getLocalizedValue(this, currency.getTitleKey()));
            }

            currencyDefault.setChecked(currency.getUseDefault());

            if (!deleteButtonFlag) {
                currencyCode.setEnabled(false);
            }
        } else {
            deleteButtonFlag = false;
        }
    }

    @Override
    public boolean deleteEntity() {
        Intent data = getIntent();
        if (data.hasExtra(ActivityHelper.CURRENCY_ID)) {
            Long currencyId = data.getLongExtra(ActivityHelper.CURRENCY_ID, -1);
            dataProvider.deleteCurrency(currencyId);
        }

        return true;
    }

    @Override
    protected String getDeleteSuccessMessage() {
        Intent data = getIntent();
        if (data.hasExtra(ActivityHelper.CURRENCY_ID)) {
            return DELETE_SUCCESS_MESSAGE;
        }

        return null;
    }

    @Override
    protected boolean hasDeleteOperation() {
        return deleteButtonFlag;
    }

    @Override
    protected boolean saveEntity() {
        Intent data = getIntent();

        String code = currencyCode.getText().toString().trim().toUpperCase();
        String title = currencyLabel.getText().toString();
        Boolean defaultFlag = currencyDefault.isChecked();

        if (code.length() == 0) {
            ActivityHelper.showNotification(this, "message_currency_code_empty");
            return false;
        }

        Currency currency = dataProvider.getCurrencyByCode(code);
        if (currency != null && !(data.hasExtra(ActivityHelper.CURRENCY_ID) &&
                currency.getId().equals(data.getLongExtra(ActivityHelper.CURRENCY_ID, -1)))) {
            ActivityHelper.showNotification(this, "message_currency_code_exists");
            return false;
        }

        if (title.length() == 0) {
            ActivityHelper.showNotification(this, "message_currency_title_empty");
            return false;
        }

        Long currencyId;
        if (data.hasExtra(ActivityHelper.CURRENCY_ID)) {
            currencyId = data.getLongExtra(ActivityHelper.CURRENCY_ID, -1);
            dataProvider.updateCurrency(currencyId, code, title);
        } else {
            currencyId = dataProvider.createCurrency(code, title);
        }

        if (defaultFlag) {
            dataProvider.resetDefaultCurrency(currencyId);
        }

        return true;
    }

    @Override
    protected String getSaveSuccessMessage() {
        return SAVE_SUCCESS_MESSAGE;
    }
}
