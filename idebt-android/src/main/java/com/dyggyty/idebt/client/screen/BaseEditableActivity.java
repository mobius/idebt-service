package com.dyggyty.idebt.client.screen;

import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.dyggyty.idebt.client.R;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * User: vitaly.rudenya
 * Date: 04.03.13
 * Time: 10:21
 */
public abstract class BaseEditableActivity extends SherlockActivity {

    private EasyTracker easyTracker;

    @Override
    public void onStart() {
        super.onStart();
        easyTracker.activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        easyTracker.activityStop(this);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        easyTracker = EasyTracker.getInstance(this);

        setContentView(getContentView());
        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
    }

    protected abstract int getContentView();

    protected Integer getMenuId() {
        return R.menu.editable_activity_actions;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Integer menuId = getMenuId();

        if (menuId != null) {
            MenuInflater inflater = getSupportMenuInflater();
            inflater.inflate(menuId, menu);

            MenuItem deleteMenuItem = menu.findItem(R.id.ic_menu_delete);
            deleteMenuItem.setVisible(hasDeleteOperation());
        }
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Save current entity.
     *
     * @return True if no errors and False otherwise.
     */
    protected abstract boolean saveEntity();

    /**
     * Return success message key value for save operation.
     *
     * @return Message key value.
     */
    protected abstract String getSaveSuccessMessage();

    /**
     * Delete current entity.
     *
     * @return True if no errors and False otherwise.
     */
    protected boolean deleteEntity() {
        return false;
    }

    protected boolean hasDeleteOperation() {
        return false;
    }

    /**
     * Return success message key value for delete operation.
     *
     * @return Message key value.
     */
    protected String getDeleteSuccessMessage() {
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_menu_save: {
                if (saveEntity()) {
                    Intent intent = new Intent();
                    String successMessage = getSaveSuccessMessage();
                    if (successMessage != null) {
                        intent.putExtra(ActivityHelper.MESSAGE, successMessage);
                    }
                    this.setResult(RESULT_OK, intent);
                    this.finish();
                }

                return true;
            }

            case R.id.ic_menu_delete: {
                if (hasDeleteOperation() && deleteEntity()) {
                    Intent intent = new Intent();
                    String successMessage = getDeleteSuccessMessage();
                    if (successMessage != null) {
                        intent.putExtra(ActivityHelper.MESSAGE, successMessage);
                    }
                    this.setResult(RESULT_OK, intent);
                    this.finish();
                }

                return true;
            }
        }

        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && data.hasExtra(ActivityHelper.MESSAGE)) {
            String messageKey = data.getStringExtra(ActivityHelper.MESSAGE);
            ActivityHelper.showNotification(this, messageKey);
        }
    }
}
