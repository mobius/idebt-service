package com.dyggyty.idebt.client.screen.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dyggyty.idebt.client.Const;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.screen.AddDebt;
import com.dyggyty.idebt.client.screen.BaseFragmentActivity;
import com.viewpagerindicator.IconPagerAdapter;
import com.viewpagerindicator.PageIndicator;

/**
 * User: mobius
 * Date: 23.09.13
 * Time: 15:58
 */
public class MainActivity extends BaseFragmentActivity {

    private static final String[] TABS = new String[2];
    private PagerAdapter adapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TABS[0] = getResources().getString(R.string.title_debts);
        TABS[1] = getResources().getString(R.string.title_history);

        adapter = new MainPageAdapter(getSupportFragmentManager());

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        PageIndicator indicator = (PageIndicator) findViewById(R.id.indicator);
        indicator.setOnPageChangeListener(
                new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i2) {
                        Menu menu = getMenu();
                        if (menu != null) {
                            MenuItem addDebt = menu.findItem(R.id.ic_menu_add_debt);
                            addDebt.setVisible(i == 0);
                        }
                    }

                    @Override
                    public void onPageSelected(int i) {
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {
                    }
                }
        );
        indicator.setViewPager(pager);
    }

    @Override
    public PagerAdapter getAdapter() {
        return adapter;
    }

    @Override
    protected int getContentView() {
        return R.layout.main;
    }

    protected Integer getMenuId() {
        return R.menu.main_activity_actions;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_menu_add_debt: {
                Intent i = new Intent(this, AddDebt.class);
                startActivityForResult(i, Const.ADD_DEBT_ACTIVITY);
                return true;
            }

            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    class MainPageAdapter extends FragmentPagerAdapter implements IconPagerAdapter {

        public MainPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: {
                    return new DebtListFragment();
                }

                case 1: {
                    return new HistoryListFragment();
                }
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TABS[position % TABS.length].toUpperCase();
        }

        @Override
        public int getIconResId(int index) {
            switch (index) {
                case 0: {
                    return android.R.drawable.ic_menu_info_details;
                }

                case 1: {
                    return android.R.drawable.ic_menu_recent_history;
                }
            }
            return 0;
        }

        @Override
        public int getCount() {
            return TABS.length;
        }
    }
}
