package com.dyggyty.idebt.client.screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * User: vitaly.rudenya
 * Date: 07.10.13
 * Time: 11:13
 */
public abstract class BaseFragmentActivity extends SherlockFragmentActivity {

    private EasyTracker easyTracker;
    private Menu menu;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        easyTracker = EasyTracker.getInstance(this);
        setContentView(getContentView());
    }

    public abstract PagerAdapter getAdapter();

    protected abstract int getContentView();

    @Override
    public void onStart() {
        super.onStart();
        easyTracker.activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        easyTracker.activityStop(this);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && data.hasExtra(ActivityHelper.MESSAGE)) {
            String messageKey = data.getStringExtra(ActivityHelper.MESSAGE);
            ActivityHelper.showNotification(this, messageKey);
        }

        getAdapter().notifyDataSetChanged();
    }

    protected Integer getMenuId() {
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Integer menuId = getMenuId();
        if (menuId != null) {
            MenuInflater inflater = getSupportMenuInflater();
            inflater.inflate(menuId, menu);

            this.menu = menu;
        }
        return super.onCreateOptionsMenu(menu);
    }

    protected Menu getMenu() {
        return menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return ActivityHelper.onOptionsItemSelected(this, item);
    }
}
