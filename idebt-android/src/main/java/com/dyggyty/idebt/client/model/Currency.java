package com.dyggyty.idebt.client.model;

/**
 * User: mobius
 * Date: 29.07.13
 * Time: 13:04
 */
public class Currency implements Comparable<Currency> {
    private Long id;
    private String currencyCode;
    private String title;
    private String titleKey;
    private Boolean useDefault;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleKey() {
        return titleKey;
    }

    public void setTitleKey(String titleKey) {
        this.titleKey = titleKey;
    }

    public Boolean getUseDefault() {
        return useDefault;
    }

    public void setUseDefault(Boolean useDefault) {
        this.useDefault = useDefault;
    }

    public String toString() {
        return currencyCode;
    }

    @Override
    public int compareTo(Currency o) {
        return currencyCode.compareTo(o.currencyCode);
    }
}
