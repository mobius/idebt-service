package com.dyggyty.idebt.client.screen.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.dyggyty.idebt.client.ColoredAdapter;
import com.dyggyty.idebt.client.Const;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.async.SQLiteDataProvider;
import com.dyggyty.idebt.client.model.CurrencyDetail;
import com.dyggyty.idebt.client.screen.ActivityHelper;
import com.dyggyty.idebt.client.screen.BaseFragmentActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: mobius
 * Date: 28.09.13
 * Time: 19:43
 */
public class CurrencySettingsFragment extends ListFragment {

    private List<Map<String, Object>> currencyMap = new ArrayList<Map<String, Object>>();
    private SQLiteDataProvider dataProvider;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        updateData();
        setListAdapter(createAdapter());

        return inflater.inflate(R.layout.settings_currency_list_fragment, container, false);
    }

    private void updateData() {
        if (dataProvider == null) {
            dataProvider = ActivityHelper.getDataProvider(getActivity().getApplicationContext());
        }

        currencyMap.clear();
        for (CurrencyDetail currencyDetail : getCurrencies(dataProvider)) {
            currencyMap.add(getCurrencyDetail(currencyDetail));
        }

        Collections.sort(currencyMap, new Comparator<Map<String, Object>>() {
            @SuppressWarnings("unchecked")
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                return ((Comparable) o1.get(ActivityHelper.CURRENCY)).compareTo(o2.get(ActivityHelper.CURRENCY));
            }
        });
    }

    private ColoredAdapter createAdapter() {

        return new ColoredAdapter(
                getActivity(),
                currencyMap,
                getDetailsRow(),
                new String[]{ActivityHelper.CURRENCY, ActivityHelper.CURRENCY_TITLE},
                new int[]{R.id.li_code, R.id.li_title}
        ) {
            @SuppressWarnings("unchecked")
            protected int getRowColor(int position) {
                Map<String, Object> row = (Map<String, Object>) getItem(position);
                if ((Boolean) row.get(ActivityHelper.CURRENCY_DEFAULT_FLAG)) {
                    return getResources().getColor(R.color.selected_item_end);
                } else {
                    return super.getRowColor(position);
                }
            }
        };
    }

    private int getDetailsRow() {
        return R.layout.settings_currency_list_fragment_row;
    }

    private List<CurrencyDetail> getCurrencies(SQLiteDataProvider dataProvider) {
        return dataProvider.getCurrencyDetails();
    }

    private Map<String, Object> getCurrencyDetail(CurrencyDetail currencyDetail) {

        String titleKey = currencyDetail.getTitleKey();
        String title = currencyDetail.getTitle();

        Map<String, Object> line = new HashMap<String, Object>();
        line.put("id", currencyDetail.getId());
        line.put(ActivityHelper.CURRENCY_ID, currencyDetail.getId());
        line.put(ActivityHelper.CURRENCY, currencyDetail.getCurrencyCode());
        line.put(ActivityHelper.CURRENCY_TITLE, titleKey != null && (title == null || title.trim().length() == 0) ?
                ActivityHelper.getLocalizedValue(getActivity(), titleKey) : title);
        line.put(ActivityHelper.CURRENCY_DEFAULT_FLAG, currencyDetail.getUseDefault());
        line.put(ActivityHelper.CURRENCY_HAS_TRANSACTIONS, currencyDetail.isHasTransactions());

        return line;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateData();
        setListAdapter(createAdapter());
        ((BaseFragmentActivity) getActivity()).getAdapter().notifyDataSetChanged();
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Map row = (Map) getListAdapter().getItem(position);

        Intent i = new Intent(getActivity(), CurrencyDetailsActivity.class);
        i.putExtra(ActivityHelper.CURRENCY_ID, (Long) row.get(ActivityHelper.CURRENCY_ID));
        startActivityForResult(i, Const.CURRENCY_DETAILS_ACTIVITY);
    }
}
