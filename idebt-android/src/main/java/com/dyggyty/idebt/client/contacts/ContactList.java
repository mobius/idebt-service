package com.dyggyty.idebt.client.contacts;

import java.util.ArrayList;
import java.util.List;

/**
 * User: mobius
 * Date: 04.03.13
 * Time: 14:50
 */
public class ContactList {
    private List<Contact> contacts = new ArrayList<Contact>();

    public List<Contact> getContacts() {
        return contacts;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    public ContactList clone() {
        ContactList contactList = new ContactList();

        List<Contact> contacts = new ArrayList<Contact>();
        contacts.addAll(this.contacts);
        contactList.contacts = contacts;

        return contactList;
    }
}
