package com.dyggyty.idebt.client.screen;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.dyggyty.idebt.client.ColoredAdapter;
import com.dyggyty.idebt.client.Const;
import com.dyggyty.idebt.client.R;
import com.dyggyty.idebt.client.async.SQLiteDataProvider;
import com.dyggyty.idebt.client.contacts.Contact;
import com.dyggyty.idebt.client.contacts.ContactAPI;

import java.util.*;

/**
 * User: mobius
 * Date: 05.03.13
 * Time: 13:52
 */
public class ContactListActivity extends BaseListActivity {

    public static final String CONTACT_DELIMITER = "  #";
    private static final String CONTACT_DELIMITER_FLAG = "delimiterFlag";
    public static final String CONTACT = "contact";
    public static final String CONTACT_ID = "contactId";
    private static final String CONTACT_NAME = "contactName";
    private static final String CONTACT_PHONE = "contactPhone";
    private static final String CONTACT_EMAIL = "contactEmail";

    private EditText contactsFilterText;
    private ContactAPI contactAPI;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ContactListActivity currentActivity = this;

        contactAPI = new ContactAPI(getApplication().getContentResolver());

        contactsFilterText = (EditText) findViewById(R.id.et_username);
        contactsFilterText.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        currentActivity.refresh();
                    }
                });
    }

    @Override
    protected ColoredAdapter createAdapter(SQLiteDataProvider dataProvider) {

        String filter = contactsFilterText.getText().toString();

        List<Contact> contactList =
                mergeContacts(dataProvider.getUserContacts(), contactAPI.getContactList().getContacts());

        contactList = filterContacts(contactList, filter);
        Collections.sort(contactList);

        List<Map<String, Object>> items = new ArrayList<Map<String, Object>>();

        String lastLetter = "";

        for (Contact contact : contactList) {
            String contactName = contact.getDisplayName();
            if (contactName != null && contactName.trim().length() > 0) {
                String letter = contactName.trim().substring(0, 1).toUpperCase();
                if (!letter.equals(lastLetter)) {
                    Map<String, Object> delimiterRow = new HashMap<String, Object>();
                    delimiterRow.put(CONTACT_NAME, CONTACT_DELIMITER + letter);
                    delimiterRow.put(CONTACT_DELIMITER_FLAG, Boolean.TRUE);
                    items.add(delimiterRow);
                    lastLetter = letter;
                }
            }

            Map<String, Object> contactRow = getContactRow(contact);
            if (contactRow != null) {
                items.add(contactRow);
            }
        }

        return new ColoredAdapter(
                this,
                items,
                R.layout.contact_row,
                new String[]{CONTACT_NAME, CONTACT_PHONE, CONTACT_EMAIL},
                new int[]{R.id.li_contact_name, R.id.li_contact_phone, R.id.li_contact_email}
        ) {

            public boolean isEnabled(int position) {
                return !isHeader(position);
            }

            @SuppressWarnings("unchecked")
            @Override
            protected boolean isHeader(int position) {
                Map<String, Object> row = (Map<String, Object>) getItem(position);
                return row.get(CONTACT_DELIMITER_FLAG) != null;
            }
        };
    }

    protected boolean hideTitle() {
        return false;
    }

    protected int getContentViewResId() {
        return R.layout.contacts_list;
    }

    protected void onItemClick(Map<String, ?> item) {
        Contact contact = (Contact) item.get(CONTACT);
        if (contact.getId() == null) {
            SQLiteDataProvider dataProvider = ActivityHelper.getDataProvider(getApplicationContext());
            Long contactId = dataProvider.addContact(contact);
            contact.setId(contactId);
        }

        setResult(RESULT_OK, populateContactIntent(new Intent(), item));
        finish();
    }

    private static Intent populateContactIntent(Intent intent, Map<String, ?> item) {
        intent.putExtra(CONTACT, (Contact) item.get(CONTACT));
        intent.putExtra(Const.ACTIVITY_IND, Const.CONTACT_LIST_ACTIVITY);

        return intent;
    }

    public void filterContact(View v) {
        refresh();
    }

    private static Map<String, Object> getContactRow(Contact contact) {
        String displayName = contact.getDisplayName();
        if (displayName != null) {
            Map<String, Object> contactRow = new HashMap<String, Object>();
            contactRow.put(CONTACT, contact);
            contactRow.put(CONTACT_NAME, displayName);
            contactRow.put(CONTACT_ID, contact.getId());

            if (contact.getPhone() != null && contact.getPhone().size() > 0) {
                contactRow.put(CONTACT_PHONE, contact.getPhone().get(0).getNumber());
            }

            if (contact.getEmail() != null && contact.getEmail().size() > 0) {
                contactRow.put(CONTACT_EMAIL, contact.getEmail().get(0).getAddress());
            }

            return contactRow;
        }
        return null;
    }

    private static List<Contact> filterContacts(List<Contact> contacts, String name) {
        String filter;
        if (name != null && name.trim().length() > 0) {
            filter = name.trim().toUpperCase();
        } else {
            return contacts;
        }

        List<Contact> result = new ArrayList<Contact>();
        for (Contact contact : contacts) {
            if (contact != null && contact.getDisplayName() != null) {
                if (contact.getDisplayName().trim().toUpperCase().contains(filter)) {
                    result.add(contact);
                }
            }
        }

        return result;
    }

    private List<Contact> mergeContacts(List<Contact> idebtContacts, List<Contact> phoneContacts) {

        List<Contact> mergedContacts = new ArrayList<Contact>();

        Map<String, Contact> phoneContactsMap = new HashMap<String, Contact>();
        for (Contact contact : phoneContacts) {
            phoneContactsMap.put(contact.getAddressBookId(), contact);
        }

        SQLiteDataProvider dataProvider = ActivityHelper.getDataProvider(getApplicationContext());
        for (Contact idebtContact : idebtContacts) {
            String addressBookId = idebtContact.getAddressBookId();
            if (addressBookId != null) {
                Contact phoneContact = phoneContactsMap.get(addressBookId);
                if (phoneContact != null) {
                    if (!phoneContact.equals(idebtContact)) {
                        phoneContact.setId(idebtContact.getId());
                        dataProvider.updateContact(phoneContact);
                        mergedContacts.add(phoneContact);
                    } else {
                        mergedContacts.add(idebtContact);
                    }
                    phoneContactsMap.remove(addressBookId);
                }
            }
        }

        mergedContacts.addAll(phoneContactsMap.values());

        return mergedContacts;
    }
}
