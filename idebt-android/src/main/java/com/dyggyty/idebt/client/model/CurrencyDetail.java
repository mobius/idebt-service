package com.dyggyty.idebt.client.model;

/**
 * User: mobius
 * Date: 28.09.13
 * Time: 20:16
 */
public class CurrencyDetail extends Currency {
    private boolean hasTransactions;

    public boolean isHasTransactions() {
        return hasTransactions;
    }

    public void setHasTransactions(boolean hasTransactions) {
        this.hasTransactions = hasTransactions;
    }
}
