package com.dyggyty.idebt.client.contacts;

import java.io.Serializable;
import java.util.List;

/**
 * User: mobius
 * Date: 04.03.13
 * Time: 14:58
 */
public class Contact implements Comparable<Contact>, Serializable {
    private Long id;
    private String addressBookId;
    private String displayName;
    private List<Phone> phone;
    private List<Email> email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressBookId() {
        return addressBookId;
    }

    public void setAddressBookId(String addressBookId) {
        this.addressBookId = addressBookId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<Phone> getPhone() {
        return phone;
    }

    public void setPhone(List<Phone> phone) {
        this.phone = phone;
    }

    public List<Email> getEmail() {
        return email;
    }

    public void setEmail(List<Email> email) {
        this.email = email;
    }

    @Override
    public int compareTo(Contact contact) {
        if (displayName == null) {
            if (contact.displayName == null) {
                return 0;
            } else {
                return -1;
            }
        }

        if (contact.displayName == null) {
            return 1;
        }

        return displayName.compareTo(contact.displayName);
    }
}
