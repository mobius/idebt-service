package com.dyggyty.idebt.client;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import java.util.List;
import java.util.Map;

public class ColoredAdapter extends SimpleAdapter {
    private static final int[] COLORS = new int[]{0x20CCCCCC, 0x00000000};
    private static final int DELIMITER_BACKGROUND = 0x20AAAADD;

    public ColoredAdapter(Context context, List<Map<String, Object>> items, int resource, String[] from, int[] to) {
        super(context, items, resource, from, to);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        view.setBackgroundColor(getRowColor(position));
        return view;
    }

    protected int getRowColor(int position) {
        if (isHeader(position)) {
            return DELIMITER_BACKGROUND;
        } else {
            int colorPos = position % COLORS.length;
            return COLORS[colorPos];
        }
    }

    protected boolean isHeader(int position) {
        return false;
    }
}
