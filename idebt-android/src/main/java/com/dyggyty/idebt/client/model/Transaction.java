package com.dyggyty.idebt.client.model;

import java.util.Date;

/**
 * User: mobius
 * Date: 29.07.13
 * Time: 15:18
 */
public class Transaction extends FinancialRecord {
    private Long id;
    private Date created;
    private Date updated;
    private Date finalDate;
    private String transactionComment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public String getTransactionComment() {
        return transactionComment;
    }

    public void setTransactionComment(String transactionComment) {
        this.transactionComment = transactionComment;
    }

    @Override
    public int compareTo(FinancialRecord t) {
        Transaction transaction1 = (Transaction) t;
        int result = -created.compareTo(transaction1.created);
        if (result == 0) {
            return super.compareTo(t);
        }

        return result;
    }
}
