package com.dyggyty.idebt.client.screen.transaction;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import com.dyggyty.idebt.client.R;

/**
 * @author vitaly.rudenya
 */
public class DateFragment extends Fragment {

    private static final String KEY_YEAR = "year";
    private static final String KEY_MONTH = "month";
    private static final String KEY_DAY = "day";

    private DatePicker datePicker;
    private Integer year;
    private Integer month;
    private Integer day;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_YEAR)) {
                year = savedInstanceState.getInt(KEY_YEAR);
            }

            if (savedInstanceState.containsKey(KEY_MONTH)) {
                month = savedInstanceState.getInt(KEY_MONTH);
            }

            if (savedInstanceState.containsKey(KEY_DAY)) {
                day = savedInstanceState.getInt(KEY_DAY);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        LinearLayout layout =
                (LinearLayout) inflater.inflate(R.layout.add_debt_period_picker_date_page, container, false);
        datePicker = (DatePicker) layout.findViewById(R.id.et_due_date);

        if (year != null && month != null && day != null) {
            datePicker.updateDate(year, month, day);
        }

        DatePickupFragmentActivity parent = (DatePickupFragmentActivity) getActivity();
        parent.setDatePicker(datePicker);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_YEAR, datePicker.getYear());
        outState.putInt(KEY_MONTH, datePicker.getMonth());
        outState.putInt(KEY_DAY, datePicker.getDayOfMonth());
    }
}