package com.dyggyty.idebt.rest;

import com.dyggyty.idebt.facade.PersonContactFacade;
import com.dyggyty.idebt.facade.UserFacade;
import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Phone;
import com.dyggyty.idebt.model.User;
import com.dyggyty.idebt.rest.response.ChangeUserInfoResponse;
import com.dyggyty.idebt.rest.response.ChangeUserPasswordResponse;
import com.dyggyty.idebt.rest.response.CheckUserResponse;
import com.dyggyty.idebt.rest.response.DeleteUserContactResponse;
import com.dyggyty.idebt.rest.response.DeleteUserResponse;
import com.dyggyty.idebt.rest.response.ForgotUserPasswordResponse;
import com.dyggyty.idebt.rest.response.GetUserContactResponse;
import com.dyggyty.idebt.rest.response.GetUserContactsResponse;
import com.dyggyty.idebt.rest.response.GetUserInfoResponse;
import com.dyggyty.idebt.rest.response.LoginUserResponse;
import com.dyggyty.idebt.rest.response.RegisterUserResponse;
import com.dyggyty.idebt.rest.response.SaveUserContactResponse;
import com.dyggyty.idebt.utils.MessageException;
import com.dyggyty.idebt.utils.RequestValidator;
import com.dyggyty.idebt.wl.RequestParams;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 22.07.12
 * Time: 23:00
 */
@Path("user")
public class UserAction extends GenericAction {

    private UserFacade userFacade;
    private PersonContactFacade personContactFacade;

    @POST
    @Path("check")
    @Produces("application/json")
    public CheckUserResponse check(@Context HttpServletRequest request) {

        CheckUserResponse userResponse = new CheckUserResponse();
        String userEmail = getParameter(request, RequestParams.REQUEST_USER_EMAIL);

        try {
            RequestValidator.validateCheckUserExists(userEmail);
            String lowerUserEmail = userEmail.toLowerCase();

            Boolean userExistsFlag = userFacade.checkUserExists(lowerUserEmail);
            userResponse.setUserExists(userExistsFlag);
        } catch (MessageException me) {
            processMessageException(me, userResponse);
        }

        return userResponse;
    }

    @POST
    @Path("login")
    @Produces("application/json")
    public LoginUserResponse login(@Context HttpServletRequest request) {

        String password = getParameter(request, RequestParams.REQUEST_PASSWORD);
        String userEmail = getParameter(request, RequestParams.REQUEST_USER_EMAIL);
        LoginUserResponse userResponse = new LoginUserResponse();

        try {
            RequestValidator.validateLoginUser(userEmail, password);
            String lowerUserEmail = userEmail.toLowerCase();

            String session = userFacade.loginUser(lowerUserEmail, password);
            userResponse.setUserSessionId(session);
        } catch (MessageException me) {
            processMessageException(me, userResponse);
        }

        return userResponse;
    }

    @POST
    @Path("register")
    @Produces("application/json")
    public RegisterUserResponse register(@Context HttpServletRequest request) {

        String password = getParameter(request, RequestParams.REQUEST_PASSWORD);
        String userEmail = getParameter(request, RequestParams.REQUEST_USER_EMAIL);
        String nickName = getParameter(request, RequestParams.REQUEST_NICK_NAME);
        String phone = getParameter(request, RequestParams.REQUEST_PHONE);

        RegisterUserResponse userResponse = new RegisterUserResponse();

        try {
            RequestValidator.validateRegisterUser(userEmail, nickName, password, phone);
            String lowerUserEmail = userEmail.toLowerCase();

            userFacade.registerUser(lowerUserEmail, password, nickName, phone);
        } catch (MessageException me) {
            processMessageException(me, userResponse);
        }

        return userResponse;
    }

    @GET
    @Path("usercontacts/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public GetUserContactsResponse getUserContacts(@PathParam(PARAM_USER_SESSION) final String userSession) {

        GetUserContactsResponse response = new GetUserContactsResponse();
        try {
            RequestValidator.validateGetUserContacts(userSession);

            List<PersonContact> personContacts = userFacade.getUserContacts(userSession);
            for (PersonContact personContact : personContacts) {
                response.addContactDetails(personContact);
            }
        } catch (MessageException me) {
            processMessageException(me, response);
        }
        return response;
    }

    @POST
    @Path("usercontact/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public GetUserContactResponse getUserContact(@Context HttpServletRequest request,
                                                 @PathParam(PARAM_USER_SESSION) final String userSession) {

        GetUserContactResponse response = new GetUserContactResponse();
        try {
            Long personId = getLongValue(request, RequestParams.REQUEST_PERSON_CONTACT_ID);

            RequestValidator.validateGetUserContact(userSession, personId);

            PersonContact personContact = personContactFacade.getPersonContact(userSession, personId);
            response.setContactDetails(personContact);
        } catch (MessageException me) {
            processMessageException(me, response);
        }
        return response;
    }

    @GET
    @Path("confirmemail/{" + RequestParams.PARAM_CONFIRMATION_KEY + "}")
    @Produces("text/html")
    public String confirmEmail(@Context HttpServletRequest request,
                               @Context HttpServletResponse response,
                               @PathParam(RequestParams.PARAM_CONFIRMATION_KEY) final String confirmationKey) throws IOException {

        String idebtWebUrl = getWebAppPath(request);

        try {
            userFacade.confirmEmail(confirmationKey);
            response.sendRedirect(idebtWebUrl + "confirmsuccess.xhtml");
        } catch (MessageException me) {
            response.sendRedirect(idebtWebUrl + "confirmfail.xhtml");
        }

        return null;
    }

    @POST
    @Path("saveusercontact/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public SaveUserContactResponse saveUserContact(@Context HttpServletRequest request,
                                                   @PathParam(PARAM_USER_SESSION) final String userSession) {

        SaveUserContactResponse response = new SaveUserContactResponse();
        try {
            Long personId = getLongValue(request, RequestParams.REQUEST_PERSON_CONTACT_ID);
            String personEmail = getParameter(request, RequestParams.REQUEST_PERSON_CONTACT_EMAIL);
            String personPhone = getParameter(request, RequestParams.REQUEST_PERSON_CONTACT_PHONE);
            String personName = getParameter(request, RequestParams.REQUEST_PERSON_CONTACT_NAME);
            String contactExternalId = getParameter(request, RequestParams.REQUEST_PERSON_CONTACT_EXT_ID);

            RequestValidator.validateSaveUserContact(userSession, personName, personEmail, personPhone,
                    contactExternalId);

            Long newId = personContactFacade.saveOrUpdatePersonContact(userSession, personId, personName, personEmail,
                    personPhone, contactExternalId);
            response.setPersonId(newId);
        } catch (MessageException me) {
            processMessageException(me, response);
        }

        return response;
    }

    @POST
    @Path("deleteusercontact/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public DeleteUserContactResponse deleteUserContact(@Context HttpServletRequest request,
                                                       @PathParam(PARAM_USER_SESSION) final String userSession) {

        DeleteUserContactResponse response = new DeleteUserContactResponse();
        try {
            Long personId = getLongValue(request, RequestParams.REQUEST_PERSON_CONTACT_ID);
            RequestValidator.validateDeleteUserContact(userSession, personId);
            personContactFacade.deletePerson(userSession, personId);
        } catch (MessageException me) {
            processMessageException(me, response);
        }

        return response;
    }

    @POST
    @Path("changepassword/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public ChangeUserPasswordResponse changeUserPassword(@Context HttpServletRequest request,
                                                         @PathParam(PARAM_USER_SESSION) final String userSession) {

        String userPassword = getParameter(request, RequestParams.REQUEST_PASSWORD);
        String userNewPassword = getParameter(request, RequestParams.REQUEST_NEW_PASSWORD);
        String confirmationCode = getParameter(request, RequestParams.PARAM_CONFIRMATION_KEY);

        ChangeUserPasswordResponse response = new ChangeUserPasswordResponse();
        try {
            RequestValidator.validateChangeUserPassword(userSession, userPassword, userNewPassword, confirmationCode);

            userFacade.changeUserPassword(userSession, confirmationCode, userPassword, userNewPassword);
        } catch (MessageException me) {
            processMessageException(me, response);
        }

        return response;
    }

    @POST
    @Path("forgotpassword/{" + PARAM_USER_NAME + "}")
    @Produces("application/json")
    public ForgotUserPasswordResponse forgotUserPassword(@Context HttpServletRequest request) {

        String userEmail = getParameter(request, RequestParams.REQUEST_USER_EMAIL);

        ForgotUserPasswordResponse response = new ForgotUserPasswordResponse();

        try {
            RequestValidator.validateForgotPassword(userEmail);
            String lowerUserEmail = userEmail.toLowerCase();

            userFacade.forgotUserPassword(lowerUserEmail);
        } catch (MessageException me) {
            processMessageException(me, response);
        }

        return response;
    }

    @POST
    @Path("personalinfo/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public ChangeUserInfoResponse changePersonalInfo(@Context HttpServletRequest request,
                                                     @PathParam(PARAM_USER_SESSION) final String userSession) {

        String nickName = getParameter(request, RequestParams.REQUEST_NICK_NAME);
        String userEmail = getParameter(request, RequestParams.REQUEST_USER_EMAIL);
        String userPhone = getParameter(request, RequestParams.REQUEST_PHONE);

        ChangeUserInfoResponse userResponse = new ChangeUserInfoResponse();

        try {
            RequestValidator.validateUpdateUserInfo(userSession, nickName, userEmail, userPhone);

            userFacade.changeUserInfo(userSession, nickName, userEmail, userPhone);
        } catch (MessageException me) {
            processMessageException(me, userResponse);
        }

        return userResponse;
    }

    @GET
    @Path("getpersonalinfo/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public GetUserInfoResponse getPersonalInfo(@PathParam(PARAM_USER_SESSION) final String userSession) {

        GetUserInfoResponse userResponse = new GetUserInfoResponse();

        try {
            RequestValidator.validateGetUserInfo(userSession);

            User currentUser = userFacade.getCurrentUser(userSession);
            userResponse.setNickName(currentUser.getName());
            userResponse.setUserName(currentUser.getUsername());

            List<String> userEmails = new ArrayList<String>();
            Collection<Email> emails = currentUser.getEmails();
            if (emails != null) {
                for (Email email : emails) {
                    userEmails.add(email.getContactInfo());
                }
            }
            userResponse.setUserEmails(userEmails);

            List<String> userPhones = new ArrayList<String>();
            Collection<Phone> phones = currentUser.getPhones();
            if (phones != null) {
                for (Phone phone : phones) {
                    userPhones.add(phone.getContactInfo());
                }
            }
            userResponse.setUserPhones(userPhones);

        } catch (MessageException me) {
            processMessageException(me, userResponse);
        }

        return userResponse;
    }

    @GET
    @Path("delete/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public DeleteUserResponse deleteUser(@PathParam(PARAM_USER_SESSION) final String userSession) {

        DeleteUserResponse userResponse = new DeleteUserResponse();

        try {
            userFacade.deleteCurrentUser(userSession);
        } catch (MessageException me) {
            processMessageException(me, userResponse);
        }

        return userResponse;
    }

    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    public void setPersonContactFacade(PersonContactFacade personContactFacade) {
        this.personContactFacade = personContactFacade;
    }
}
