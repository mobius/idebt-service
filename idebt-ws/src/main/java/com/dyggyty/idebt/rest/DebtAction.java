package com.dyggyty.idebt.rest;

import com.dyggyty.idebt.facade.TransactionFacade;
import com.dyggyty.idebt.model.Debt;
import com.dyggyty.idebt.rest.response.GetDebtsResponse;
import com.dyggyty.idebt.utils.MessageException;
import com.dyggyty.idebt.utils.RequestValidator;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * User: mobius
 * Date: 01.09.12
 * Time: 13:07
 */
@Path("debt")
public class DebtAction extends GenericAction {

    private TransactionFacade transactionFacade;

    @GET
    @Path("getall/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public GetDebtsResponse getAll(@PathParam(PARAM_USER_SESSION) final String userSession) {

        GetDebtsResponse getDebtsResponse = new GetDebtsResponse();

        try {
            RequestValidator.validateGetUserDebts(userSession);
            List<Debt> debts = transactionFacade.getDebts(userSession);
            for (Debt debt : debts) {
                getDebtsResponse.addDebt(debt);
            }
        } catch (MessageException me) {
            processMessageException(me, getDebtsResponse);
        }

        return getDebtsResponse;
    }

    public void setTransactionFacade(TransactionFacade transactionFacade) {
        this.transactionFacade = transactionFacade;
    }
}
