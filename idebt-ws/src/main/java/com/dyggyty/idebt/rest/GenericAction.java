package com.dyggyty.idebt.rest;

import com.dyggyty.idebt.rest.response.GenericResponse;
import com.dyggyty.idebt.utils.ErrorMessages;
import com.dyggyty.idebt.utils.MessageException;
import com.dyggyty.idebt.wl.RequestParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Date;

/**
 * User: mobius
 * Date: 10.08.12
 * Time: 14:00
 */
public abstract class GenericAction {

    private final Log LOGGER = LogFactory.getLog(this.getClass());
    private static final String VALIDATION_PREFIX = "validate.";
    private static final String VALIDATION_IS_NOT_NUMBER = ".not.number";
    private static final String VALIDATION_IS_NOT_DATE = ".not.date";

    public static final String PARAM_USER_NAME = "userName";
    public static final String PARAM_USER_SESSION = "userSession";

    protected static String getParameter(HttpServletRequest request, String paramName) {
        String value = request.getParameter(paramName);
        if (value == null) {
            value = request.getHeader(paramName);
        }
        return value;
    }

    protected void processMessageException(MessageException me, GenericResponse response) {
        String message = me.getMessage();
        if (ErrorMessages.STATUS_OK.equals(message)) {
            LOGGER.info(message);
        } else {
            LOGGER.warn(message);
        }
        response.setErrorMessageKey(message);
    }

    protected String getWebAppPath(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        sb.append(request.getScheme()).append("://")
                .append(request.getServerName());

        if (request.getServerPort() != 80) {
            sb.append(":").append(request.getServerPort()).append("/idebt-web/");
        }

        return sb.toString();
    }

    protected Integer getIntegerValue(HttpServletRequest request, String parameterName) {
        String strValue = getParameter(request, parameterName);

        if (strValue != null) {
            try {
                return Integer.valueOf(strValue);
            } catch (NumberFormatException ex) {
                throw new MessageException(VALIDATION_PREFIX + parameterName + VALIDATION_IS_NOT_NUMBER);
            }
        }

        return null;
    }

    protected Long getLongValue(HttpServletRequest request, String parameterName) {
        String strValue = getParameter(request, parameterName);

        if (strValue != null) {
            try {
                return Long.valueOf(strValue);
            } catch (NumberFormatException ex) {
                throw new MessageException(VALIDATION_PREFIX + parameterName + VALIDATION_IS_NOT_NUMBER);
            }
        }

        return null;
    }

    protected Double getDoubleValue(HttpServletRequest request, String parameterName) {
        String strValue = getParameter(request, parameterName);

        if (strValue != null) {
            try {
                return Double.valueOf(strValue);
            } catch (NumberFormatException ex) {
                throw new MessageException(VALIDATION_PREFIX + parameterName + VALIDATION_IS_NOT_NUMBER);
            }
        }

        return null;
    }

    protected Date getDateValue(HttpServletRequest request, String parameterName) {
        String strValue = getParameter(request, parameterName);

        if (strValue != null) {
            try {
                return RequestParams.APP_DATE_FORMAT.parse(strValue);
            } catch (ParseException ex) {
                throw new MessageException(VALIDATION_PREFIX + parameterName + VALIDATION_IS_NOT_DATE);
            }
        }

        return null;
    }
}
