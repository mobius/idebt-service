package com.dyggyty.idebt.rest;

import com.dyggyty.idebt.facade.CurrencyFacade;
import com.dyggyty.idebt.model.Currency;
import com.dyggyty.idebt.rest.response.GetCurrencyResponse;
import com.dyggyty.idebt.utils.MessageException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 09.10.12
 * Time: 3:05
 */
@Path("currency")
public class CurrencyAction extends GenericAction {

    private CurrencyFacade currencyFacade;

    public void setCurrencyFacade(CurrencyFacade currencyFacade) {
        this.currencyFacade = currencyFacade;
    }

    @GET
    @Path("getall")
    @Produces("application/json")
    public GetCurrencyResponse getAll() {

        GetCurrencyResponse response = new GetCurrencyResponse();

        try {
            List<Currency> currencies = currencyFacade.getAll();
            for (Currency currency : currencies) {
                response.addCurrencyDetail(currency);
            }
        } catch (MessageException me) {
            processMessageException(me, response);
        }

        return response;
    }
}
