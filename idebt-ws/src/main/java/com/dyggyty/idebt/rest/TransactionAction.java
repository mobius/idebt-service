package com.dyggyty.idebt.rest;

import com.dyggyty.idebt.facade.TransactionFacade;
import com.dyggyty.idebt.model.Transaction;
import com.dyggyty.idebt.rest.response.AddTransactionResponse;
import com.dyggyty.idebt.rest.response.ConfirmTransactionResponse;
import com.dyggyty.idebt.rest.response.DeleteTransactionResponse;
import com.dyggyty.idebt.rest.response.GetTransactionResponse;
import com.dyggyty.idebt.rest.response.GetTransactionsResponse;
import com.dyggyty.idebt.utils.MessageException;
import com.dyggyty.idebt.utils.RequestValidator;
import com.dyggyty.idebt.wl.RequestParams;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * User: mobius
 * Date: 10.08.12
 * Time: 13:48
 */
@Path("transaction")
public class TransactionAction extends GenericAction {

    private TransactionFacade transactionFacade;

    @POST
    @Path("add/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public AddTransactionResponse addTransaction(@Context HttpServletRequest request,
                                                 @PathParam(PARAM_USER_SESSION) final String userSession) {

        AddTransactionResponse addTransactionResponse = new AddTransactionResponse();

        try {
            Long targetPersonId = getLongValue(request, RequestParams.REQUEST_PERSON_CONTACT_ID);
            Double amount = getDoubleValue(request, RequestParams.REQUEST_AMOUNT);
            String currencyCode = getParameter(request, RequestParams.REQUEST_CURRENCY_CODE);
            Date finalDate = getDateValue(request, RequestParams.REQUEST_FINAL_DATE);
            String comment = getParameter(request, RequestParams.REQUEST_COMMENT);

            RequestValidator.
                    validateAddTransaction(userSession, targetPersonId, amount, currencyCode, finalDate, comment);
            transactionFacade.addTransaction(userSession, targetPersonId, amount, currencyCode, finalDate, comment);
        } catch (MessageException me) {
            processMessageException(me, addTransactionResponse);
        }

        return addTransactionResponse;
    }

    @POST
    @Path("confirm/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public ConfirmTransactionResponse confirmTransaction(@Context HttpServletRequest request,
                                                         @PathParam(PARAM_USER_SESSION) final String userSession) {

        ConfirmTransactionResponse confirmTransactionResponse = new ConfirmTransactionResponse();

        try {
            Long transactionId = getLongValue(request, RequestParams.REQUEST_TRANSACTION_ID);

            transactionFacade.confirmTransaction(userSession, transactionId);
        } catch (MessageException me) {
            processMessageException(me, confirmTransactionResponse);
        }

        return confirmTransactionResponse;
    }

    @GET
    @Path("getall/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public GetTransactionsResponse getTransactions(@PathParam(PARAM_USER_SESSION) final String userSession) {

        GetTransactionsResponse getTransactionsResponse = new GetTransactionsResponse();

        try {
            RequestValidator.validateGetUserTransactions(userSession);
            List<Transaction> transactions = transactionFacade.getTransactions(userSession);
            for (Transaction transaction : transactions) {
                getTransactionsResponse.addTransaction(userSession, transaction);
            }
        } catch (MessageException me) {
            processMessageException(me, getTransactionsResponse);
        }

        return getTransactionsResponse;
    }

    @POST
    @Path("get/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public GetTransactionResponse getTransaction(@Context HttpServletRequest request,
                                                 @PathParam(PARAM_USER_SESSION) final String userSession) {

        GetTransactionResponse getTransactionsResponse = new GetTransactionResponse();

        try {
            Long transactionId = getLongValue(request, RequestParams.REQUEST_TRANSACTION_ID);

            RequestValidator.validateGetUserTransaction(userSession, transactionId);

            Transaction transaction = transactionFacade.getTransaction(userSession, transactionId);
            getTransactionsResponse.setTransactionDetails(transaction);
        } catch (MessageException me) {
            processMessageException(me, getTransactionsResponse);
        }

        return getTransactionsResponse;
    }

    @GET
    @Path("confirm/{" + RequestParams.PARAM_CONFIRMATION_KEY + "}")
    @Produces("text/html")
    public String confirmTransaction(@Context HttpServletRequest request,
                                     @Context HttpServletResponse response,
                                     @PathParam(RequestParams.PARAM_CONFIRMATION_KEY) final String confirmationKey) throws IOException {

        String idebtWebUrl = getWebAppPath(request);

        try {
            transactionFacade.confirmTransaction(confirmationKey);
            response.sendRedirect(idebtWebUrl + "confirmsuccess.xhtml");
        } catch (MessageException me) {
            response.sendRedirect(idebtWebUrl + "confirmfail.xhtml");
        }

        return null;
    }


    @POST
    @Path("update/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public AddTransactionResponse updateTransaction(@Context HttpServletRequest request,
                                                    @PathParam(PARAM_USER_SESSION) final String userSession) {

        AddTransactionResponse addTransactionResponse = new AddTransactionResponse();

        try {
            Long transactionId = getLongValue(request, RequestParams.REQUEST_TRANSACTION_ID);
            Long targetPersonId = getLongValue(request, RequestParams.REQUEST_PERSON_CONTACT_ID);
            Double amount = getDoubleValue(request, RequestParams.REQUEST_AMOUNT);
            String currencyCode = getParameter(request, RequestParams.REQUEST_CURRENCY_CODE);
            Date finalDate = getDateValue(request, RequestParams.REQUEST_FINAL_DATE);
            String comment = getParameter(request, RequestParams.REQUEST_COMMENT);

            RequestValidator.validateUpdateTransaction(userSession, transactionId, targetPersonId, amount,
                    currencyCode, finalDate, comment);

            transactionFacade.updateTransaction(userSession, transactionId, targetPersonId, amount, currencyCode,
                    finalDate, comment);
        } catch (MessageException me) {
            processMessageException(me, addTransactionResponse);
        }

        return addTransactionResponse;
    }

    @POST
    @Path("delete/{" + PARAM_USER_SESSION + "}")
    @Produces("application/json")
    public DeleteTransactionResponse deleteTransaction(@Context HttpServletRequest request,
                                                       @PathParam(PARAM_USER_SESSION) final String userSession) {

        DeleteTransactionResponse deleteTransactionResponse = new DeleteTransactionResponse();

        try {
            Long transactionId = getLongValue(request, RequestParams.REQUEST_TRANSACTION_ID);

            RequestValidator.validateDeleteTransaction(userSession, transactionId);

            transactionFacade.deleteTransaction(userSession, transactionId);
        } catch (MessageException me) {
            processMessageException(me, deleteTransactionResponse);
        }

        return deleteTransactionResponse;
    }

    public void setTransactionFacade(TransactionFacade transactionFacade) {
        this.transactionFacade = transactionFacade;
    }
}
