package com.dyggyty.idebt.rest;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * User: vitaly.rudenya
 * Date: 22.07.12
 * Time: 22:59
 */
public class IdebtApplication extends Application {
    private static Set<Object> singletons = new HashSet<Object>();

    public synchronized void setLoginAction(UserAction userAction) {
        singletons.add(userAction);
    }

    public synchronized void setTransactionAction(TransactionAction transactionAction) {
        singletons.add(transactionAction);
    }

    public synchronized void setDebtAction(DebtAction debtAction) {
        singletons.add(debtAction);
    }

    public synchronized void setCurrencyAction(CurrencyAction currencyAction) {
        singletons.add(currencyAction);
    }

    public synchronized Set<Object> getSingletons() {
        return singletons;
    }
}
