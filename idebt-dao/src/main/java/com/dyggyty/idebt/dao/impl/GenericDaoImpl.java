package com.dyggyty.idebt.dao.impl;

import com.dyggyty.idebt.dao.GenericDao;
import com.dyggyty.idebt.model.AbstractBean;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * User: mobius
 * Date: 15.05.12
 * Time: 16:24
 */
@Transactional
public abstract class GenericDaoImpl<T extends AbstractBean> implements GenericDao<T> {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected abstract Class<T> getEntityClass();

    @SuppressWarnings("unchecked")
    @Override
    public T getById(Serializable id) {
        return (T) getCurrentSession().get(getEntityClass(), id);
    }

    @Override
    public Serializable save(T obj) {
        getCurrentSession().save(obj);
        return obj.getId();
    }

    @Override
    public Serializable saveOrUpdate(T obj) {
        getCurrentSession().saveOrUpdate(obj);
        return obj.getId();
    }

    @Override
    public void update(T obj) {
        getCurrentSession().update(obj);
    }

    @Override
    public void delete(T obj) {
        getCurrentSession().delete(obj);
    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
