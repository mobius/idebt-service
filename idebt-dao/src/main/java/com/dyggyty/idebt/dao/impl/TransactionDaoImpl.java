package com.dyggyty.idebt.dao.impl;

import com.dyggyty.idebt.dao.TransactionDao;
import com.dyggyty.idebt.model.Transaction;
import com.dyggyty.idebt.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: mobius
 * Date: 17.08.12
 * Time: 8:01
 */
@Transactional
public class TransactionDaoImpl extends GenericDaoImpl<Transaction> implements TransactionDao {

    private static final String FIELD_OWNER = "owner";
    private static final String FIELD_PERSON_CONTACT = "personContact";
    private static final String FIELD_CONFIRMATION_CODE = "confirmationCode";
    private static final String FIELD_ID = "id";
    private static final String FIELD_VERIFIED = "verified";

    private static final String ALIAS_USER = "user";
    private static final String ALIAS_TARGET_PERSON = "target";

    private static final String FIELD_USER_SESSION = ALIAS_USER + ".session";
    private static final String FIELD_TARGET_PERSON_ID = ALIAS_TARGET_PERSON + ".id";
    private static final String FIELD_TARGET_PERSON_DELETED = ALIAS_TARGET_PERSON + ".deleted";

    @Override
    protected Class<Transaction> getEntityClass() {
        return Transaction.class;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Transaction> getUserTransactions(String userSession) {

        DetachedCriteria targetUserIds = DetachedCriteria.forClass(User.class)
                .add(Restrictions.eq(FIELD_USER_SESSION, userSession))
                .setProjection(Projections.property(UserDaoImpl.FIELD_ID));

        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .createAlias(FIELD_OWNER, ALIAS_USER)
                .createAlias(FIELD_PERSON_CONTACT, ALIAS_TARGET_PERSON)
                .add(Restrictions.eq(FIELD_TARGET_PERSON_DELETED, Boolean.FALSE))
                .add(Restrictions.or(
                        Restrictions.eq(FIELD_USER_SESSION, userSession),
                        Property.forName(FIELD_TARGET_PERSON_ID).in(targetUserIds)));
        return criteria.list();
    }

    @Override
    public Transaction getUserTransaction(String userSession, Long transactionId) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_ID, transactionId))
                .createAlias(FIELD_OWNER, ALIAS_USER)
                .add(Restrictions.eq(FIELD_USER_SESSION, userSession));
        return (Transaction) criteria.uniqueResult();
    }

    @Override
    public Transaction getTransactionByConfirmationCode(String confirmationCode) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_CONFIRMATION_CODE, confirmationCode));

        return (Transaction) criteria.uniqueResult();
    }

    @Override
    public Transaction getNonConfirmedTransaction(String userSession, Long transactionId) {
        DetachedCriteria targetUserIds = DetachedCriteria.forClass(User.class)
                .add(Restrictions.eq(FIELD_USER_SESSION, userSession))
                .setProjection(Projections.property(UserDaoImpl.FIELD_ID));

        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_ID, transactionId))
                .add(Restrictions.eq(FIELD_VERIFIED, Boolean.FALSE))
                .add(Property.forName(FIELD_TARGET_PERSON_ID).in(targetUserIds));

        return (Transaction) criteria.uniqueResult();
    }

    @Override
    public boolean hasPersonTransactions(Long personId) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .createAlias(FIELD_PERSON_CONTACT, ALIAS_TARGET_PERSON)
                .add(Restrictions.eq(FIELD_TARGET_PERSON_ID, personId))
                .setProjection(Projections.count(FIELD_ID));

        return (Long) criteria.uniqueResult() > 0;
    }

    @Override
    public boolean isConfirmationCodeExists(String confirmationCode) {
        return (Long) getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_CONFIRMATION_CODE, confirmationCode))
                .setProjection(Projections.count(FIELD_CONFIRMATION_CODE)).uniqueResult() > 0;
    }
}
