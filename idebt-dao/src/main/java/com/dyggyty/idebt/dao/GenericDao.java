package com.dyggyty.idebt.dao;

import com.dyggyty.idebt.model.AbstractBean;

import java.io.Serializable;

/**
 * User: mobius
 * Date: 14.05.12
 * Time: 10:46
 */
public interface GenericDao<T extends AbstractBean> {

    /**
     * Retrieve object by ID.
     *
     * @param id Object identifier.
     * @return Object from database.
     */
    public T getById(Serializable id);

    /**
     * Save object
     *
     * @param obj Object to be stored.
     * @return Database identifier
     */
    public Serializable save(T obj);

    /**
     * Save or update object
     *
     * @param obj Object to be stored.
     * @return Database identifier
     */
    public Serializable saveOrUpdate(T obj);

    /**
     * Update specified entity.
     *
     * @param obj Object to be stored.
     */
    public void update(T obj);

    /**
     * Delete existing object.
     *
     * @param obj Object to be deleted.
     */
    public void delete(T obj);
}
