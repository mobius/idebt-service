package com.dyggyty.idebt.dao.impl;

import com.dyggyty.idebt.dao.PhoneDao;
import com.dyggyty.idebt.model.Phone;

/**
 * User: mobius
 * Date: 07.11.12
 * Time: 16:13
 */
public class PhoneDaoImpl extends GenericDaoImpl<Phone> implements PhoneDao {
    @Override
    protected Class<Phone> getEntityClass() {
        return Phone.class;
    }
}
