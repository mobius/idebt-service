package com.dyggyty.idebt.dao;

import com.dyggyty.idebt.model.User;

/**
 * User: mobius
 * Date: 05.06.12
 * Time: 15:54
 */
public interface UserDao extends GenericDao<User> {

    /**
     * Retrieve user by user name and encrypted password.
     *
     * @param userName User login name.
     * @param password Encrypted password.
     * @return User entity.
     */
    public User getUserByNameAndPassword(String userName, String password);

    /**
     * Check user exists.
     *
     * @param userName User login name.
     * @return TRUE if user exists, else false.
     */
    public boolean checkUserExists(String userName);

    /**
     * Retrieve user by login name.
     *
     * @param email User login name.
     * @return Existing user object.
     */
    public User getUserByLogin(String email);

    /**
     * Retrieve user by login name.
     *
     * @param userSession User session value.
     * @return Existing user object.
     */
    public User getUserBySession(String userSession);

    /**
     * Retrieve user password salt
     *
     * @param userName User login name
     * @return Salt
     */
    public String getUserSalt(String userName);

    /**
     * Retrieve user by verified user email.
     * Email should belong to one user else null value will return.
     *
     * @param email User email.
     * @return User entity bean.
     */
    public User getVerifiedUser(String email);

    /**
     * Retrieve user by confirmatio key.
     *
     * @param confirmationKey User confirmation key.
     * @return Existing user object.
     */
    public User getUserByConfirmationKey(String confirmationKey);

    public boolean isConfirmationCodeExists(String confirmationCode);
}
