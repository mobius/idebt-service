package com.dyggyty.idebt.dao.impl;

import com.dyggyty.idebt.dao.CurrencyDao;
import com.dyggyty.idebt.model.Currency;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 04.10.12
 * Time: 6:51
 */
@Transactional
public class CurrencyDaoImpl extends GenericDaoImpl<Currency> implements CurrencyDao {

    @Override
    protected Class<Currency> getEntityClass() {
        return Currency.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Currency> getAll() {
        return getCurrentSession().createCriteria(getEntityClass()).list();
    }
}
