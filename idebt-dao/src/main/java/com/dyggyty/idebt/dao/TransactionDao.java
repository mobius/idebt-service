package com.dyggyty.idebt.dao;

import com.dyggyty.idebt.model.Transaction;

import java.util.List;

/**
 * User: mobius
 * Date: 17.08.12
 * Time: 7:58
 */
public interface TransactionDao extends GenericDao<Transaction> {

    public List<Transaction> getUserTransactions(String userSession);

    public Transaction getUserTransaction(String userSession, Long transactionId);

    public Transaction getTransactionByConfirmationCode(String confirmationCode);

    public Transaction getNonConfirmedTransaction(String userSession, Long transactionId);

    /**
     * Check if person has transactions.
     *
     * @param personId Person database ID.
     * @return True if person has transactions else False.
     */
    public boolean hasPersonTransactions(Long personId);

    public boolean isConfirmationCodeExists(String confirmationCode);
}
