package com.dyggyty.idebt.dao;

import com.dyggyty.idebt.model.PersonContact;

import java.util.List;

/**
 * User: mobius
 * Date: 13.09.12
 * Time: 12:06
 */
public interface PersonContactDao extends GenericDao<PersonContact> {

    public List<PersonContact> getNonLinkedPersonsByEmail(String email);

    /**
     * Retrieve user contacts by user session.
     *
     * @param userSession User session identifier.
     * @return List of contacts.
     */
    public List<PersonContact> getUserContacts(String userSession);

    /**
     * Retrieve person by database identifier.
     *
     * @param session Current user session.
     * @param id      Database ID.
     * @return User entity.
     */
    public PersonContact getBySessionAndId(String session, Long id);

    /**
     * Retrieve person contact by owner and linked person.
     *
     * @param owner        Owner database identifier.
     * @param linkedPerson Linked person database identifier.
     * @return Person contact information.
     */
    public PersonContact getByOwnerAndLinkedPerson(Long owner, Long linkedPerson);
}
