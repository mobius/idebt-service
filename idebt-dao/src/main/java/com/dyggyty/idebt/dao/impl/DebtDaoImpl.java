package com.dyggyty.idebt.dao.impl;

import com.dyggyty.idebt.dao.DebtDao;
import com.dyggyty.idebt.model.Debt;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 04.10.12
 * Time: 5:48
 */
@Transactional
public class DebtDaoImpl extends GenericDaoImpl<Debt> implements DebtDao {

    private static final String FIELD_OWNER = "owner";
    private static final String FIELD_PERSON_CONTACT = "personContact";
    private static final String FIELD_CURRENCY = "currency";

    private static final String ALIAS_USER = "user";
    private static final String ALIAS_PERSON = "person";
    private static final String ALIAS_CURRENCY = "curr";

    private static final String FIELD_USER_SESSION = ALIAS_USER + ".session";
    private static final String FIELD_USER_ID = ALIAS_USER + ".id";
    private static final String FIELD_PERSON_ID = ALIAS_PERSON + ".id";
    private static final String FIELD_PERSON_DELETED = ALIAS_PERSON + ".deleted";
    private static final String FIELD_CURRENCY_CODE = ALIAS_CURRENCY + ".id";

    @Override
    protected Class<Debt> getEntityClass() {
        return Debt.class;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Debt> getUserDebts(String userSession) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .createAlias(FIELD_PERSON_CONTACT, ALIAS_PERSON)
                .add(Restrictions.eq(FIELD_PERSON_DELETED, Boolean.FALSE))
                .createAlias(FIELD_OWNER, ALIAS_USER)
                .add(Restrictions.eq(FIELD_USER_SESSION, userSession));
        return criteria.list();
    }

    @Override
    public Debt getDebts(Long ownerId, Long targetPersonId, String currencyCode) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .createAlias(FIELD_OWNER, ALIAS_USER)
                .add(Restrictions.eq(FIELD_USER_ID, ownerId))
                .createAlias(FIELD_PERSON_CONTACT, ALIAS_PERSON)
                .add(Restrictions.eq(FIELD_PERSON_DELETED, Boolean.FALSE))
                .add(Restrictions.eq(FIELD_PERSON_ID, targetPersonId))
                .createAlias(FIELD_CURRENCY, ALIAS_CURRENCY)
                .add(Restrictions.eq(FIELD_CURRENCY_CODE, currencyCode));
        return (Debt) criteria.uniqueResult();
    }
}
