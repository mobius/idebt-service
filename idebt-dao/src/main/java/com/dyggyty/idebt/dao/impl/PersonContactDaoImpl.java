package com.dyggyty.idebt.dao.impl;

import com.dyggyty.idebt.dao.PersonContactDao;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: mobius
 * Date: 13.09.12
 * Time: 12:07
 */
@Transactional
public class PersonContactDaoImpl extends GenericDaoImpl<PersonContact> implements PersonContactDao {

    private static final String FIELD_ID = "id";
    private static final String FIELD_EMAILS = "emails";
    private static final String FIELD_CONTACT_INFO = "contactInfo";
    private static final String FIELD_OWNER = "owner";
    private static final String FIELD_LINKED_USER = "linkedUser";
    private static final String FIELD_DELETED = "deleted";

    private static final String ALIAS_EMAIL = "email";
    private static final String ALIAS_PERSON_OWNER = "personOwner";
    private static final String ALIAS_LINKED_USER = "lUser";

    private static final String FIELD_EMAIL = ALIAS_EMAIL + "." + FIELD_CONTACT_INFO;
    private static final String FIELD_OWNER_ID = ALIAS_PERSON_OWNER + "." + FIELD_ID;
    private static final String FIELD_LINKED_USER_ID = ALIAS_LINKED_USER + "." + FIELD_ID;

    @Override
    protected Class<PersonContact> getEntityClass() {
        return PersonContact.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PersonContact> getNonLinkedPersonsByEmail(String email) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.isNull(FIELD_LINKED_USER))
                .createAlias(FIELD_EMAILS, ALIAS_EMAIL, JoinType.LEFT_OUTER_JOIN)
                .add(Restrictions.eq(FIELD_EMAIL, email));

        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PersonContact> getUserContacts(String userSession) {
        DetachedCriteria ownerIds = DetachedCriteria.forClass(User.class).
                add(Restrictions.eq(UserDaoImpl.FIELD_SESSION, userSession)).
                setProjection(Projections.property(UserDaoImpl.FIELD_ID));

        Criteria criteria = getCurrentSession().createCriteria(getEntityClass()).
                add(Restrictions.eq(FIELD_DELETED, Boolean.FALSE)).
                createAlias(FIELD_OWNER, ALIAS_PERSON_OWNER).
                add(Property.forName(FIELD_OWNER_ID).in(ownerIds));

        return criteria.list();
    }

    @Override
    public PersonContact getBySessionAndId(String session, Long id) {
        DetachedCriteria ownerIds = DetachedCriteria.forClass(User.class).
                add(Restrictions.eq(UserDaoImpl.FIELD_SESSION, session)).
                setProjection(Projections.property(UserDaoImpl.FIELD_ID));

        Criteria criteria = getCurrentSession().createCriteria(getEntityClass()).
                add(Restrictions.eq(FIELD_ID, id)).
                add(Restrictions.eq(FIELD_DELETED, Boolean.FALSE)).
                createAlias(FIELD_OWNER, ALIAS_PERSON_OWNER).
                add(Property.forName(FIELD_OWNER_ID).in(ownerIds));

        return (PersonContact) criteria.uniqueResult();
    }

    @Override
    public PersonContact getByOwnerAndLinkedPerson(Long owner, Long linkedPerson) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass()).
                add(Restrictions.eq(FIELD_DELETED, Boolean.FALSE)).
                createAlias(FIELD_OWNER, ALIAS_PERSON_OWNER).
                createAlias(FIELD_LINKED_USER, ALIAS_LINKED_USER).
                add(Restrictions.eq(FIELD_OWNER_ID, owner)).
                add(Restrictions.eq(FIELD_LINKED_USER_ID, linkedPerson));
        return (PersonContact) criteria.uniqueResult();
    }
}
