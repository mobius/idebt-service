package com.dyggyty.idebt.dao;

import com.dyggyty.idebt.model.Phone;

/**
 * User: mobius
 * Date: 07.11.12
 * Time: 16:12
 */
public interface PhoneDao extends GenericDao<Phone> {
}
