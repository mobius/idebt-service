package com.dyggyty.idebt.dao;

import com.dyggyty.idebt.model.Currency;

import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 04.10.12
 * Time: 6:50
 */
public interface CurrencyDao extends GenericDao<Currency> {

    /**
     * Get all currencies.
     *
     * @return List of currencies.
     */
    public List<Currency> getAll();
}
