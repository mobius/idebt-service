package com.dyggyty.idebt.dao.impl;

import com.dyggyty.idebt.dao.UserDao;
import com.dyggyty.idebt.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: mobius
 * Date: 05.06.12
 * Time: 15:55
 */
@Transactional
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

    public static final String FIELD_ID = "id";
    public static final String FIELD_SESSION = "session";
    private static final String FIELD_USERNAME = "username";
    private static final String FIELD_PASSWORD = "password";
    private static final String FIELD_SALT = "salt";
    private static final String FIELD_CONTACT_INFO = "contactInfo";
    private static final String FIELD_VERIFICATION_CODE = "verificationCode";
    private static final String FIELD_VERIFIED = "verified";

    private static final String ALIAS_EMAIL = "email";

    @Override
    protected Class<User> getEntityClass() {
        return User.class;
    }

    @Override
    public User getUserByNameAndPassword(String userName, String password) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_USERNAME, userName))
                .add(Restrictions.eq(FIELD_PASSWORD, password));

        List users = criteria.list();
        return (User) (users.size() > 0 ? users.get(0) : null);
    }

    @Override
    public boolean checkUserExists(String userName) {
        return getUserByLogin(userName) != null;
    }

    @Override
    public User getUserByLogin(String email) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_USERNAME, email));
        return (User) criteria.uniqueResult();
    }

    @Override
    public User getUserBySession(String userSession) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_SESSION, userSession));
        return (User) criteria.uniqueResult();
    }

    @Override
    public String getUserSalt(String userName) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_USERNAME, userName))
                .setProjection(Projections.property(FIELD_SALT));
        return (String) criteria.uniqueResult();
    }

    @Override
    public User getVerifiedUser(String email) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_USERNAME, email))
                .add(Restrictions.eq(FIELD_VERIFIED, Boolean.TRUE));

        return (User) criteria.uniqueResult();
    }

    @Override
    public User getUserByConfirmationKey(String confirmationKey) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_VERIFICATION_CODE, confirmationKey));
        return (User) criteria.uniqueResult();
    }

    @Override
    public boolean isConfirmationCodeExists(String confirmationCode) {
        return (Long) getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_VERIFICATION_CODE, confirmationCode))
                .setProjection(Projections.count(FIELD_VERIFICATION_CODE)).uniqueResult() > 0;
    }
}
