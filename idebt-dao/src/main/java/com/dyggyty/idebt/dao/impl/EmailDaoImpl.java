package com.dyggyty.idebt.dao.impl;

import com.dyggyty.idebt.dao.EmailDao;
import com.dyggyty.idebt.model.Email;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: mobius
 * Date: 14.10.12
 * Time: 13:53
 */
@Transactional
public class EmailDaoImpl extends GenericDaoImpl<Email> implements EmailDao {

    private static final String FIELD_CONFIRMATION_CODE = "confirmationCode";
    public static final String FIELD_VERIFIED = "verified";

    @Override
    protected Class<Email> getEntityClass() {
        return Email.class;
    }

    @Override
    public Email getByConfirmationCode(String confirmationCode) {
        Criteria criteria = getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_CONFIRMATION_CODE, confirmationCode));
        return (Email) criteria.uniqueResult();
    }

    @Override
    public boolean isConfirmationCodeExists(String confirmationCode) {
        return (Long) getCurrentSession().createCriteria(getEntityClass())
                .add(Restrictions.eq(FIELD_CONFIRMATION_CODE, confirmationCode))
                .setProjection(Projections.count(FIELD_CONFIRMATION_CODE)).uniqueResult() > 0;
    }
}
