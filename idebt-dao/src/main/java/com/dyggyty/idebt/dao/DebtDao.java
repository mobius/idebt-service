package com.dyggyty.idebt.dao;

import com.dyggyty.idebt.model.Debt;
import com.dyggyty.idebt.model.User;

import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 04.10.12
 * Time: 5:47
 */
public interface DebtDao extends GenericDao<Debt> {

    /**
     * Retreive user debts.
     *
     * @param userSession User session.
     * @return List of user debts.
     */
    public List<Debt> getUserDebts(String userSession);

    /**
     * Retrieve debt by owner and target person.
     *
     * @param ownerId        Owner database identifier.
     * @param targetPersonId Target person database identifier.
     * @param currencyCode   Debt currency code.
     * @return Debt instance.
     */
    public Debt getDebts(Long ownerId, Long targetPersonId, String currencyCode);
}
