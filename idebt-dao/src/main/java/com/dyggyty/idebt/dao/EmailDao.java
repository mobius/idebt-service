package com.dyggyty.idebt.dao;

import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.Person;

/**
 * User: mobius
 * Date: 14.10.12
 * Time: 13:52
 */
public interface EmailDao extends GenericDao<Email> {

    public Email getByConfirmationCode(String confirmationCode);

    public boolean isConfirmationCodeExists(String confirmationCode);
}
