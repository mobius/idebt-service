CREATE TABLE currency
(
  id character varying(3) NOT NULL,
  title_key character varying(64) NOT NULL,
  CONSTRAINT currency_pkey PRIMARY KEY (id)
);

CREATE TABLE person
(
  id bigserial NOT NULL,
  person_name character varying(128) NOT NULL,
  person_owner bigint,
  CONSTRAINT person_pkey PRIMARY KEY (id)
);

CREATE TABLE users
(
  id bigint NOT NULL,
  last_login timestamp without time zone,
  created timestamp without time zone NOT NULL,
  username character varying(64) NOT NULL,
  user_password character varying(128) NOT NULL,
  salt character varying(128) NOT NULL,
  verified boolean NOT NULL,
  user_session character varying(128),
  verification_code character varying(128),
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT fk6a68e08c0adc283 FOREIGN KEY (id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

ALTER TABLE person
  ADD CONSTRAINT fkc4e39b55fbabfc3d FOREIGN KEY (person_owner)
        REFERENCES users (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE contact
(
  id bigserial NOT NULL,
  contact_info character varying(255) NOT NULL,
  verified boolean NOT NULL,
  confirmation_code character varying(128),
  person_id bigint NOT NULL,
  CONSTRAINT contact_pkey PRIMARY KEY (id),
  CONSTRAINT fk38b72420c0adc283 FOREIGN KEY (person_id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE email
(
  id bigint NOT NULL,
  person_id bigint,
  CONSTRAINT email_pkey PRIMARY KEY (id),
  CONSTRAINT fk5c24b9c960f8df1 FOREIGN KEY (id)
      REFERENCES contact (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk5c24b9cc0adc283 FOREIGN KEY (person_id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE phone
(
  id bigint NOT NULL,
  person_id bigint,
  CONSTRAINT phone_pkey PRIMARY KEY (id),
  CONSTRAINT fk65b3d6e960f8df1 FOREIGN KEY (id)
      REFERENCES contact (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk65b3d6ec0adc283 FOREIGN KEY (person_id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE debt
(
  id bigserial NOT NULL,
  amount double precision NOT NULL,
  target_person_id bigint NOT NULL,
  currency character varying(3) NOT NULL,
  owner_id bigint NOT NULL,
  CONSTRAINT debt_pkey PRIMARY KEY (id),
  CONSTRAINT fk2efc9371683d1b FOREIGN KEY (owner_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk2efc938fd7d6cb FOREIGN KEY (currency)
      REFERENCES currency (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk2efc93b65d2d55 FOREIGN KEY (target_person_id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE "transaction"
(
  id bigserial NOT NULL,
  amount double precision NOT NULL,
  created timestamp without time zone NOT NULL,
  duration integer NOT NULL,
  verified boolean NOT NULL,
  confirmation_code character varying(128),
  closed timestamp without time zone,
  owner_id bigint NOT NULL,
  target_person_id bigint NOT NULL,
  currency character varying(3) NOT NULL,
  CONSTRAINT transaction_pkey PRIMARY KEY (id),
  CONSTRAINT fk7fa0d2de71683d1b FOREIGN KEY (owner_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk7fa0d2de8fd7d6cb FOREIGN KEY (currency)
      REFERENCES currency (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk7fa0d2deb65d2d55 FOREIGN KEY (target_person_id)
      REFERENCES person (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
