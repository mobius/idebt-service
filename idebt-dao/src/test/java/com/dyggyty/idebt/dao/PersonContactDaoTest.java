package com.dyggyty.idebt.dao;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

/**
 * User: vitaly.rudenya
 * Date: 09.10.12
 * Time: 4:06
 */
@ContextConfiguration(locations = {"classpath:dao-context.xml"})
public class PersonContactDaoTest extends AbstractTransactionalJUnit4SpringContextTests {

    @Test
    @Rollback
    public void testGetUserContacts() {
        PersonContactDao personContactDao = applicationContext.getBean(PersonContactDao.class);
        personContactDao.getUserContacts("ba869c07-a0d4-42c8-9ca1-d56ba0aa4494");
    }
}
