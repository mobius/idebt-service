package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 10.10.12
 * Time: 15:56
 */
@XmlRootElement(name = "changeUserInfoResponse")
public class ChangeUserInfoResponse extends GenericResponse {
}
