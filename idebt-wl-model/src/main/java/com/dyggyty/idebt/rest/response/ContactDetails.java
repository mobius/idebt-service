package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * User: mobius
 * Date: 21.10.12
 * Time: 4:29
 */
@XmlRootElement(name = "contactDetails")
public class ContactDetails {
    private Long id;
    private String externalId;
    private String name;
    private String linkedContactName;
    private Long linkedContactId;
    private List<String> phones;
    private List<String> emails;

    public ContactDetails() {
    }

    public ContactDetails(Long id, String externalId, String name, String linkedContactName, Long linkedContactId,
                          List<String> phones, List<String> emails) {
        this.id = id;
        this.externalId = externalId;
        this.name = name;
        this.linkedContactName = linkedContactName;
        this.linkedContactId = linkedContactId;
        this.phones = phones;
        this.emails = emails;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLinkedContactName() {
        return linkedContactName;
    }

    public void setLinkedContactName(String linkedContactName) {
        this.linkedContactName = linkedContactName;
    }

    public Long getLinkedContactId() {
        return linkedContactId;
    }

    public void setLinkedContactId(Long linkedContactId) {
        this.linkedContactId = linkedContactId;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    public Boolean getHasContacts() {
        return (emails != null && emails.size() > 0) || (phones != null && phones.size() > 0);
    }
}
