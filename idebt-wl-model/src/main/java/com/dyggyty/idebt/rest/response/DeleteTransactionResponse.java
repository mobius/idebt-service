package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 05.11.12
 * Time: 15:30
 */
@XmlRootElement(name = "addTransactionResponse")
public class DeleteTransactionResponse extends GenericResponse {
}
