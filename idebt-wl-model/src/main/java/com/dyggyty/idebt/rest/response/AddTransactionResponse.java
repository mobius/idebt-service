package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 10.08.12
 * Time: 14:03
 */
@XmlRootElement(name = "addTransactionResponse")
public class AddTransactionResponse extends GenericResponse {
}
