package com.dyggyty.idebt.rest.response;

import com.dyggyty.idebt.model.Currency;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * User: mobius
 * Date: 09.10.12
 * Time: 14:53
 */
@XmlRootElement(name = "getCurrencyResponse")
public class GetCurrencyResponse extends GenericResponse {

    private List<CurrencyDetail> currencyDetails = new ArrayList<CurrencyDetail>();

    public List<CurrencyDetail> getCurrencyDetails() {
        return currencyDetails;
    }

    public void setCurrencyDetails(List<CurrencyDetail> currencyDetails) {
        this.currencyDetails = currencyDetails;
    }

    public void addCurrencyDetail(Currency currency) {
        currencyDetails.add(new CurrencyDetail(currency.getId(), currency.getTitleKey()));
    }

    @XmlRootElement(name = "currencyDetail")
    public static class CurrencyDetail {
        private String code;
        private String titleKey;

        public CurrencyDetail() {
        }

        public CurrencyDetail(String code, String titleKey) {
            this.code = code;
            this.titleKey = titleKey;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitleKey() {
            return titleKey;
        }

        public void setTitleKey(String titleKey) {
            this.titleKey = titleKey;
        }
    }
}
