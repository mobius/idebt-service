package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 06.08.12
 * Time: 16:11
 */
@XmlRootElement(name = "registerUserResponse")
public class RegisterUserResponse extends GenericResponse {
}
