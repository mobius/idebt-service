package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 11.10.12
 * Time: 13:08
 */
@XmlRootElement(name = "changeUserPasswordResponse")
public class ChangeUserPasswordResponse extends GenericResponse {
}
