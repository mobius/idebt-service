package com.dyggyty.idebt.rest.response;

import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Phone;
import com.dyggyty.idebt.model.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 09.10.12
 * Time: 1:34
 */
@XmlRootElement(name = "getUserContactsResponse")
public class GetUserContactsResponse extends GenericResponse {

    private List<ContactDetails> contactDetails = new ArrayList<ContactDetails>();

    public List<ContactDetails> getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(List<ContactDetails> contactDetails) {
        this.contactDetails = contactDetails;
    }

    public void addContactDetails(PersonContact personContact) {
        List<String> phones = new ArrayList<String>();
        if (personContact.getPhones() != null) {
            for (Phone phone : personContact.getPhones()) {
                phones.add(phone.getContactInfo());
            }
        }

        List<String> emails = new ArrayList<String>();
        if (personContact.getEmails() != null) {
            for (Email email : personContact.getEmails()) {
                emails.add(email.getContactInfo());
            }
        }

        User linkedContact = personContact.getLinkedUser();

        contactDetails.add(new ContactDetails(personContact.getId(), personContact.getExternalId(),
                personContact.getName(), linkedContact == null ? null : linkedContact.getName(),
                linkedContact == null ? null : linkedContact.getId(), phones, emails));
    }
}
