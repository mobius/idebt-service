package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 23.10.12
 * Time: 14:05
 */
@XmlRootElement(name = "forgotUserPasswordResponse")
public class ForgotUserPasswordResponse extends GenericResponse {
}
