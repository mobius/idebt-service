package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * User: mobius
 * Date: 21.10.12
 * Time: 11:24
 */
@XmlRootElement(name = "transactionDetail")
public class TransactionDetail {
    private Long transactionId;
    private String personContact;
    private Long personContactId;
    private String linkedUser;
    private Long linkedUserId;
    private String owner;
    private Long ownerId;
    private Double amount;
    private Date created;
    private Date finalDate;
    private Boolean verified;
    private Date closed;
    private String currency;
    private String currencyCode;
    private String comment;

    public TransactionDetail() {
    }

    public TransactionDetail(Long transactionId, String personContact, Long personContactId, String linkedUser,
                             Long linkedUserId, String owner, Long ownerId, Double amount, Date created,
                             Date finalDate, Boolean verified, Date closed, String currency, String currencyCode,
                             String comment) {
        this.transactionId = transactionId;
        this.personContact = personContact;
        this.personContactId = personContactId;
        this.linkedUser = linkedUser;
        this.linkedUserId = linkedUserId;
        this.owner = owner;
        this.ownerId = ownerId;
        this.amount = amount;
        this.created = created;
        this.finalDate = finalDate;
        this.verified = verified;
        this.closed = closed;
        this.currency = currency;
        this.currencyCode = currencyCode;
        this.comment = comment;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getPersonContact() {
        return personContact;
    }

    public void setPersonContact(String personContact) {
        this.personContact = personContact;
    }

    public Long getPersonContactId() {
        return personContactId;
    }

    public void setPersonContactId(Long personContactId) {
        this.personContactId = personContactId;
    }

    public String getLinkedUser() {
        return linkedUser;
    }

    public void setLinkedUser(String linkedUser) {
        this.linkedUser = linkedUser;
    }

    public Long getLinkedUserId() {
        return linkedUserId;
    }

    public void setLinkedUserId(Long linkedUserId) {
        this.linkedUserId = linkedUserId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public Date getClosed() {
        return closed;
    }

    public void setClosed(Date closed) {
        this.closed = closed;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
