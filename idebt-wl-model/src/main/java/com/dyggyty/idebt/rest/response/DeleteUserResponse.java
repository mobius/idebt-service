package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 21.10.12
 * Time: 9:36
 */
@XmlRootElement(name = "deleteUserResponse")
public class DeleteUserResponse extends GenericResponse {
}
