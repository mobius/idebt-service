package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 15.10.12
 * Time: 16:23
 */
@XmlRootElement(name = "confirmTransactionResponse")
public class ConfirmTransactionResponse extends GenericResponse {
}
