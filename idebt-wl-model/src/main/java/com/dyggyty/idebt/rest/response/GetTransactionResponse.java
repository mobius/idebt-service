package com.dyggyty.idebt.rest.response;

import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Transaction;
import com.dyggyty.idebt.model.User;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 21.10.12
 * Time: 11:23
 */
@XmlRootElement(name = "getTransactionResponse")
public class GetTransactionResponse extends GenericResponse {
    private TransactionDetail transactionDetails;

    public TransactionDetail getTransactionDetails() {
        return transactionDetails;
    }

    public void setTransactionDetails(TransactionDetail transactionDetails) {
        this.transactionDetails = transactionDetails;
    }

    public void setTransactionDetails(Transaction transaction) {

        PersonContact targetPerson = transaction.getPersonContact();
        User owner = transaction.getOwner();
        User linkedUser = null;

        if (targetPerson != null) {
            linkedUser = targetPerson.getLinkedUser();
        }

        transactionDetails = new TransactionDetail(
                transaction.getId(),
                targetPerson == null ? null : targetPerson.getName(),
                targetPerson == null ? null : targetPerson.getId(),
                linkedUser == null ? null : linkedUser.getName(),
                linkedUser == null ? null : linkedUser.getId(),
                owner.getName(),
                owner.getId(),
                transaction.getAmount(),
                transaction.getCreated(),
                transaction.getFinalDate(),
                transaction.getVerified(),
                transaction.getClosed(),
                transaction.getCurrency().getTitleKey(),
                transaction.getCurrency().getId(),
                transaction.getComment()
        );
    }
}
