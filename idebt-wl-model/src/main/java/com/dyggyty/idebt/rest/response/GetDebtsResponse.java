package com.dyggyty.idebt.rest.response;

import com.dyggyty.idebt.model.Debt;
import com.dyggyty.idebt.model.PersonContact;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * User: mobius
 * Date: 13.09.12
 * Time: 12:11
 */
@XmlRootElement(name = "getDebtsResponse")
public class GetDebtsResponse extends GenericResponse {

    private List<DebtDetail> debtDetails = new ArrayList<DebtDetail>();

    public void setDebtDetails(List<DebtDetail> debtDetails) {
        this.debtDetails = debtDetails;
    }

    public List<DebtDetail> getDebtDetails() {
        return debtDetails;
    }

    public void addDebt(Debt debt) {
        PersonContact targetPerson = debt.getPersonContact();
        debtDetails.add(new DebtDetail(
                targetPerson.getName(),
                targetPerson.getId(),
                debt.getAmount(),
                debt.getCurrency().getTitleKey(),
                debt.getCurrency().getId()
        ));
    }

    @XmlRootElement(name = "debtDetail")
    public static class DebtDetail {
        private String targetPerson;
        private Long targetPersonId;
        private Double amount;
        private String currency;
        private String currencyCode;

        public DebtDetail() {
        }

        public DebtDetail(String targetPerson, Long targetPersonId, Double amount, String currency, String currencyCode) {
            this.targetPerson = targetPerson;
            this.targetPersonId = targetPersonId;
            this.amount = amount;
            this.currency = currency;
            this.currencyCode = currencyCode;
        }

        public String getTargetPerson() {
            return targetPerson;
        }

        public void setTargetPerson(String targetPerson) {
            this.targetPerson = targetPerson;
        }

        public Long getTargetPersonId() {
            return targetPersonId;
        }

        public void setTargetPersonId(Long targetPersonId) {
            this.targetPersonId = targetPersonId;
        }

        public Double getAmount() {
            return amount;
        }

        public Double getPositiveAmount() {
            return Math.abs(amount);
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }
    }
}
