package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: mobius
 * Date: 06.10.12
 * Time: 15:18
 */
@XmlRootElement(name = "checkUserResponse")
public class CheckUserResponse extends GenericResponse {
    private Boolean userExists;

    public Boolean getUserExists() {
        return userExists;
    }

    public void setUserExists(Boolean userExists) {
        this.userExists = userExists;
    }
}
