package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * User: mobius
 * Date: 21.10.12
 * Time: 9:26
 */
@XmlRootElement(name = "getUserInfoResponse")
public class GetUserInfoResponse extends GenericResponse {
    private String nickName;
    private List<String> userEmails;
    private List<String> userPhones;
    private String userName;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<String> getUserEmails() {
        return userEmails;
    }

    public void setUserEmails(List<String> userEmails) {
        this.userEmails = userEmails;
    }

    public List<String> getUserPhones() {
        return userPhones;
    }

    public void setUserPhones(List<String> userPhones) {
        this.userPhones = userPhones;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
