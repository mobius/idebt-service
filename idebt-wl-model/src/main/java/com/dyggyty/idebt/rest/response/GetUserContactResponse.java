package com.dyggyty.idebt.rest.response;

import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Phone;
import com.dyggyty.idebt.model.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * User: mobius
 * Date: 21.10.12
 * Time: 4:30
 */
@XmlRootElement(name = "getUserContactsResponse")
public class GetUserContactResponse extends GenericResponse {
    private ContactDetails contactDetails;

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    public void setContactDetails(PersonContact personContact) {
        List<String> phones = new ArrayList<String>();
        if (personContact.getPhones() != null) {
            for (Phone phone : personContact.getPhones()) {
                phones.add(phone.getContactInfo());
            }
        }

        List<String> emails = new ArrayList<String>();
        if (personContact.getEmails() != null) {
            for (Email email : personContact.getEmails()) {
                emails.add(email.getContactInfo());
            }
        }

        User linkedContact = personContact.getLinkedUser();

        contactDetails = new ContactDetails(personContact.getId(), personContact.getExternalId(),
                personContact.getName(), linkedContact == null ? null : linkedContact.getName(),
                linkedContact == null ? null : linkedContact.getId(), phones, emails);
    }

}
