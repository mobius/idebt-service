package com.dyggyty.idebt.rest.response;

import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Transaction;
import com.dyggyty.idebt.model.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * User: mobius
 * Date: 01.09.12
 * Time: 12:59
 */
@XmlRootElement(name = "getTransactionsResponse")
public class GetTransactionsResponse extends GenericResponse {

    private List<TransactionDetail> transactionDetails = new ArrayList<TransactionDetail>();

    public List<TransactionDetail> getTransactionDetails() {
        return transactionDetails;
    }

    public void setTransactionDetails(List<TransactionDetail> transactionDetails) {
        this.transactionDetails = transactionDetails;
    }

    public void addTransaction(String userSession, Transaction transaction) {

        PersonContact targetPerson;
        User owner;
        User linkedUser = null;
        if (userSession.equals(transaction.getOwner().getSession())) {
            targetPerson = transaction.getPersonContact();
            if (targetPerson != null) {
                linkedUser = targetPerson.getLinkedUser();
            }
            owner = null;
        } else {
            targetPerson = null;
            owner = transaction.getOwner();
        }

        transactionDetails.add(new TransactionDetail(
                transaction.getId(),
                targetPerson == null ? null : targetPerson.getName(),
                targetPerson == null ? null : targetPerson.getId(),
                linkedUser == null ? null : linkedUser.getName(),
                linkedUser == null ? null : linkedUser.getId(),
                owner == null ? null : owner.getName(),
                owner == null ? null : owner.getId(),
                transaction.getAmount(),
                transaction.getCreated(),
                transaction.getFinalDate(),
                transaction.getVerified(),
                transaction.getClosed(),
                transaction.getCurrency().getTitleKey(),
                transaction.getCurrency().getId(),
                transaction.getComment()
        ));
    }
}
