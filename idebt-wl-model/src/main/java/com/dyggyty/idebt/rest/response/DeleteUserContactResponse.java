package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: vitaly.rudenya
 * Date: 17.10.12
 * Time: 11:55
 */
@XmlRootElement(name = "deleteUserContactResponse")
public class DeleteUserContactResponse extends GenericResponse{
}
