package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: vitaly.rudenya
 * Date: 09.10.12
 * Time: 1:34
 */
@XmlRootElement(name = "saveUserContactResponse")
public class SaveUserContactResponse extends GenericResponse {

    private Long personId;

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }
}
