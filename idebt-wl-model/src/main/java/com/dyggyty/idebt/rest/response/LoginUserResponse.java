package com.dyggyty.idebt.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * User: 1
 * Date: 22.07.12
 * Time: 23:04
 */
@XmlRootElement(name = "loginUserResponse")
public class LoginUserResponse extends GenericResponse {

    private String userSessionId;

    public String getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(String userSessionId) {
        this.userSessionId = userSessionId;
    }
}
