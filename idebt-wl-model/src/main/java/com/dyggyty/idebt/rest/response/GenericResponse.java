package com.dyggyty.idebt.rest.response;

import com.dyggyty.idebt.utils.ErrorMessages;

/**
 * User: mobius
 * Date: 06.08.12
 * Time: 16:07
 */
public abstract class GenericResponse {

    private String errorMessageKey = ErrorMessages.STATUS_OK;

    public String getErrorMessageKey() {
        return errorMessageKey;
    }

    public void setErrorMessageKey(String errorMessageKey) {
        this.errorMessageKey = errorMessageKey;
    }
}
