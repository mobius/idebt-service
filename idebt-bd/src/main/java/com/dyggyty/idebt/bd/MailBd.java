package com.dyggyty.idebt.bd;

import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.Transaction;
import com.dyggyty.idebt.model.User;

/**
 * User: mobius
 * Date: 13.10.12
 * Time: 11:55
 */
public interface MailBd {

    public void sendEmailConfirmationMessage(Long emailId);

    public void sendEmailConfirmationMessage(Email email);

    public void sendTransactionConfirmationMessage(Long transactionId);

    public void sendTransactionConfirmationMessage(Transaction transaction);

    public void sendForgotPasswordMessage(User user);
}
