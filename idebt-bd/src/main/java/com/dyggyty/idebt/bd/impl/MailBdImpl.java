package com.dyggyty.idebt.bd.impl;

import com.dyggyty.idebt.bd.MailBd;
import com.dyggyty.idebt.dao.EmailDao;
import com.dyggyty.idebt.dao.TransactionDao;
import com.dyggyty.idebt.dao.UserDao;
import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Transaction;
import com.dyggyty.idebt.model.User;
import com.dyggyty.idebt.wl.RequestParams;
import com.sun.mail.smtp.SMTPTransport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Set;
import java.util.UUID;

/**
 * User: mobius
 * Date: 13.10.12
 * Time: 12:00
 */
public class MailBdImpl implements MailBd {

    private static final String EMAIL_CONFIRMATION_TITLE = "Email confirmation";
    private static final String TRANSACTION_CONFIRMATION_TITLE = "Transaction confirmation";
    private static final String FORGOT_PASSWORD_TITLE = "Forgot password";

    private static final MessageFormat EMAIL_CONFIRMATION_MESSAGE =
            new MessageFormat(getFileContent("/emailConfirmationMessage.html"));
    private static final MessageFormat TRANSACTION_CONFIRMATION_MESSAGE =
            new MessageFormat(getFileContent("/transactionConfirmationMessage.html"));
    private static final MessageFormat FORGOT_PASSWORD_MESSAGE =
            new MessageFormat(getFileContent("/forgotPasswordMessage.html"));

    private static final Log LOGGER = LogFactory.getLog(MailBdImpl.class);

    private EmailDao emailDao;
    private TransactionDao transactionDao;
    private UserDao userDao;

    private String serverExternalHost;
    private String webClient;

    private String fromAddress;
    private String smtpHost;
    private Integer smtpPort;
    private String smtpUser;
    private String smtpPassword;

    @Override
    public void sendEmailConfirmationMessage(Long emailId) {
        Email email = emailDao.getById(emailId);
        if (email != null) {
            sendEmailConfirmationMessage(email);
        }
    }

    @Override
    public void sendEmailConfirmationMessage(Email email) {

        String uuid;

        do {
            uuid = UUID.randomUUID().toString();
        } while (emailDao.isConfirmationCodeExists(uuid));

        email.setConfirmationCode(uuid);
        emailDao.update(email);

        String content = EMAIL_CONFIRMATION_MESSAGE.format(new Object[]{serverExternalHost, uuid});

        sendMail(email.getContactInfo(), content, EMAIL_CONFIRMATION_TITLE);
    }

    @Override
    public void sendTransactionConfirmationMessage(Long transactionId) {
        Transaction transaction = transactionDao.getById(transactionId);
        if (transaction != null) {
            sendTransactionConfirmationMessage(transaction);
        }
    }

    @Override
    public void sendTransactionConfirmationMessage(Transaction transaction) {

        PersonContact transactionPerson = transaction.getPersonContact();
        Set<Email> transactionPersonEmails = transactionPerson.getEmails();

        if (transactionPersonEmails != null && transactionPersonEmails.size() > 0) {
            String uuid;
            do {
                uuid = UUID.randomUUID().toString();
            } while (transactionDao.isConfirmationCodeExists(uuid));

            transaction.setConfirmationCode(uuid);
            transactionDao.update(transaction);

            String content = TRANSACTION_CONFIRMATION_MESSAGE.format(new Object[]{serverExternalHost, uuid});
            Email transactionPersonEmail = transactionPersonEmails.iterator().next();

            String email = transactionPersonEmail.getContactInfo();
            if (email != null && email.trim().length() != 0) {
                sendMail(email, content, TRANSACTION_CONFIRMATION_TITLE);
            }
        }
    }

    @Transactional
    @Override
    public void sendForgotPasswordMessage(User user) {
        Set<Email> userEmails = user.getEmails();

        if (userEmails != null && userEmails.size() > 0) {
            String uuid;
            do {
                uuid = UUID.randomUUID().toString();
            } while (userDao.isConfirmationCodeExists(uuid));

            user.setVerificationCode(uuid);
            userDao.update(user);

            String content = FORGOT_PASSWORD_MESSAGE.format(new Object[]{serverExternalHost, webClient,
                    RequestParams.PARAM_CONFIRMATION_KEY, uuid});
            Email transactionPersonEmail = userEmails.iterator().next();

            sendMail(transactionPersonEmail.getContactInfo(), content, FORGOT_PASSWORD_TITLE);
        }
    }

    public void setEmailDao(EmailDao emailDao) {
        this.emailDao = emailDao;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    public void setSmtpPort(Integer smtpPort) {
        this.smtpPort = smtpPort;
    }

    public void setSmtpUser(String smtpUser) {
        this.smtpUser = smtpUser;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public void setTransactionDao(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setServerExternalHost(String serverExternalHost) {
        this.serverExternalHost = serverExternalHost;
    }

    public void setWebClient(String webClient) {
        this.webClient = webClient;
    }

    private void sendMail(String toEmail, String content, String subject) {
        String to[] = new String[]{toEmail};

        Session session = createSession();

        try {
            Message message = setMessage(session, to, subject, content);
            Transport transport = setSMTPServer(session, smtpHost, smtpPort, smtpUser, smtpPassword);

            send(transport, message);
        } catch (MessagingException e) {
            LOGGER.error("Fail email sending", e);
        }
    }

    private Session createSession() {
        return Session.getInstance(System.getProperties());
    }

    private Message setMessage(Session session, String[] toAddresses, String subject, String content) throws MessagingException {

        Message message = new MimeMessage(session);

        message.setFrom(new InternetAddress(fromAddress));
        InternetAddress[] toIntAdds = new InternetAddress[toAddresses.length];

        for (int i = 0; i < toAddresses.length; i++)
            toIntAdds[i] = new InternetAddress(toAddresses[i]);

        message.setRecipients(Message.RecipientType.TO, toIntAdds);
        message.setSubject(subject);
        message.setSentDate(new java.util.Date());
        message.setContent(content, "text/html");

        return message;
    }

    private Transport setSMTPServer(Session session, String host, int port, String user, String password) throws MessagingException {
        Transport transport = new SMTPTransport(session, new URLName(host));
        transport.connect(host, port, user, password);
        return transport;
    }

    private void send(Transport transport, Message message) throws MessagingException {
        message.saveChanges();
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }

    private static String getFileContent(String resourceName) {
        InputStream in = MailBdImpl.class.getResourceAsStream(resourceName);
        StringBuilder content = new StringBuilder();
        byte[] cache = new byte[1024];
        try {
            int read;
            while ((read = in.read(cache)) >= 0) {
                byte[] actualRead = new byte[read];
                System.arraycopy(cache, 0, actualRead, 0, read);
                content.append(new String(actualRead));
            }
            in.close();
        } catch (IOException e) {
            LOGGER.error(e);
        }

        return content.toString();
    }
}
