package com.dyggyty.idebt.bd;

import com.dyggyty.idebt.model.User;

/**
 * User: 1
 * Date: 23.07.12
 * Time: 22:27
 */
public interface UserBd {

    /**
     * Check user name and password combination.
     *
     * @param userName User login name.
     * @param password Encrypted user password.
     * @return New user session ID or NULL if user name and password combination was not found.
     */
    public String loginUser(String userName, String password);

    /**
     * Register new user.
     *
     * @param user User entity.
     * @return TRUE if registration was success, else FALSE.
     */
    public boolean registerUser(User user);

    /**
     * Retrieve user by login name.
     *
     * @param email User login name.
     * @return Existing user object.
     */
    public User getUserByLogin(String email);

    /**
     * Retrieve user by login name.
     *
     * @param userSession User session value.
     * @return Existing user object.
     */
    public User getUserBySession(String userSession);

    /**
     * Retrieve user by confirmatio key.
     *
     * @param confirmationKey User confirmation key.
     * @return Existing user object.
     */
    public User getUserByConfirmationKey(String confirmationKey);

    /**
     * Retrieve user password salt
     *
     * @param userName User login name
     * @return Salt
     */
    public String getUserSalt(String userName);

    /**
     * Check if user exists.
     *
     * @param userName User name
     * @return TRUE if user exists anf FALSE otherwise.
     */
    public Boolean checkUserExists(String userName);

    /**
     * Retrieve user by database identifier.
     *
     * @param id Database ID.
     * @return User entity.
     */
    public User getById(Long id);

    public void saveUser(User user);

    /**
     * Confirm user email contact.
     *
     * @param confirmationCode Email confirmation code.
     */
    public void confirmEmail(String confirmationCode);

    /**
     * Retrieve user by verified user email.
     * Email should belong to one user else null value will return.
     *
     * @param email User email.
     * @return User entity bean.
     */
    public User getVerifiedUser(String email);

    /**
     * Delete current user.
     *
     * @param userSession Current user session.
     */
    public void deleteCurrentUser(String userSession);
}
