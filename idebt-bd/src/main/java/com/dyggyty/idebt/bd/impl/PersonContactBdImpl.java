package com.dyggyty.idebt.bd.impl;

import com.dyggyty.idebt.bd.PersonContactBd;
import com.dyggyty.idebt.dao.EmailDao;
import com.dyggyty.idebt.dao.PersonContactDao;
import com.dyggyty.idebt.dao.PhoneDao;
import com.dyggyty.idebt.dao.UserDao;
import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Phone;
import com.dyggyty.idebt.model.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * User: mobius
 * Date: 01.09.12
 * Time: 11:49
 */
public class PersonContactBdImpl implements PersonContactBd {

    private PersonContactDao personContactDao;
    private UserDao userDao;
    private EmailDao emailDao;
    private PhoneDao phoneDao;

    @Transactional
    @Override
    public void savePersonContact(PersonContact personToSave) {
        Set<Email> emails = personToSave.getEmails();
        if (emails != null && emails.size() > 0) {

            Email email = emails.iterator().next();

            User user = userDao.getVerifiedUser(email.getContactInfo());
            if (user != null) {
                personToSave.setLinkedUser(user);
            }
        }

        personContactDao.saveOrUpdate(personToSave);
    }

    @Override
    public PersonContact getById(Long id) {
        return personContactDao.getById(id);
    }

    @Override
    public PersonContact getBySessionAndId(String session, Long id) {
        return personContactDao.getBySessionAndId(session, id);
    }

    @Override
    public List<PersonContact> getUserContacts(String userSession) {
        return personContactDao.getUserContacts(userSession);
    }

    @Override
    public void deletePersonContact(PersonContact person) {
        personContactDao.delete(person);
    }

    @Override
    public void deleteEmail(Email email) {
        emailDao.delete(email);
    }

    @Override
    public void deletePhone(Phone phone) {
        phoneDao.delete(phone);
    }

    public void setPersonContactDao(PersonContactDao personContactDao) {
        this.personContactDao = personContactDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setEmailDao(EmailDao emailDao) {
        this.emailDao = emailDao;
    }

    public void setPhoneDao(PhoneDao phoneDao) {
        this.phoneDao = phoneDao;
    }
}
