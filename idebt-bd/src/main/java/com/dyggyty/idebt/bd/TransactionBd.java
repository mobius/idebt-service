package com.dyggyty.idebt.bd;

import com.dyggyty.idebt.model.Transaction;

import java.util.Date;
import java.util.List;

/**
 * User: mobius
 * Date: 16.08.12
 * Time: 12:40
 */
public interface TransactionBd {

    /**
     * Create user transactions
     *
     * @param transaction Transaction details.
     * @return Database transaction entity
     */
    public Transaction createTransaction(Transaction transaction);

    /**
     * Retrieve user transactions by session key.
     *
     * @param userSession User session key
     * @return List of transactions.
     */
    public List<Transaction> getUserTransactions(String userSession);

    /**
     * Confirm transaction.
     *
     * @param confirmationCode Transaction confirmation code.
     */
    public void confirmTransaction(String confirmationCode);

    /**
     * Confirm transaction.
     *
     * @param personSession Transaction target person session identifier.
     * @param transactionId Transaction database identifier.
     */
    public void confirmTransaction(String personSession, Long transactionId);

    /**
     * Check if person has transactions.
     *
     * @param personId Person database ID.
     * @return True if person has transactions else False.
     */
    public boolean hasPersonTransactions(Long personId);

    /**
     * Delete user transaction
     *
     * @param userSession   Transaction owner user session.
     * @param transactionId Transaction database identifier.
     */
    public void deleteTransaction(String userSession, Long transactionId);

    /**
     * Update user transaction.
     *
     * @param userSession    Transaction owner session.
     * @param transactionId  Transaction database identifier.
     * @param targetPersonId Target person database identifier.
     * @param amount         Transaction amount value.
     * @param currencyCode   Transaction currency.
     * @param finalDate      Terms limitation.
     */
    public void updateTransaction(String userSession, Long transactionId, Long targetPersonId, Double amount,
                                  String currencyCode, Date finalDate, String comment);

    /**
     * Get user transaction.
     *
     * @param userSession   Transaction owner session.
     * @param transactionId Transaction database identifier.
     * @return Status of the operation.
     */
    public Transaction getTransaction(String userSession, Long transactionId);
}
