package com.dyggyty.idebt.bd.impl;

import com.dyggyty.idebt.bd.DebtBd;
import com.dyggyty.idebt.dao.DebtDao;
import com.dyggyty.idebt.model.Debt;

import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 04.10.12
 * Time: 5:46
 */
public class DebtBdImpl implements DebtBd {

    private DebtDao debtDao;

    @Override
    public List<Debt> getUserDebts(String userSession) {
        return debtDao.getUserDebts(userSession);
    }

    public void setDebtDao(DebtDao debtDao) {
        this.debtDao = debtDao;
    }
}
