package com.dyggyty.idebt.bd.impl;

import com.dyggyty.idebt.bd.CurrencyBd;
import com.dyggyty.idebt.dao.CurrencyDao;
import com.dyggyty.idebt.model.Currency;

import java.util.List;

/**
 * User: mobius
 * Date: 16.09.12
 * Time: 5:41
 */
public class CurrencyBdImpl implements CurrencyBd {

    private CurrencyDao currencyDao;

    @Override
    public Currency getByCode(String currencyCode) {
        return currencyDao.getById(currencyCode);
    }

    @Override
    public List<Currency> getAll() {
        return currencyDao.getAll();
    }

    public void setCurrencyDao(CurrencyDao currencyDao) {
        this.currencyDao = currencyDao;
    }
}
