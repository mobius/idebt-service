package com.dyggyty.idebt.bd.impl;

import com.dyggyty.idebt.bd.TransactionBd;
import com.dyggyty.idebt.dao.CurrencyDao;
import com.dyggyty.idebt.dao.DebtDao;
import com.dyggyty.idebt.dao.EmailDao;
import com.dyggyty.idebt.dao.PersonContactDao;
import com.dyggyty.idebt.dao.TransactionDao;
import com.dyggyty.idebt.model.Currency;
import com.dyggyty.idebt.model.Debt;
import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Transaction;
import com.dyggyty.idebt.model.User;
import com.dyggyty.idebt.utils.ErrorMessages;
import com.dyggyty.idebt.utils.MessageException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * User: mobius
 * Date: 17.08.12
 * Time: 7:51
 */
public class TransactionBdImpl implements TransactionBd {

    private TransactionDao transactionDao;
    private DebtDao debtDao;
    private PersonContactDao personContactDao;
    private CurrencyDao currencyDao;
    private EmailDao emailDao;

    @Override
    @Transactional
    public Transaction createTransaction(Transaction transaction) {
        transactionDao.save(transaction);

        User owner = transaction.getOwner();
        PersonContact targetPerson = transaction.getPersonContact();
        Currency currency = transaction.getCurrency();

        addTransactionDebt(owner, targetPerson, transaction.getAmount(), currency);

        return transaction;
    }

    @Override
    public List<Transaction> getUserTransactions(String userSession) {
        return transactionDao.getUserTransactions(userSession);
    }

    @Transactional
    @Override
    public void confirmTransaction(String confirmationCode) {
        Transaction transaction = transactionDao.getTransactionByConfirmationCode(confirmationCode);
        if (transaction == null) {
            throw new MessageException(ErrorMessages.INVALID_CONFIRMATION_CODE);
        }

        transaction.setVerified(Boolean.TRUE);
        transaction.setConfirmationCode(null);

        transactionDao.update(transaction);

        //We need to confirm e-mail with transaction confirmation\
        PersonContact targetPerson = transaction.getPersonContact();
        Set<Email> emails = targetPerson.getEmails();

        if (emails != null && emails.size() > 0) {
            Email email = emails.iterator().next();
            email.setVerified(Boolean.TRUE);
            email.setConfirmationCode(null);
            emailDao.update(email);
        }

        if (targetPerson.getLinkedUser() != null) {
            addConfirmedTransactionDebt(transaction, true);
        }
    }

    @Transactional
    @Override
    public void confirmTransaction(String personSession, Long transactionId) {
        Transaction transaction = transactionDao.getNonConfirmedTransaction(personSession, transactionId);
        if (transaction == null) {
            throw new MessageException(ErrorMessages.USER_TRANSACTION_IS_MISSING);
        }

        transaction.setVerified(Boolean.TRUE);
        transaction.setConfirmationCode(null);

        transactionDao.update(transaction);

        addConfirmedTransactionDebt(transaction, true);
    }

    @Override
    public boolean hasPersonTransactions(Long personId) {
        return transactionDao.hasPersonTransactions(personId);
    }

    @Transactional
    @Override
    public void deleteTransaction(String userSession, Long transactionId) {
        Transaction transaction = transactionDao.getUserTransaction(userSession, transactionId);

        if (transaction == null) {
            throw new MessageException(ErrorMessages.USER_TRANSACTION_IS_MISSING);
        }

        PersonContact targetPerson = transaction.getPersonContact();
        if (transaction.getVerified() && targetPerson.getLinkedUser() != null) {
            addConfirmedTransactionDebt(transaction, false);
        }

        addTransactionDebt(transaction.getOwner(), targetPerson, -transaction.getAmount(), transaction.getCurrency());

        transactionDao.delete(transaction);
    }

    @Transactional
    @Override
    public void updateTransaction(String userSession, Long transactionId, Long targetPersonId, Double amount,
                                  String currencyCode, Date finalDate, String comment) {

        Transaction transaction = transactionDao.getUserTransaction(userSession, transactionId);

        if (transaction == null) {
            throw new MessageException(ErrorMessages.USER_TRANSACTION_IS_MISSING);
        }

        PersonContact currentTargetPerson = transaction.getPersonContact();
        if (transaction.getVerified() && currentTargetPerson.getLinkedUser() != null) {
            PersonContact linkedPersonContact =
                    personContactDao.getByOwnerAndLinkedPerson(currentTargetPerson.getLinkedUser().getId(),
                            transaction.getOwner().getId());

            addTransactionDebt(currentTargetPerson.getLinkedUser(), linkedPersonContact,
                    -transaction.getAmount(), transaction.getCurrency());
        }

        addTransactionDebt(transaction.getOwner(), currentTargetPerson, -transaction.getAmount(),
                transaction.getCurrency());

        if (!currentTargetPerson.getId().equals(targetPersonId)) {
            PersonContact targetPerson = personContactDao.getBySessionAndId(userSession, targetPersonId);

            if (targetPerson == null) {
                throw new MessageException(ErrorMessages.USER_CONTACT_IS_MISSING);
            }

            transaction.setPersonContact(targetPerson);
        }

        Currency currency = currencyDao.getById(currencyCode);

        addTransactionDebt(transaction.getOwner(), transaction.getPersonContact(), transaction.getAmount(), currency);

        transaction.setVerified(Boolean.FALSE);
        transaction.setAmount(amount);
        transaction.setCurrency(currency);
        transaction.setFinalDate(finalDate);
        transaction.setComment(comment);

        transactionDao.update(transaction);
    }

    @Override
    public Transaction getTransaction(String userSession, Long transactionId) {
        return transactionDao.getUserTransaction(userSession, transactionId);
    }

    private void addConfirmedTransactionDebt(Transaction transaction, boolean toTargetPerson) {
        User owner = transaction.getOwner();
        User targetPerson = transaction.getPersonContact().getLinkedUser();
        Currency currency = transaction.getCurrency();

        PersonContact linkedPersonContact =
                personContactDao.getByOwnerAndLinkedPerson(targetPerson.getId(), owner.getId());

        addTransactionDebt(targetPerson, linkedPersonContact,
                (toTargetPerson ? -1 : 1) * transaction.getAmount(), currency);
    }

    private void addTransactionDebt(User debtOwner, PersonContact targetPerson, Double amount, Currency currency) {
        Debt debt = debtDao.getDebts(debtOwner.getId(), targetPerson.getId(), currency.getId());
        if (debt == null) {
            debt = new Debt();
            debt.setAmount(0d);
            debt.setCurrency(currency);
            debt.setOwner(debtOwner);
            debt.setPersonContact(targetPerson);
        }

        debt.setAmount(debt.getAmount() + amount);

        if (debt.getAmount() == 0d) {
            if (debt.getId() != null) {
                debtDao.delete(debt);
            }
        } else {
            debtDao.saveOrUpdate(debt);
        }
    }

    public void setTransactionDao(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

    public void setDebtDao(DebtDao debtDao) {
        this.debtDao = debtDao;
    }

    public void setPersonContactDao(PersonContactDao personContactDao) {
        this.personContactDao = personContactDao;
    }

    public void setCurrencyDao(CurrencyDao currencyDao) {
        this.currencyDao = currencyDao;
    }

    public void setEmailDao(EmailDao emailDao) {
        this.emailDao = emailDao;
    }
}
