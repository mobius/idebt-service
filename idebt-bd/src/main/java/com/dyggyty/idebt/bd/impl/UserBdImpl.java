package com.dyggyty.idebt.bd.impl;

import com.dyggyty.idebt.bd.UserBd;
import com.dyggyty.idebt.dao.EmailDao;
import com.dyggyty.idebt.dao.PersonContactDao;
import com.dyggyty.idebt.dao.UserDao;
import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.User;
import com.dyggyty.idebt.utils.ErrorMessages;
import com.dyggyty.idebt.utils.MessageException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * User: vitaly.rudenya
 * Date: 23.07.12
 * Time: 22:28
 */
public class UserBdImpl implements UserBd {

    private UserDao userDao;
    private EmailDao emailDao;
    private PersonContactDao personContactDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setEmailDao(EmailDao emailDao) {
        this.emailDao = emailDao;
    }

    public void setPersonContactDao(PersonContactDao personContactDao) {
        this.personContactDao = personContactDao;
    }

    @Override
    @Transactional
    public String loginUser(String userName, String password) {

        String uuid;
        User user = userDao.getUserByNameAndPassword(userName, password);
        if (user != null) {
            uuid = UUID.randomUUID().toString();
            user.setSession(uuid);
            user.setLastLogin(new Date());
            userDao.update(user);
        } else {
            throw new MessageException(ErrorMessages.USER_NOT_FOUND);
        }

        return uuid;
    }

    @Override
    @Transactional
    public boolean registerUser(User user) {
        boolean status = !userDao.checkUserExists(user.getUsername());
        if (status) {
            userDao.save(user);
        } else {
            throw new MessageException(ErrorMessages.USER_ALREADY_EXISTS);
        }
        return status;
    }

    @Override
    public User getUserByLogin(String email) {
        return userDao.getUserByLogin(email);
    }

    @Override
    public User getUserBySession(String userSession) {
        return userDao.getUserBySession(userSession);
    }

    @Override
    public User getUserByConfirmationKey(String confirmationKey) {
        return userDao.getUserByConfirmationKey(confirmationKey);
    }

    @Override
    public String getUserSalt(String userName) {
        return userDao.getUserSalt(userName);
    }

    @Override
    public Boolean checkUserExists(String userName) {
        return userDao.checkUserExists(userName);
    }

    @Override
    public User getById(Long id) {
        return userDao.getById(id);
    }

    @Override
    public void saveUser(User user) {
        userDao.saveOrUpdate(user);
    }

    @Transactional
    @Override
    public void confirmEmail(String confirmationCode) {
        Email email = emailDao.getByConfirmationCode(confirmationCode);

        if (email == null) {
            throw new MessageException(ErrorMessages.INVALID_CONFIRMATION_CODE);
        }

        email.setVerified(Boolean.TRUE);
        email.setConfirmationCode(null);

        emailDao.update(email);

        User currentUser = (User) email.getPerson();
        List<PersonContact> personContacts = personContactDao.getNonLinkedPersonsByEmail(email.getContactInfo());
        for (PersonContact personContact : personContacts) {
            personContact.setLinkedUser(currentUser);
            personContactDao.update(personContact);
        }
    }

    @Override
    public User getVerifiedUser(String email) {
        return userDao.getVerifiedUser(email);
    }

    @Transactional
    @Override
    public void deleteCurrentUser(String userSession) {
        User currentUser = userDao.getUserBySession(userSession);
        currentUser.setDeleted(Boolean.TRUE);
        userDao.update(currentUser);
    }
}
