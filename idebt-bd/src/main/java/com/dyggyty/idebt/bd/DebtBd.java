package com.dyggyty.idebt.bd;

import com.dyggyty.idebt.model.Debt;

import java.util.List;

/**
 * User: vitaly.rudenya
 * Date: 04.10.12
 * Time: 3:43
 */
public interface DebtBd {

    /**
     * Retrieve user debts by session key.
     *
     * @param userSession User session key
     * @return List of debts.
     */
    public List<Debt> getUserDebts(String userSession);
}