package com.dyggyty.idebt.bd;

import com.dyggyty.idebt.model.Email;
import com.dyggyty.idebt.model.PersonContact;
import com.dyggyty.idebt.model.Phone;

import java.util.List;

/**
 * User: mobius
 * Date: 17.08.12
 * Time: 9:37
 */
public interface PersonContactBd {

    public void savePersonContact(PersonContact personToSave);

    /**
     * Retrieve person by database identifier.
     *
     * @param id Database ID.
     * @return User entity.
     */
    public PersonContact getById(Long id);

    /**
     * Retrieve person by database identifier.
     *
     * @param session Current user session.
     * @param id      Database ID.
     * @return User entity.
     */
    public PersonContact getBySessionAndId(String session, Long id);

    /**
     * Retrieve user contacts by user session.
     *
     * @param userSession User session identifier.
     * @return List of contacts.
     */
    public List<PersonContact> getUserContacts(String userSession);

    /**
     * Delete person from database.
     *
     * @param person Person object to delete.
     */
    public void deletePersonContact(PersonContact person);

    public void deleteEmail(Email email);

    public void deletePhone(Phone phone);
}
