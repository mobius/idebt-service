package com.dyggyty.idebt.bd;

import com.dyggyty.idebt.model.Currency;

import java.util.List;

/**
 * User: mobius
 * Date: 16.09.12
 * Time: 5:35
 */
public interface CurrencyBd {

    /**
     * Get currency by code.
     *
     * @param currencyCode Currency code.
     * @return Currency object
     */
    public Currency getByCode(String currencyCode);

    /**
     * Get all currencies.
     *
     * @return List of currencies.
     */
    public List<Currency> getAll();
}
