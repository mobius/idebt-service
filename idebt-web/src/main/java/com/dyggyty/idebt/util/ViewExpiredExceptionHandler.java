package com.dyggyty.idebt.util;

import com.dyggyty.idebt.utils.ErrorMessages;
import com.dyggyty.idebt.wl.LoginForm;
import org.springframework.web.jsf.FacesContextUtils;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import java.util.Iterator;

/**
 * User: vitaly.rudenya
 * Date: 15.11.12
 * Time: 4:53
 */
public class ViewExpiredExceptionHandler extends ExceptionHandlerWrapper {

    private ExceptionHandler wrapped;

    public ViewExpiredExceptionHandler(ExceptionHandler wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public void handle() throws FacesException {
        for (Iterator<ExceptionQueuedEvent> iter = getUnhandledExceptionQueuedEvents().iterator(); iter.hasNext(); ) {
            Throwable exception = iter.next().getContext().getException();

            if (exception instanceof ViewExpiredException || exception instanceof IllegalStateException) {
                FacesContext ctx = FacesContext.getCurrentInstance();
                MessageFactory messageFactory =
                        FacesContextUtils.getWebApplicationContext(ctx).getBean(MessageFactory.class);

                ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        messageFactory.getMessage(ErrorMessages.INVALID_USER_SESSION), null));

                FacesContext facesContext = FacesContext.getCurrentInstance();
                facesContext.getApplication().getNavigationHandler().
                        handleNavigation(facesContext, null, LoginForm.PAGE_LOGIN);
                facesContext.renderResponse();
                iter.remove();
            }
        }

        getWrapped().handle();
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }
}
