package com.dyggyty.idebt.util;

import com.dyggyty.idebt.rest.response.AddTransactionResponse;
import com.dyggyty.idebt.rest.response.ChangeUserInfoResponse;
import com.dyggyty.idebt.rest.response.ChangeUserPasswordResponse;
import com.dyggyty.idebt.rest.response.CheckUserResponse;
import com.dyggyty.idebt.rest.response.DeleteTransactionResponse;
import com.dyggyty.idebt.rest.response.DeleteUserContactResponse;
import com.dyggyty.idebt.rest.response.ForgotUserPasswordResponse;
import com.dyggyty.idebt.rest.response.GetCurrencyResponse;
import com.dyggyty.idebt.rest.response.GetDebtsResponse;
import com.dyggyty.idebt.rest.response.GetTransactionResponse;
import com.dyggyty.idebt.rest.response.GetTransactionsResponse;
import com.dyggyty.idebt.rest.response.GetUserContactResponse;
import com.dyggyty.idebt.rest.response.GetUserContactsResponse;
import com.dyggyty.idebt.rest.response.GetUserInfoResponse;
import com.dyggyty.idebt.rest.response.LoginUserResponse;
import com.dyggyty.idebt.rest.response.RegisterUserResponse;
import com.dyggyty.idebt.rest.response.SaveUserContactResponse;

import java.util.Date;

/**
 * User: vitaly.rudenya
 * Date: 15.11.12
 * Time: 8:55
 */
public interface RequestProcessor {
    public DeleteTransactionResponse deleteTransaction(String userSession, Long transactionId);

    public ChangeUserPasswordResponse changeUserPassword(String userSession, String password, String newPassword,
                                                  String confirmationKey);

    public ForgotUserPasswordResponse forgotPassword(String userName, String userEmail);

    public ChangeUserInfoResponse updateUserInfo(String userSession, String nickName, String userEmail, String userPhone);

    public GetUserInfoResponse getUserInfo(String userSession);

    public DeleteUserContactResponse deleteUserContact(String userSession, Long contactId);

    public SaveUserContactResponse saveUserContact(String userSession, Long personId, String personName,
                                            String personEmail, String personPhone);

    public GetCurrencyResponse getCurrencies();

    public GetUserContactResponse getUserContact(String userSession, Long contactId);

    public GetUserContactsResponse getUserContacts(String userSession);

    public AddTransactionResponse updateTransaction(String userSession, Long transactionId, Long targetPersonId,
                                             Double amount, String currencyCode, Date finalDate);

    public AddTransactionResponse addTransaction(String userSession, Long targetPersonId, Double amount,
                                          String currencyCode, Date finalDate);

    public GetTransactionResponse getUserTransaction(String userSession, Long transactionId);

    public GetTransactionsResponse getUserTransactions(String userSession);

    public GetDebtsResponse getUserDebts(String userSession);

    public RegisterUserResponse registerUser(String userName, String nickName, String password, String email,
                                      String phone);

    public CheckUserResponse checkUserExists(String userName);

    public LoginUserResponse loginUser(String userName, String password);
}
