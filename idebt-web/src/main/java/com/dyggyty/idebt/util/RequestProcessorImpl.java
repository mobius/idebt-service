package com.dyggyty.idebt.util;

import com.dyggyty.idebt.rest.response.AddTransactionResponse;
import com.dyggyty.idebt.rest.response.ChangeUserInfoResponse;
import com.dyggyty.idebt.rest.response.ChangeUserPasswordResponse;
import com.dyggyty.idebt.rest.response.CheckUserResponse;
import com.dyggyty.idebt.rest.response.DeleteTransactionResponse;
import com.dyggyty.idebt.rest.response.DeleteUserContactResponse;
import com.dyggyty.idebt.rest.response.ForgotUserPasswordResponse;
import com.dyggyty.idebt.rest.response.GenericResponse;
import com.dyggyty.idebt.rest.response.GetCurrencyResponse;
import com.dyggyty.idebt.rest.response.GetDebtsResponse;
import com.dyggyty.idebt.rest.response.GetTransactionResponse;
import com.dyggyty.idebt.rest.response.GetTransactionsResponse;
import com.dyggyty.idebt.rest.response.GetUserContactResponse;
import com.dyggyty.idebt.rest.response.GetUserContactsResponse;
import com.dyggyty.idebt.rest.response.GetUserInfoResponse;
import com.dyggyty.idebt.rest.response.LoginUserResponse;
import com.dyggyty.idebt.rest.response.RegisterUserResponse;
import com.dyggyty.idebt.rest.response.SaveUserContactResponse;
import com.dyggyty.idebt.utils.ApplicationInternalException;
import com.dyggyty.idebt.utils.RequestValidator;
import com.dyggyty.idebt.wl.RequestParams;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.mapped.Configuration;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * User: mobius
 * Date: 05.10.12
 * Time: 12:15
 */
public class RequestProcessorImpl implements RequestProcessor {

    private static final MappedNamespaceConvention CONVENTION = new MappedNamespaceConvention(new Configuration());

    private static final Map<Class, Unmarshaller> JAXB = new HashMap<Class, Unmarshaller>();

    private static final String URL_ENCODING = "UTF-8";

    private String webServiceUrl; //todo we don't need to setup the hots manually

    public RequestProcessorImpl() {

    }

    public RequestProcessorImpl(String webServiceUrl) {
        this.webServiceUrl = webServiceUrl;
    }

    public void setWebServiceUrl(String webServiceUrl) {
        this.webServiceUrl = webServiceUrl;
    }

    @Override
    public DeleteTransactionResponse deleteTransaction(String userSession, Long transactionId) {
        RequestValidator.validateDeleteTransaction(userSession, transactionId);
        String requestUrl = String.format(RequestParams.TRANSACTION_DELETE, webServiceUrl, userSession);

        Map<String, String> params = new HashMap<String, String>();
        params.put(RequestParams.REQUEST_TRANSACTION_ID, transactionId.toString());

        return processRequest(requestUrl, params, DeleteTransactionResponse.class);
    }

    @Override
    public ChangeUserPasswordResponse changeUserPassword(String userSession, String password, String newPassword,
                                                         String confirmationKey) {
        RequestValidator.validateChangeUserPassword(userSession, password, newPassword, confirmationKey);

        String requestUrl = String.format(RequestParams.USER_CHANGE_PASSWORD, webServiceUrl, userSession);

        Map<String, String> params = new HashMap<String, String>();

        params.put(RequestParams.REQUEST_PASSWORD, password);
        params.put(RequestParams.REQUEST_NEW_PASSWORD, newPassword);
        params.put(RequestParams.PARAM_CONFIRMATION_KEY, confirmationKey);

        return processRequest(requestUrl, params, ChangeUserPasswordResponse.class);
    }

    @Override
    public ForgotUserPasswordResponse forgotPassword(String userName, String userEmail) {
        RequestValidator.validateForgotPassword(userEmail);

        String requestUrl = String.format(RequestParams.USER_FORGOT_PASSWORD, webServiceUrl, userName);

        Map<String, String> params = new HashMap<String, String>();
        params.put(RequestParams.REQUEST_USER_EMAIL, userEmail);

        return processRequest(requestUrl, params, ForgotUserPasswordResponse.class);
    }


    @Override
    public ChangeUserInfoResponse updateUserInfo(String userSession, String nickName, String userEmail, String userPhone) {

        RequestValidator.validateUpdateUserInfo(userSession, nickName, userEmail, userPhone);

        String requestUrl = String.format(RequestParams.USER_PERSONAL_INFO, webServiceUrl, userSession);

        Map<String, String> params = new HashMap<String, String>();

        params.put(RequestParams.REQUEST_NICK_NAME, nickName);
        params.put(RequestParams.REQUEST_USER_EMAIL, userEmail);
        params.put(RequestParams.REQUEST_PHONE, userPhone);

        return processRequest(requestUrl, params, ChangeUserInfoResponse.class);
    }

    @Override
    public GetUserInfoResponse getUserInfo(String userSession) {
        RequestValidator.validateGetUserInfo(userSession);
        String requestUrl = String.format(RequestParams.USER_GET_PERSONAL_INFO, webServiceUrl, userSession);
        return processRequest(requestUrl, GetUserInfoResponse.class);
    }

    @Override
    public DeleteUserContactResponse deleteUserContact(String userSession, Long contactId) {

        RequestValidator.validateDeleteUserContact(userSession, contactId);

        String requestUrl = String.format(RequestParams.USER_DELETE_USER_CONTACT, webServiceUrl, userSession);

        Map<String, String> params = new HashMap<String, String>();
        params.put(RequestParams.REQUEST_PERSON_CONTACT_ID, contactId.toString());

        return processRequest(requestUrl, params, DeleteUserContactResponse.class);
    }

    @Override
    public SaveUserContactResponse saveUserContact(String userSession, Long personId, String personName,
                                                   String personEmail, String personPhone) {

        RequestValidator.validateSaveUserContact(userSession, personName, personEmail, personPhone, null);

        String requestUrl = String.format(RequestParams.USER_SAVE_USER_CONTACTS, webServiceUrl, userSession);

        Map<String, String> params = new HashMap<String, String>();

        if (personId != null) {
            params.put(RequestParams.REQUEST_PERSON_CONTACT_ID, personId.toString());
        }
        params.put(RequestParams.REQUEST_PERSON_CONTACT_NAME, personName);

        if (personEmail != null) {
            params.put(RequestParams.REQUEST_PERSON_CONTACT_EMAIL, personEmail);
        }

        if (personPhone != null) {
            params.put(RequestParams.REQUEST_PERSON_CONTACT_PHONE, personPhone);
        }

        return processRequest(requestUrl, params, SaveUserContactResponse.class);
    }

    @Override
    public GetCurrencyResponse getCurrencies() {
        String requestUrl = String.format(RequestParams.CURRENCY_GET_ALL, webServiceUrl);
        return processRequest(requestUrl, GetCurrencyResponse.class);
    }

    @Override
    public GetUserContactResponse getUserContact(String userSession, Long contactId) {
        RequestValidator.validateGetUserContact(userSession, contactId);
        String userContactsUrl = String.format(RequestParams.USER_CONTACT, webServiceUrl, userSession);

        Map<String, String> params = new HashMap<String, String>();
        params.put(RequestParams.REQUEST_PERSON_CONTACT_ID, contactId.toString());

        return processRequest(userContactsUrl, params, GetUserContactResponse.class);
    }

    @Override
    public GetUserContactsResponse getUserContacts(String userSession) {
        RequestValidator.validateGetUserContacts(userSession);
        String userContactsUrl = String.format(RequestParams.USER_CONTACTS, webServiceUrl, userSession);
        return processRequest(userContactsUrl, GetUserContactsResponse.class);
    }

    @Override
    public AddTransactionResponse updateTransaction(String userSession, Long transactionId, Long targetPersonId,
                                                    Double amount, String currencyCode, Date finalDate) {

        RequestValidator.validateUpdateTransaction(userSession, transactionId, targetPersonId, amount,
                currencyCode, finalDate, null);

        String loginUrl = String.format(RequestParams.TRANSACTION_UPDATE, webServiceUrl, userSession);

        Map<String, String> params = new HashMap<String, String>();

        params.put(RequestParams.REQUEST_TRANSACTION_ID, transactionId.toString());
        params.put(RequestParams.REQUEST_PERSON_CONTACT_ID, targetPersonId.toString());
        params.put(RequestParams.REQUEST_AMOUNT, amount.toString());
        params.put(RequestParams.REQUEST_CURRENCY_CODE, currencyCode);
        params.put(RequestParams.REQUEST_FINAL_DATE, RequestParams.APP_DATE_FORMAT.format(finalDate));

        return processRequest(loginUrl, params, AddTransactionResponse.class);
    }

    @Override
    public AddTransactionResponse addTransaction(String userSession, Long targetPersonId, Double amount,
                                                 String currencyCode, Date finalDate) {

        RequestValidator.validateAddTransaction(userSession, targetPersonId, amount, currencyCode, finalDate, null);

        String loginUrl = String.format(RequestParams.TRANSACTION_ADD, webServiceUrl, userSession);

        Map<String, String> params = new HashMap<String, String>();

        params.put(RequestParams.REQUEST_PERSON_CONTACT_ID, targetPersonId.toString());
        params.put(RequestParams.REQUEST_AMOUNT, amount.toString());
        params.put(RequestParams.REQUEST_CURRENCY_CODE, currencyCode);
        params.put(RequestParams.REQUEST_FINAL_DATE, RequestParams.APP_DATE_FORMAT.format(finalDate));

        return processRequest(loginUrl, params, AddTransactionResponse.class);
    }

    @Override
    public GetTransactionResponse getUserTransaction(String userSession, Long transactionId) {
        RequestValidator.validateGetUserTransaction(userSession, transactionId);
        String requestUrl = String.format(RequestParams.TRANSACTION_GET, webServiceUrl, userSession);

        Map<String, String> params = new HashMap<String, String>();
        params.put(RequestParams.REQUEST_TRANSACTION_ID, transactionId.toString());

        return processRequest(requestUrl, params, GetTransactionResponse.class);
    }

    @Override
    public GetTransactionsResponse getUserTransactions(String userSession) {
        RequestValidator.validateGetUserTransactions(userSession);
        String loginUrl = String.format(RequestParams.TRANSACTION_GET_ALL, webServiceUrl, userSession);
        return processRequest(loginUrl, GetTransactionsResponse.class);
    }

    @Override
    public GetDebtsResponse getUserDebts(String userSession) {
        RequestValidator.validateGetUserDebts(userSession);
        String loginUrl = String.format(RequestParams.DEBT_GET_ALL, webServiceUrl, userSession);
        return processRequest(loginUrl, GetDebtsResponse.class);
    }

    @Override
    public RegisterUserResponse registerUser(String userName, String nickName, String password, String email,
                                             String phone) {

        RequestValidator.validateRegisterUser(email, nickName, password, phone);

        String loginUrl = String.format(RequestParams.USER_REGISTER, webServiceUrl);

        Map<String, String> params = new HashMap<String, String>();

        params.put(RequestParams.REQUEST_PASSWORD, password);
        params.put(RequestParams.REQUEST_USER_EMAIL, email);
        params.put(RequestParams.REQUEST_NICK_NAME, nickName);
        params.put(RequestParams.REQUEST_PHONE, phone);

        return processRequest(loginUrl, params, RegisterUserResponse.class);
    }

    @Override
    public CheckUserResponse checkUserExists(String userName) {
        RequestValidator.validateCheckUserExists(userName);
        String loginUrl = String.format(RequestParams.USER_CHECK_EXISTS, webServiceUrl, userName);
        return processRequest(loginUrl, CheckUserResponse.class);
    }

    @Override
    public LoginUserResponse loginUser(String userName, String password) {
        RequestValidator.validateLoginUser(userName, password);
        String loginUrl = String.format(RequestParams.USER_LOGIN, webServiceUrl, userName);

        Map<String, String> params = new HashMap<String, String>();
        params.put(RequestParams.REQUEST_PASSWORD, password);

        return processRequest(loginUrl, params, LoginUserResponse.class);
    }

    private static Unmarshaller getUnmarshaller(Class clazz) throws JAXBException {
        if (!JAXB.containsKey(clazz)) {
            synchronized (JAXB) {
                if (!JAXB.containsKey(clazz)) {
                    Unmarshaller unmarshaller = JAXBContext.newInstance(clazz).createUnmarshaller();
                    JAXB.put(clazz, unmarshaller);
                }
            }
        }

        return JAXB.get(clazz);
    }

    private static <T extends GenericResponse> T processRequest(String url, Class<T> clazz) {
        return processRequest(url, null, clazz);
    }

    @SuppressWarnings("unchecked")
    private static <T extends GenericResponse> T processRequest(String url, Map<String, String> params,
                                                                Class<T> clazz) {

        try {
            URL urlCon = new URL(url);
            URLConnection conn = urlCon.openConnection();
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=" + URL_ENCODING);

            OutputStreamWriter writer = null;
            if (params != null && params.size() > 0) {
                conn.setDoOutput(true);
                writer = new OutputStreamWriter(conn.getOutputStream());

                StringBuilder builder = new StringBuilder();

                for (Map.Entry<String, String> paramEntry : params.entrySet()) {
                    String value = paramEntry.getValue();

                    if (value != null) {
                        if (builder.length() > 0) {
                            builder.append("&");
                        }

                        builder.append(URLEncoder.encode(paramEntry.getKey(), URL_ENCODING)).append("=").
                                append(URLEncoder.encode(paramEntry.getValue(), URL_ENCODING));
                    }
                }
                //write parameters
                writer.write(builder.toString());
                writer.flush();
            }

            // Get the response
            StringBuilder answer = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), URL_ENCODING));
            String line;
            while ((line = reader.readLine()) != null) {
                answer.append(line);
            }
            if (writer != null) {
                writer.close();
            }
            reader.close();

            //Output the response
            JSONObject obj = new JSONObject(answer.toString());
            MappedXMLStreamReader xmlStreamReader = new MappedXMLStreamReader(obj, CONVENTION);
            Unmarshaller unmarshaller = getUnmarshaller(clazz);
            return (T) unmarshaller.unmarshal(xmlStreamReader);
        } catch (Exception ex) {
            throw new ApplicationInternalException(ex);
        }
    }
}
