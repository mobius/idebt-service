package com.dyggyty.idebt.util;

import com.dyggyty.idebt.rest.response.GenericResponse;
import com.dyggyty.idebt.utils.ApplicationInternalException;
import com.dyggyty.idebt.utils.ErrorMessages;
import com.dyggyty.idebt.utils.MessageException;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * User: vitaly.rudenya
 * Date: 15.11.12
 * Time: 6:17
 */
public class RequestErrorProcessor implements MethodInterceptor {

    private static final Log LOGGER = LogFactory.getLog(RequestProcessor.class);

    private MessageFactory messageFactory;

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        GenericResponse retVal;
        try {
            retVal = (GenericResponse) methodInvocation.proceed();
        } catch (ApplicationInternalException ex) {
            LOGGER.error(ex);
            Class returnType = methodInvocation.getMethod().getReturnType();
            retVal = (GenericResponse) returnType.newInstance();
            retVal.setErrorMessageKey(ErrorMessages.SERVER_COMMUNICATION_ERROR);
        } catch (MessageException ex) {
            Class returnType = methodInvocation.getMethod().getReturnType();
            retVal = (GenericResponse) returnType.newInstance();
            retVal.setErrorMessageKey(ex.getMessage());
        }

        if (!ErrorMessages.STATUS_OK.equals(retVal.getErrorMessageKey())) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    messageFactory.getMessage(retVal.getErrorMessageKey()), null));
        }
        return retVal;
    }
}
