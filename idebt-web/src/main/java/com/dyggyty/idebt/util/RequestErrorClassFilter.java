package com.dyggyty.idebt.util;

import org.springframework.aop.ClassFilter;

/**
 * User: vitaly.rudenya
 * Date: 15.11.12
 * Time: 6:30
 */
public class RequestErrorClassFilter implements ClassFilter {
    public boolean matches(Class aClass) {
        return RequestProcessor.class.equals(aClass);
    }
}
