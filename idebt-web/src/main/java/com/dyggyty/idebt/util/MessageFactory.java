package com.dyggyty.idebt.util;

import javax.faces.context.FacesContext;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class MessageFactory {

    private static final Map<Locale, ResourceBundle> BUNDLES = new HashMap<Locale, ResourceBundle>();
    private String baseName;

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    public String getMessage(String key) {
        return getBundle().getString(key);
    }

    private ResourceBundle getBundle() {
        Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle bundle = BUNDLES.get(locale);

        if (bundle == null) {
            synchronized (BUNDLES) {
                bundle = BUNDLES.get(locale);
                if (bundle == null) {
                    bundle = ResourceBundle.getBundle(baseName, locale);
                    BUNDLES.put(locale, bundle);
                }
            }
        }

        return bundle;
    }
}