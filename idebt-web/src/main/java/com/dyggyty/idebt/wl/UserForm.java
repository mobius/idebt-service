package com.dyggyty.idebt.wl;

import com.dyggyty.idebt.rest.response.ChangeUserInfoResponse;
import com.dyggyty.idebt.rest.response.CheckUserResponse;
import com.dyggyty.idebt.rest.response.GetUserInfoResponse;
import com.dyggyty.idebt.rest.response.RegisterUserResponse;
import com.dyggyty.idebt.util.MessageFactory;
import com.dyggyty.idebt.utils.ErrorMessages;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserForm extends GenericForm {

    private static final String PAGE_USER_ACCOUNT = "useraccount";

    private String realName;
    private String userName;
    private String password;
    private String confirmPassword;
    private String email;
    private String contactNumber;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public boolean getIsUserNameDisabled() {
        HttpSession session = getSession();
        return session.getAttribute(RequestParams.SESSION_ID) != null;
    }

    public String editProfile() throws Exception {
        HttpSession session = getSession();
        String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);

        if (sessionId == null) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    getMessageFactory().getMessage(ErrorMessages.INVALID_USER_SESSION), null));
            return LoginForm.PAGE_LOGIN;
        }

        GetUserInfoResponse userInfoResponse = getRequestProcessor().getUserInfo(sessionId);

        realName = userInfoResponse.getNickName();
        userName = userInfoResponse.getUserName();

        List<String> emails = userInfoResponse.getUserEmails();
        if (emails != null && emails.size() > 0) {
            email = emails.get(0);
        }

        List<String> phones = userInfoResponse.getUserPhones();
        if (phones != null && phones.size() > 0) {
            contactNumber = phones.get(0);
        }

        return PAGE_USER_ACCOUNT;
    }

    public String saveUser() throws Exception {
        if (validateData()) {
            HttpSession session = getSession();
            String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);

            String messageKey;
            if (sessionId == null) {
                RegisterUserResponse registerUserResponse =
                        getRequestProcessor().registerUser(userName, realName, password, email, contactNumber);
                messageKey = registerUserResponse.getErrorMessageKey();
            } else {
                ChangeUserInfoResponse userInfoResponse =
                        getRequestProcessor().updateUserInfo(sessionId, realName, email, contactNumber);

                messageKey = userInfoResponse.getErrorMessageKey();
            }

            if (!ErrorMessages.STATUS_OK.equals(messageKey)) {
                return PAGE_USER_ACCOUNT;
            } else {
                //todo add success message
            }
        } else {
            return PAGE_USER_ACCOUNT;
        }
        return back();
    }

    public boolean validUser(String username) {
        CheckUserResponse checkUserResponse = getRequestProcessor().checkUserExists(username);

        if (ErrorMessages.STATUS_OK.equals(checkUserResponse.getErrorMessageKey())) {
            boolean userExists = checkUserResponse.getUserExists();
            if (userExists) {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        getMessageFactory().getMessage(ErrorMessages.USER_ALREADY_EXISTS), null));
            }
            return !userExists;
        }
        return false;
    }

    private boolean validateData() throws Exception {
        boolean status = true;
        FacesContext ctx = FacesContext.getCurrentInstance();

        MessageFactory messageFactory = getMessageFactory();

        String realName = this.realName.trim();
        if (!(((realName.length()) >= 1) && ((realName.length()) <= 25))) {
            ctx.addMessage("UserForm:realName", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    messageFactory.getMessage("errorRealNameLength"), null));
            status = false;
        }

        HttpSession session = getSession();
        String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);

        if (sessionId == null) {
            //Checking User Name
            String username = userName.trim();
            status = status && validUser(username);

            if ((password.length()) < 6 || (password.length()) > 20) {
                ctx.addMessage("UserForm:password", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        messageFactory.getMessage("errorPasswordLength"), null));
                status = false;
            }

            Pattern p1 = Pattern.compile("^[a-zA-Z0-9]+$");
            Matcher m1 = p1.matcher(password);
            boolean matchFound1 = m1.matches();

            if (!matchFound1) {
                ctx.addMessage("UserForm:password", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        messageFactory.getMessage("errorPassword"), null));
                status = false;
            }

            if (!confirmPassword.equals(password)) {
                ctx.addMessage("UserForm:confirmPassword", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        messageFactory.getMessage("errorConfirmPassword"), null));
                status = false;
            }
        }

        String cn = contactNumber.trim();
        if ((cn.length()) == 0) {
            ctx.addMessage("UserForm:contactNumber", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    messageFactory.getMessage("errorContactNumberBlank"), null));
            status = false;
        }

        return status;
    }

    @Override
    protected String getPageName() {
        return PAGE_USER_ACCOUNT;
    }
}