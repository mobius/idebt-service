package com.dyggyty.idebt.wl;

import com.dyggyty.idebt.util.MessageFactory;
import com.dyggyty.idebt.util.RequestProcessor;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * User: vitaly.rudenya
 * Date: 08.10.12
 * Time: 5:42
 */
public abstract class GenericForm {

    private static final String SESSION_NAVIGATION = "NavigationBean";
    private static final String PAGE_MAIN = "index";

    private RequestProcessor requestProcessor;
    private MessageFactory messageFactory;
    private FacesContext context;
    private HttpSession session;
    private NavigationBean navigationBean;

    public GenericForm() {
        context = FacesContext.getCurrentInstance();
        session = (HttpSession) context.getExternalContext().getSession(true);

        getNavigationBean().next(getPageName());
    }

    protected Integer getIntValue(String name) {
        return (Integer) getNavigationBean().getState().get(name);
    }

    protected Long getLongValue(String name) {
        return (Long) getNavigationBean().getState().get(name);
    }

    protected Double getDoubleValue(String name) {
        return (Double) getNavigationBean().getState().get(name);
    }

    protected Date getDateValue(String name) {
        return (Date) getNavigationBean().getState().get(name);
    }

    protected String getStringValue(String name) {
        return (String) getNavigationBean().getState().get(name);
    }

    protected Boolean getBooleanValue(String name) {
        return (Boolean) getNavigationBean().getState().get(name);
    }

    protected void setValue(String name, Integer value) {
        Integer valueToSave = value != null && value == 0 ? null : value;
        getNavigationBean().getState().put(name, value);
    }

    protected void setValue(String name, Double value) {
        Double valueToSave = value != null && value.equals(0d) ? null : value;
        getNavigationBean().getState().put(name, valueToSave);
    }

    protected void setValue(String name, Date value) {
        getNavigationBean().getState().put(name, value);
    }

    protected void setValue(String name, String value) {
        String valueToSave = value != null && value.trim().isEmpty() ? null : value;
        getNavigationBean().getState().put(name, valueToSave);
    }

    protected void setValue(String name, Long value) {
        Long valueToSave = value != null && value == 0 ? null : value;
        getNavigationBean().getState().put(name, valueToSave);
    }

    protected void setValue(String name, Boolean value) {
        getNavigationBean().getState().put(name, value);
    }

    private NavigationBean getNavigationBean() {
        if (navigationBean == null) {
            navigationBean = (NavigationBean) session.getAttribute(SESSION_NAVIGATION);
        }

        if (navigationBean == null) {
            navigationBean = new NavigationBean();
            session.setAttribute(SESSION_NAVIGATION, navigationBean);
        }

        return navigationBean;
    }

    public String back() {
        NavigationBean navigationBean = getNavigationBean();
        navigationBean.back();
        String page = navigationBean.getPageName();
        if (page == null) {
            page = PAGE_MAIN;
        }

        return page;
    }

    protected abstract String getPageName();

    public FacesContext getContext() {
        return context;
    }

    public HttpSession getSession() {
        return session;
    }

    public void setRequestProcessor(RequestProcessor requestProcessor) {
        this.requestProcessor = requestProcessor;
    }

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }

    public RequestProcessor getRequestProcessor() {
        return requestProcessor;
    }

    public MessageFactory getMessageFactory() {
        return messageFactory;
    }
}
