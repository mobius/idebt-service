package com.dyggyty.idebt.wl;

import java.util.HashMap;
import java.util.Map;

/**
 * Back/Forward navigation for JSF
 * User: mobius
 * Date: 16.10.12
 * Time: 11:39
 */
public class NavigationBean {

    private NavigationBean previous;
    private String pageName;
    private volatile Map<String, Object> state;

    public Map<String, Object> getState() {
        if (state == null) {
            synchronized (this) {
                if (state == null) {
                    state = new HashMap<String, Object>();
                }
            }
        }
        return state;
    }

    public void next(String pageName) {

        NavigationBean navBean = null;
        NavigationBean currentBean = this;
        do {
            if (pageName.equals(currentBean.pageName)) {
                navBean = currentBean;
                break;
            } else {
                currentBean = currentBean.previous;
            }
        } while (currentBean != null);

        if (navBean == null) {
            NavigationBean navigationBean = new NavigationBean();
            navigationBean.pageName = this.pageName;
            navigationBean.previous = previous;
            navigationBean.state = state;

            this.pageName = pageName;
            previous = navigationBean;
            state = null;
        } else {
            previous = navBean.previous;
            this.pageName = navBean.pageName;
            state = navBean.state;
        }
    }

    public void back() {
        if (previous != null) {
            this.pageName = previous.pageName;
            this.state = previous.state;
            this.previous = previous.previous;
        } else {
            this.previous = null;
            this.pageName = null;
            this.state = null;
        }
    }

    public String getPageName() {
        return pageName;
    }
}
