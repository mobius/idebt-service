package com.dyggyty.idebt.wl;

import com.dyggyty.idebt.rest.response.ContactDetails;
import com.dyggyty.idebt.rest.response.GetCurrencyResponse;
import com.dyggyty.idebt.rest.response.TransactionDetail;

import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * User: vitaly.rudenya
 * Date: 08.10.12
 * Time: 10:28
 */
public class TransactionForm extends GenericForm {

    public static final String PAGE_ADD_TRANSACTION = "addtransaction";

    public static final String FIELD_TRANSACTION_ID = "transactionId";
    private static final String FIELD_TARGET_PERSON_ID = "targetPersonId";
    private static final String FIELD_AMOUNT = "amount";
    private static final String FIELD_CURRENCY_CODE = "currencyCode";
    private static final String FIELD_TRANSACTION_END_DATE = "transactionEndDate";
    private static final String FIELD_LEND_FLAG = "lendFlag";
    private static final String FIELD_ENABLE_TRANSACTION_DATE = "enableTransactionDate";

    public TransactionForm() {
        setValue(FIELD_TRANSACTION_END_DATE, new Date());
        setValue(FIELD_LEND_FLAG, Boolean.TRUE);
        setValue(FIELD_ENABLE_TRANSACTION_DATE, Boolean.TRUE);
    }

    public List<ContactDetails> getContacts() {
        HttpSession session = getSession();
        String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);
        return getRequestProcessor().getUserContacts(sessionId).getContactDetails();
    }

    public List<GetCurrencyResponse.CurrencyDetail> getCurrencies() {
        return getRequestProcessor().getCurrencies().getCurrencyDetails();
    }

    public Long getTransactionId() {
        return getLongValue(FIELD_TRANSACTION_ID);
    }

    public void setTransactionId(Long transactionId) {
        setValue(FIELD_TRANSACTION_ID, transactionId);
    }

    public Long getTargetPersonId() {
        return getLongValue(FIELD_TARGET_PERSON_ID);
    }

    public void setTargetPersonId(Long targetPersonId) {
        setValue(FIELD_TARGET_PERSON_ID, targetPersonId);
    }

    public Double getAmount() {
        return getDoubleValue(FIELD_AMOUNT);
    }

    public void setAmount(Double amount) {
        setValue(FIELD_AMOUNT, amount);
    }

    public String getCurrencyCode() {
        return getStringValue(FIELD_CURRENCY_CODE);
    }

    public void setCurrencyCode(String currencyCode) {
        setValue(FIELD_CURRENCY_CODE, currencyCode);
    }

    public Date getTransactionEndDate() {
        return getDateValue(FIELD_TRANSACTION_END_DATE);
    }

    public void setTransactionEndDate(Date transactionEndDate) {
        setValue(FIELD_TRANSACTION_END_DATE, transactionEndDate);
    }

    public Boolean getLendFlag() {
        return getBooleanValue(FIELD_LEND_FLAG);
    }

    public void setLendFlag(Boolean lendFlag) {
        setValue(FIELD_LEND_FLAG, lendFlag);
    }

    public Boolean getEnableTransactionDate() {
        return getBooleanValue(FIELD_ENABLE_TRANSACTION_DATE);
    }

    public String saveTransaction() {
        Double amount = getDoubleValue(FIELD_AMOUNT);
        Calendar transactionEndDate = Calendar.getInstance();
        transactionEndDate.setTime(getDateValue(FIELD_TRANSACTION_END_DATE));
        transactionEndDate.set(Calendar.HOUR_OF_DAY, 0);
        transactionEndDate.set(Calendar.MINUTE, 0);
        transactionEndDate.set(Calendar.SECOND, 0);
        transactionEndDate.set(Calendar.MILLISECOND, 0);

        Boolean lendFlag = getBooleanValue(FIELD_LEND_FLAG);

        if (amount < 0) {
            //todo show validation message
        }

        Calendar currentDate = Calendar.getInstance();
        currentDate.set(Calendar.HOUR_OF_DAY, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.SECOND, 0);
        currentDate.set(Calendar.MILLISECOND, 0);

        HttpSession session = getSession();
        String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);

        Double amountToSave = lendFlag ? amount : -amount;

        Long targetPersonId = getLongValue(FIELD_TARGET_PERSON_ID);
        String currencyCode = getStringValue(FIELD_CURRENCY_CODE);

        Long transactionId = getTransactionId();

        if (transactionId == null || transactionId == 0) {
            getRequestProcessor().addTransaction(sessionId, targetPersonId, amountToSave, currencyCode,
                    transactionEndDate.getTime());
        } else {
            getRequestProcessor().updateTransaction(sessionId, transactionId, targetPersonId, amountToSave,
                    currencyCode, transactionEndDate.getTime());
        }

        return back();
    }

    public String createByExample() {

        Map<String, String> parameters = getContext().getExternalContext().getRequestParameterMap();
        String amountStr = parameters.get(FIELD_AMOUNT);
        if (amountStr != null && amountStr.trim().length() > 0) {
            Double amount = Double.valueOf(amountStr);
            setAmount(amount);
        }

        String lendFlagStr = parameters.get(FIELD_LEND_FLAG);
        if (lendFlagStr != null && lendFlagStr.trim().length() > 0) {
            setLendFlag(Boolean.valueOf(lendFlagStr));
        }

        String currencyCode = parameters.get(FIELD_CURRENCY_CODE);
        if (currencyCode != null && currencyCode.trim().length() > 0) {
            setCurrencyCode(currencyCode);
        }

        String contactStr = parameters.get(FIELD_TARGET_PERSON_ID);
        if (contactStr != null && contactStr.trim().length() > 0) {
            setTargetPersonId(Long.valueOf(contactStr));
        }

        setValue(FIELD_ENABLE_TRANSACTION_DATE, Boolean.FALSE);

        return PAGE_ADD_TRANSACTION;
    }

    public String load() {
        Map<String, String> parameters = getContext().getExternalContext().getRequestParameterMap();
        String transactionIdStr = parameters.get(FIELD_TRANSACTION_ID);
        if (transactionIdStr == null || transactionIdStr.length() == 0) {
            //todo show validation message
            return back();
        } else {
            HttpSession session = getSession();
            String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);

            TransactionDetail transactionDetail = getRequestProcessor().
                    getUserTransaction(sessionId, Long.valueOf(transactionIdStr)).getTransactionDetails();

            if (transactionDetail != null) {
                Calendar transactionEndDate = Calendar.getInstance();
                transactionEndDate.setTime(transactionDetail.getFinalDate());

                setAmount(Math.abs(transactionDetail.getAmount()));
                setCurrencyCode(transactionDetail.getCurrencyCode());
                setLendFlag(transactionDetail.getAmount() > 0);
                setTargetPersonId(transactionDetail.getPersonContactId());
                setTransactionEndDate(transactionEndDate.getTime());
                setTransactionId(transactionDetail.getTransactionId());
            }
        }

        return PAGE_ADD_TRANSACTION;
    }

    @Override
    protected String getPageName() {
        return PAGE_ADD_TRANSACTION;
    }
}
