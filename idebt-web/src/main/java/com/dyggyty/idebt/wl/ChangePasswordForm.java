package com.dyggyty.idebt.wl;

import com.dyggyty.idebt.util.MessageFactory;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: mobius
 * Date: 22.10.12
 * Time: 15:51
 */
public class ChangePasswordForm extends GenericForm {

    private static final String PAGE_NAME = "changePassword";

    private String confirmationCode;
    private String oldPwd;
    private String newPwd;
    private String newPwdConfirm;

    public String getConfirmationCode() {
        if (confirmationCode == null) {
            synchronized (this) {
                if (confirmationCode == null) {
                    Map<String, String> parameters = getContext().getExternalContext().getRequestParameterMap();
                    confirmationCode = parameters.get(RequestParams.PARAM_CONFIRMATION_KEY);

                }
            }
        }
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public String getOldPwd() {
        return oldPwd;
    }

    public void setOldPwd(String oldPwd) {
        this.oldPwd = oldPwd;
    }

    public Boolean getShowOldPassword() {
        String confirmCode = getConfirmationCode();

        return confirmCode == null || confirmationCode.trim().length() == 0;
    }

    public String getNewPwd() {
        return newPwd;
    }

    public void setNewPwd(String newPwd) {
        this.newPwd = newPwd;
    }

    public String getNewPwdConfirm() {
        return newPwdConfirm;
    }

    public void setNewPwdConfirm(String newPwdConfirm) {
        this.newPwdConfirm = newPwdConfirm;
    }

    private boolean validatePassword() {
        boolean status = true;
        MessageFactory mf = new MessageFactory();
        FacesContext ctx = FacesContext.getCurrentInstance();
        if (newPwd.length() < 6 || newPwd.length() > 20) {
            ctx.addMessage("changePwdForm:newPwd", new FacesMessage(FacesMessage.SEVERITY_ERROR, mf.getMessage("errorPasswordLength"), null));
            status = false;
        }
        Pattern p1 = Pattern.compile("^[a-zA-Z0-9]+$");
        Matcher m1 = p1.matcher(newPwd);
        boolean matchFound1 = m1.matches();
        if (!matchFound1) {
            ctx.addMessage("changePwdForm:newPwd", new FacesMessage(FacesMessage.SEVERITY_ERROR, mf.getMessage("errorPassword"), null));
            status = false;
        }
        if (!newPwdConfirm.equals(newPwd)) {
            ctx.addMessage("changePwdForm:newPwdConfirm", new FacesMessage(FacesMessage.SEVERITY_ERROR, mf.getMessage("errorPasswordConfirm"), null));
            status = false;
        }
        return status;
    }

    public String changePassword() throws Exception {
        if (validatePassword()) {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) context.getExternalContext().getSession(false);
            Object sessionObj = session.getAttribute(RequestParams.SESSION_ID);
            String sessionId;
            if (sessionObj == null) {
                sessionId = null;
            } else {
                sessionId = sessionObj.toString();
            }

            getRequestProcessor().changeUserPassword(sessionId, oldPwd, newPwd, confirmationCode);
            return back();
        }
        return PAGE_NAME;
    }


    @Override
    protected String getPageName() {
        return PAGE_NAME;
    }
}
