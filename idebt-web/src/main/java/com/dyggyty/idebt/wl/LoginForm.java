package com.dyggyty.idebt.wl;

import com.dyggyty.idebt.rest.response.LoginUserResponse;
import com.dyggyty.idebt.utils.ErrorMessages;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class LoginForm extends GenericForm {

    public static final String PAGE_LOGIN = "login";

    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String validUser() throws Exception {
        String returnString = "success";

        LoginUserResponse loginUserResponse = getRequestProcessor().loginUser(userName, password);

        String messageKey = loginUserResponse.getErrorMessageKey();
        if (!ErrorMessages.STATUS_OK.equals(messageKey)) {
            return PAGE_LOGIN;
        } else {
            HttpSession session = getSession();
            session.setAttribute(RequestParams.SESSION_ID, loginUserResponse.getUserSessionId());
        }

        return returnString;
    }

    public String logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.invalidate();

        return "index";
    }

    @Override
    protected String getPageName() {
        return PAGE_LOGIN;
    }
}