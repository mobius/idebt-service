package com.dyggyty.idebt.wl;

import com.dyggyty.idebt.rest.response.ContactDetails;
import com.dyggyty.idebt.rest.response.GetDebtsResponse;
import com.dyggyty.idebt.rest.response.GetTransactionsResponse;
import com.dyggyty.idebt.rest.response.GetUserContactsResponse;
import com.dyggyty.idebt.rest.response.TransactionDetail;
import org.richfaces.event.ItemChangeEvent;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: mobius
 * Date: 07.10.12
 * Time: 13:23
 */
public class MainForm extends GenericForm {

    private static final String PAGE_NAME = "main";

    private static final String FIELD_ACTIVE_TAB = "activeTab";

    private volatile List<Debt> debts;
    private volatile List<Transaction> transactions;
    private volatile List<ContactDetails> contactDetails;
    private Long transactionToDelete;
    private Long contactToDelete;

    public Long getTransactionToDelete() {
        return transactionToDelete;
    }

    public void setTransactionToDelete(Long transactionToDelete) {
        this.transactionToDelete = transactionToDelete;
    }

    public Long getContactToDelete() {
        return contactToDelete;
    }

    public void setContactToDelete(Long contactToDelete) {
        this.contactToDelete = contactToDelete;
    }

    public String getActiveTab() {
        return getStringValue(FIELD_ACTIVE_TAB);
    }

    public void changeTabActiveItem(ItemChangeEvent event) {
        setValue(FIELD_ACTIVE_TAB, event.getNewItemName());
    }

    public List<Debt> getDebts() {
        if (debts == null) {
            synchronized (this) {
                if (debts == null) {
                    HttpSession session = getSession();
                    String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);

                    GetDebtsResponse debtsResponse = getRequestProcessor().getUserDebts(sessionId);
                    if (debtsResponse != null) {
                        List<GetDebtsResponse.DebtDetail> debtDetails = debtsResponse.getDebtDetails();
                        if (debtDetails != null) {
                            Map<Long, Debt> debtMap = new HashMap<Long, Debt>();
                            for (GetDebtsResponse.DebtDetail debtDetail : debtDetails) {
                                Long contactId = debtDetail.getTargetPersonId();
                                Debt debt = debtMap.get(contactId);
                                if (debt == null) {
                                    debt = new Debt();
                                    debt.setContactId(contactId);
                                    debt.setContactName(debtDetail.getTargetPerson());
                                    debtMap.put(contactId, debt);
                                }

                                List<Debt.DebtItem> debtItems = debt.getDebtItems();
                                if (debtItems == null) {
                                    debtItems = new ArrayList<Debt.DebtItem>();
                                    debt.setDebtItems(debtItems);
                                }

                                Debt.DebtItem debtItem = new Debt.DebtItem();
                                debtItem.setAmount(Math.abs(debtDetail.getAmount()));
                                debtItem.setCurrency(debtDetail.getCurrency());
                                debtItem.setDebtFlag(debtDetail.getAmount() < 0);
                                debtItem.setCurrencyCode(debtDetail.getCurrencyCode());
                                //todo setup end date

                                debtItems.add(debtItem);
                            }
                            debts = new ArrayList<Debt>(debtMap.values());
                        }
                    }
                }
            }
        }

        return debts;
    }

    public Integer getDebtsSize() {
        return getDebts() == null ? 0 : getDebts().size();
    }

    public String deleteTransaction() {
        Map<String, String> parameters = getContext().getExternalContext().getRequestParameterMap();
        String transactionIdStr = parameters.get(TransactionForm.FIELD_TRANSACTION_ID);
        if (transactionIdStr == null || transactionIdStr.length() == 0) {
            //todo show validation message
        } else {
            HttpSession session = getSession();
            String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);
            getRequestProcessor().deleteTransaction(sessionId, Long.valueOf(transactionIdStr));

            transactions = null;
            debts = null;
            //todo check operation
        }

        return PAGE_NAME;
    }

    public String deleteContact() {
        Map<String, String> parameters = getContext().getExternalContext().getRequestParameterMap();
        String contactIdStr = parameters.get(PersonForm.FIELD_PERSON_ID);
        if (contactIdStr == null || contactIdStr.length() == 0) {
            //todo show validation message
        } else {
            HttpSession session = getSession();
            String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);
            getRequestProcessor().deleteUserContact(sessionId, Long.valueOf(contactIdStr));

            contactDetails = null;
            //todo check operation
        }

        return PAGE_NAME;
    }

    public List<Transaction> getTransactions() {
        if (transactions == null) {
            synchronized (this) {
                if (transactions == null) {
                    HttpSession session = getSession();
                    String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);

                    GetTransactionsResponse transactionsResponse = getRequestProcessor().getUserTransactions(sessionId);
                    if (transactionsResponse != null) {
                        List<TransactionDetail> transactionDetails = transactionsResponse.getTransactionDetails();
                        if (transactionDetails != null && transactionDetails.size() > 0) {
                            transactions = new ArrayList<Transaction>();
                            for (TransactionDetail transactionDetail : transactionDetails) {
                                Transaction transaction = new Transaction();

                                Double amount = transactionDetail.getAmount();

                                transaction.setTransactionId(transactionDetail.getTransactionId());
                                transaction.setEndDate(transactionDetail.getFinalDate());
                                transaction.setPersonContact(transactionDetail.getPersonContact());
                                transaction.setPersonContactId(transactionDetail.getPersonContactId());
                                transaction.setAmount(Math.abs(amount));
                                transaction.setCreated(transactionDetail.getCreated());
                                transaction.setVerified(transactionDetail.getVerified());
                                transaction.setCurrency(transactionDetail.getCurrency());
                                transaction.setDebtFlag(amount < 0);
                                transaction.setShowEdit(transactionDetail.getOwnerId() == null);

                                transactions.add(transaction);
                            }
                        }
                    }
                }
            }
        }

        return transactions;
    }

    public Integer getTransactionSize() {
        return getTransactions() == null ? 0 : getTransactions().size();
    }

    public List<ContactDetails> getContactDetails() {
        if (contactDetails == null) {
            synchronized (this) {
                if (contactDetails == null) {
                    HttpSession session = getSession();
                    String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);

                    GetUserContactsResponse transactionResponse = getRequestProcessor().getUserContacts(sessionId);
                    contactDetails = transactionResponse.getContactDetails();
                }
            }
        }

        return contactDetails;
    }

    public Integer getContactDetailsSize() {
        return getContactDetails().size();
    }

    @Override
    protected String getPageName() {
        return PAGE_NAME;
    }

    public static class Transaction {
        private Long transactionId;
        private String personContact;
        private Long personContactId;
        private Double amount;
        private Date created;
        private Date endDate;
        private Boolean verified;
        private String currency;
        private String currencyCode;
        private Boolean debtFlag;
        private Boolean showEdit;

        public Long getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(Long transactionId) {
            this.transactionId = transactionId;
        }

        public String getPersonContact() {
            return personContact;
        }

        public void setPersonContact(String personContact) {
            this.personContact = personContact;
        }

        public Long getPersonContactId() {
            return personContactId;
        }

        public void setPersonContactId(Long personContactId) {
            this.personContactId = personContactId;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        public Boolean getVerified() {
            return verified;
        }

        public void setVerified(Boolean verified) {
            this.verified = verified;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public Boolean getDebtFlag() {
            return debtFlag;
        }

        public void setDebtFlag(Boolean debtFlag) {
            this.debtFlag = debtFlag;
        }

        public Boolean getShowEdit() {
            return showEdit;
        }

        public void setShowEdit(Boolean showEdit) {
            this.showEdit = showEdit;
        }
    }

    public static class Debt {
        private String contactName;
        private Long contactId;
        private List<DebtItem> debtItems;

        public String getContactName() {
            return contactName;
        }

        public void setContactName(String contactName) {
            this.contactName = contactName;
        }

        public Long getContactId() {
            return contactId;
        }

        public void setContactId(Long contactId) {
            this.contactId = contactId;
        }

        public List<DebtItem> getDebtItems() {
            return debtItems;
        }

        public void setDebtItems(List<DebtItem> debtItems) {
            this.debtItems = debtItems;
        }

        public static class DebtItem {
            private Double amount;
            private String currency;
            private String currencyCode;
            private Boolean debtFlag;

            public Boolean getDebtFlag() {
                return debtFlag;
            }

            public void setDebtFlag(Boolean debtFlag) {
                this.debtFlag = debtFlag;
            }

            public Double getAmount() {
                return amount;
            }

            public void setAmount(Double amount) {
                this.amount = amount;
            }

            public String getCurrency() {
                return currency;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public String getCurrencyCode() {
                return currencyCode;
            }

            public void setCurrencyCode(String currencyCode) {
                this.currencyCode = currencyCode;
            }
        }
    }
}
