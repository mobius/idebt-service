package com.dyggyty.idebt.wl;

/**
 * User: vitaly.rudenya
 * Date: 24.10.12
 * Time: 11:03
 */
public class ForgotPasswordForm extends GenericForm {

    private static final String PAGE_NAME = "forgotPassword";

    private String email;
    private String userName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String sendPassword() {
        getRequestProcessor().forgotPassword(userName, email);
        return back();
    }

    @Override
    protected String getPageName() {
        return PAGE_NAME;
    }
}
