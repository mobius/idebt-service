package com.dyggyty.idebt.wl;

import com.dyggyty.idebt.rest.response.ContactDetails;
import com.dyggyty.idebt.rest.response.GetUserContactResponse;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * User: vitaly.rudenya
 * Date: 10.10.12
 * Time: 11:00
 */
public class PersonForm extends GenericForm {

    private static final String PAGE_ADD_PERSON = "persondetails";
    private static final String PARAM_PERSON_ID = "personId";

    public static final String FIELD_PERSON_ID = "personId";
    private static final String FIELD_PERSON_NAME = "personName";
    private static final String FIELD_PERSON_EMAIL = "personEmail";
    private static final String FIELD_PERSON_PHONE = "personPhone";

    public String loadPersonContact() {

        Map<String, String> parameters = getContext().getExternalContext().getRequestParameterMap();
        String personStr = parameters.get(PARAM_PERSON_ID);
        if (personStr != null) {
            Long personId = Long.valueOf(personStr);

            HttpSession session = getSession();
            String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);
            GetUserContactResponse contactResponse = getRequestProcessor().getUserContact(sessionId, personId);

            if (contactResponse != null && contactResponse.getContactDetails() != null) {
                ContactDetails contactDetails = contactResponse.getContactDetails();
                setValue(FIELD_PERSON_ID, contactDetails.getId());
                setValue(FIELD_PERSON_NAME, contactDetails.getName());

                List<String> emails = contactDetails.getEmails();
                if (emails != null && emails.size() > 0) {
                    setValue(FIELD_PERSON_EMAIL, emails.get(0));
                }

                List<String> phones = contactDetails.getPhones();
                if (phones != null && phones.size() > 0) {
                    setValue(FIELD_PERSON_PHONE, phones.get(0));
                }
            }
        }

        return PAGE_ADD_PERSON;
    }


    public Long getPersonId() {
        return getLongValue(FIELD_PERSON_ID);
    }

    public void setPersonId(Long personId) {
        setValue(FIELD_PERSON_ID, personId);
    }

    public String getPersonEmail() {
        return getStringValue(FIELD_PERSON_EMAIL);
    }

    public void setPersonEmail(String personEmail) {
        setValue(FIELD_PERSON_EMAIL, personEmail);
    }

    public String getPersonPhone() {
        return getStringValue(FIELD_PERSON_PHONE);
    }

    public void setPersonPhone(String personPhone) {
        setValue(FIELD_PERSON_PHONE, personPhone);
    }

    public String getPersonName() {
        return getStringValue(FIELD_PERSON_NAME);
    }

    public void setPersonName(String personName) {
        setValue(FIELD_PERSON_NAME, personName);
    }

    public String savePerson() {

        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        String sessionId = (String) session.getAttribute(RequestParams.SESSION_ID);

        Long personId = getLongValue(FIELD_PERSON_ID);
        String personName = getStringValue(FIELD_PERSON_NAME);
        String personEmail = getStringValue(FIELD_PERSON_EMAIL);
        String personPhone = getStringValue(FIELD_PERSON_PHONE);

        getRequestProcessor().saveUserContact(sessionId, personId == null || personId == 0 ? null : personId,
                personName, personEmail, personPhone);

        return back();
    }

    @Override
    protected String getPageName() {
        return PAGE_ADD_PERSON;
    }
}
