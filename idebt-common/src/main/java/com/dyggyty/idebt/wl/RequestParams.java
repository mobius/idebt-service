package com.dyggyty.idebt.wl;

import java.text.SimpleDateFormat;

/**
 * User: mobius
 * Date: 05.10.12
 * Time: 13:31
 */
public interface RequestParams {

    public static final SimpleDateFormat APP_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public static final String USER_LOGIN = "%s/user/login/";
    public static final String USER_CHECK_EXISTS = "%s/user/check/";
    public static final String USER_REGISTER = "%s/user/register/";
    public static final String DEBT_GET_ALL = "%s/debt/getall/%s/";
    public static final String TRANSACTION_GET_ALL = "%s/transaction/getall/%s/";
    public static final String TRANSACTION_GET = "%s/transaction/get/%s/";
    public static final String TRANSACTION_ADD = "%s/transaction/add/%s/";
    public static final String TRANSACTION_UPDATE = "%s/transaction/update/%s/";
    public static final String TRANSACTION_DELETE = "%s/transaction/delete/%s/";
    public static final String USER_CONTACTS = "%s/user/usercontacts/%s/";
    public static final String USER_CONTACT = "%s/user/usercontact/%s/";
    public static final String CURRENCY_GET_ALL = "%s/currency/getall/";
    public static final String USER_SAVE_USER_CONTACTS = "%s/user/saveusercontact/%s/";
    public static final String USER_DELETE_USER_CONTACT = "%s/user/deleteusercontact/%s/";
    public static final String USER_GET_PERSONAL_INFO = "%s/user/getpersonalinfo/%s/";
    public static final String USER_PERSONAL_INFO = "%s/user/personalinfo/%s/";
    public static final String USER_FORGOT_PASSWORD = "%s/user/forgotpassword/";
    public static final String USER_CHANGE_PASSWORD = "%s/user/changepassword/%s/";

    public static final String REQUEST_PERSON_CONTACT_EMAIL = "personContactEmail";
    public static final String REQUEST_PERSON_CONTACT_PHONE = "personContactPhone";
    public static final String REQUEST_PERSON_CONTACT_ID = "personContactId";
    public static final String REQUEST_PERSON_CONTACT_NAME = "personContactName";
    public static final String REQUEST_PERSON_CONTACT_EXT_ID = "personContactExtId";
    public static final String REQUEST_AMOUNT = "amount";
    public static final String REQUEST_CURRENCY_CODE = "currencyCode";
    public static final String REQUEST_COMMENT = "comment";
    public static final String REQUEST_FINAL_DATE = "finalDate";
    public static final String REQUEST_TRANSACTION_ID = "transactionId";

    public static final String REQUEST_PASSWORD = "userPassword";
    public static final String REQUEST_NEW_PASSWORD = "userNewPassword";
    public static final String REQUEST_USER_EMAIL = "userEmail";
    public static final String REQUEST_NICK_NAME = "nickName";
    public static final String REQUEST_PHONE = "phone";
    public static final String SESSION_ID = "sessionId";

    public static final String PARAM_CONFIRMATION_KEY = "confirmationKey";
}
