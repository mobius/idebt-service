package com.dyggyty.idebt.utils;

/**
 * User: mobius
 * Date: 13.09.12
 * Time: 12:15
 */
public class MessageException extends RuntimeException {

    public MessageException(String message) {
        super(message);
    }
}
