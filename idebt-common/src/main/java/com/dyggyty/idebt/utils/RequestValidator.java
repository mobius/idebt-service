package com.dyggyty.idebt.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: mobius
 * Date: 22.10.12
 * Time: 13:31
 */
public class RequestValidator {

    private static final Pattern EMAIL_PATTERN = Pattern.compile(".+@.+\\.[a-z]+");
    private static final Pattern NICK_NAME_PATTERN = Pattern.compile("[\\w\\p{Blank}]+");
    private static final Pattern PHONE_PATTER = Pattern.compile("^(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*$");

    private RequestValidator() {
    }

    public static void validateLoginUser(String userEmail, String password) {
        validateNotEmpty(userEmail, ErrorMessages.VALIDATION_EMPTY_EMAIL);
        validateNotEmpty(password, ErrorMessages.VALIDATION_EMPTY_USER_PASSWORD);
    }

    public static void validateCheckUserExists(String userEmail) {
        validateNotEmpty(userEmail, ErrorMessages.VALIDATION_EMPTY_EMAIL);
        validateEmail(userEmail);
    }

    public static void validateGetUserDebts(String userSession) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
    }

    public static void validateGetUserTransactions(String userSession) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
    }

    public static void validateGetUserTransaction(String userSession, Long transactionId) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
        validateNotEmpty(transactionId, ErrorMessages.VALIDATION_EMPTY_TRANSACTION_ID);
    }

    public static void validateGetUserContacts(String userSession) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
    }

    public static void validateGetUserContact(String userSession, Long contactId) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
        validateNotEmpty(contactId, ErrorMessages.VALIDATION_EMPTY_CONTACT_PERSON);
    }

    public static void validateSaveUserContact(String userSession, String personName,
                                               String personEmail, String personPhone, String externalId) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
        validateNotEmpty(personName, ErrorMessages.VALIDATION_EMPTY_NICK_NAME);

        if (personEmail != null && personEmail.trim().length() > 0) {
            validateEmail(personEmail);
        }

        if (personPhone != null && personPhone.trim().length() > 0) {
            validatePhone(personPhone);
        }

        if (externalId != null && externalId.length() > 64) {
            throw new MessageException(ErrorMessages.VALIDATION_CONTACT_EXTERNAL_ID_TOO_LONG);
        }
    }

    public static void validateDeleteUserContact(String userSession, Long contactId) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
        validateNotEmpty(contactId, ErrorMessages.VALIDATION_EMPTY_CONTACT_PERSON);
    }

    public static void validateGetUserInfo(String userSession) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
    }

    public static void validateUpdateUserInfo(String userSession, String nickName, String userEmail, String userPhone) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
        validateNotEmpty(nickName, ErrorMessages.VALIDATION_EMPTY_NICK_NAME);
        validateNotEmpty(userEmail, ErrorMessages.VALIDATION_EMPTY_EMAIL);
        //validateNotEmpty(userPhone, ErrorMessages.VALIDATION_EMPTY_USER_PHONE);

        validateEmail(userEmail);
        validatePhone(userPhone);
    }

    public static void validateChangeUserPassword(String userSession, String password, String newPassword,
                                                  String confirmationKey) {
        validateNotEmpty(newPassword, ErrorMessages.VALIDATION_EMPTY_USER_NEW_PASSWORD);
        if (userSession == null || userSession.trim().length() == 0) {
            validateNotEmpty(confirmationKey, ErrorMessages.VALIDATION_EMPTY_CONFIRMATION_KEY);
        } else {
            validateNotEmpty(password, ErrorMessages.VALIDATION_EMPTY_USER_PASSWORD);
        }
    }

    public static void validateDeleteTransaction(String userSession, Long transactionId) {
        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);
        validateNotEmpty(transactionId, ErrorMessages.VALIDATION_EMPTY_TRANSACTION_ID);
    }

    public static void validateRegisterUser(String email, String nickName, String password, String phone) {
        validateNotEmpty(nickName, ErrorMessages.VALIDATION_EMPTY_NICK_NAME);
        validateNotEmpty(password, ErrorMessages.VALIDATION_EMPTY_USER_PASSWORD);
        validateNotEmpty(email, ErrorMessages.VALIDATION_EMPTY_EMAIL);

        validateEmail(email);
        validatePhone(phone);
    }

    public static void validateUpdateTransaction(String userSession, Long transactionId, Long targetContactId,
                                                 Double amount, String currencyCode, Date finalDate, String comment) {

        validateNotEmpty(transactionId, ErrorMessages.VALIDATION_EMPTY_TRANSACTION_ID);
        validateAddTransaction(userSession, targetContactId, amount, currencyCode, finalDate, comment);
    }

    public static void validateAddTransaction(String userSession, Long targetContactId, Double amount,
                                              String currencyCode, Date finalDate, String comment) {

        validateNotEmpty(userSession, ErrorMessages.VALIDATION_EMPTY_USER_SESSION);

        if (targetContactId == null) {
            throw new MessageException(ErrorMessages.VALIDATION_EMPTY_CONTACT_PERSON);
        }

        if (amount == null) {
            throw new MessageException(ErrorMessages.VALIDATION_EMPTY_AMOUNT);
        }

        if (amount == 0d) {
            throw new MessageException(ErrorMessages.VALIDATION_ZERO_AMOUNT);
        }

        if (currencyCode == null || currencyCode.trim().length() == 0) {
            throw new MessageException(ErrorMessages.VALIDATION_EMPTY_CURRENCY_CODE);
        }

        if (finalDate == null) {
            throw new MessageException(ErrorMessages.VALIDATION_EMPTY_TERMINATION_DATE);
        }

        Calendar currentDate = Calendar.getInstance();
        currentDate.set(Calendar.MILLISECOND, 0);
        currentDate.set(Calendar.SECOND, 0);
        currentDate.set(Calendar.MINUTE, 0);
        currentDate.set(Calendar.HOUR, 0);

        Calendar finalDateCal = Calendar.getInstance();
        finalDateCal.setTime(finalDate);
        finalDateCal.add(Calendar.DAY_OF_MONTH, 1);
        finalDateCal.set(Calendar.MILLISECOND, 0);
        finalDateCal.set(Calendar.SECOND, 0);
        finalDateCal.set(Calendar.MINUTE, 0);
        finalDateCal.set(Calendar.HOUR, 0);

/*
        if (currentDate.getTime().after(finalDateCal.getTime())) {
            throw new MessageException(ErrorMessages.VALIDATION_TERMINATION_DATE_SHOULD_NOT_BE_BEFORE);
        }
*/

        if (comment != null && comment.length() > 400) {
            throw new MessageException(ErrorMessages.VALIDATION_TRANSACTION_COMMENT_SIZE);
        }
    }

    public static void validateForgotPassword(String userEmail) {
        validateNotEmpty(userEmail, ErrorMessages.VALIDATION_EMPTY_EMAIL);
        validateEmail(userEmail);
    }

    private static void validateNotEmpty(String arg, String messageKey) {
        if (arg == null || arg.trim().length() == 0) {
            throw new MessageException(messageKey);
        }
    }

    private static void validateNotEmpty(Object obj, String messageKey) {
        if (obj == null) {
            throw new MessageException(messageKey);
        }
    }

    private static void validateEmail(String email) {
        Matcher emailMatcher = EMAIL_PATTERN.matcher(email);
        if (!emailMatcher.matches()) {
            throw new MessageException(ErrorMessages.VALIDATION_INVALID_EMAIL_FORMAT);
        }
    }

    private static void validatePhone(String phone) {
        if (phone != null && phone.trim().length() > 0) {
            Matcher phoneMatcher = PHONE_PATTER.matcher(phone);
            if (!phoneMatcher.matches()) {
                throw new MessageException(ErrorMessages.VALIDATION_INVALID_PHONE_FORMAT);
            }
        }
    }
}
