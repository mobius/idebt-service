package com.dyggyty.idebt.utils;

/**
 * User: vitaly.rudenya
 * Date: 15.11.12
 * Time: 11:00
 */
public class ApplicationInternalException extends RuntimeException {

    public ApplicationInternalException() {
        super();
    }

    public ApplicationInternalException(Throwable cause) {
        super(cause);
    }
}
