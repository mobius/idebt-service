package com.dyggyty.idebt.utils;

/**
 * User: mobius
 * Date: 16.08.12
 * Time: 12:34
 */
public interface ErrorMessages {
    public static final String STATUS_OK = "status.ok";
    public static final String USER_NOT_FOUND = "user.not.found";
    public static final String CONTACT_NOT_FOUND = "contact.not.found";
    public static final String USER_ALREADY_EXISTS = "user.already.exists";
    public static final String INVALID_USER_LOGIN_AND_EMAIL = "user.invalid.login.and.email";
    public static final String INVALID_USER_PASSWORD = "user.invalid.password";
    public static final String USER_CONTACT_IS_MISSING = "user.contact.missing";
    public static final String USER_TRANSACTION_IS_MISSING = "user.transaction.missing";
    public static final String CURRENCY_IS_MISSING = "currency.missing";
    public static final String INVALID_CONFIRMATION_CODE = "confirmation.code.invalid";
    public static final String SERVER_COMMUNICATION_ERROR = "server.communication.error";

    public static final String VALIDATION_EMPTY_TRANSACTION_ID = "validation.empty.transaction.id";
    public static final String VALIDATION_EMPTY_USER_SESSION = "validation.empty.user.session";
    public static final String VALIDATION_EMPTY_USER_NEW_PASSWORD = "validation.empty.user.new_password";
    public static final String VALIDATION_EMPTY_CONTACT_PERSON = "validation.empty.contact.person";
    public static final String VALIDATION_EMPTY_AMOUNT = "validation.empty.amount";
    public static final String VALIDATION_EMPTY_CONFIRMATION_KEY = "validation.empty.confirmation_key";
    public static final String VALIDATION_ZERO_AMOUNT = "validation.zero.amount";
    public static final String VALIDATION_EMPTY_CURRENCY_CODE = "validation.empty.currency.code";
    public static final String VALIDATION_EMPTY_TERMINATION_DATE = "validation.empty.termination.date";
    public static final String VALIDATION_EMPTY_NICK_NAME = "validation.empty.nick_name";
    public static final String VALIDATION_CONTACT_EXTERNAL_ID_TOO_LONG = "validation.contact.external.id.too.long";
    public static final String VALIDATION_EMPTY_EMAIL = "validation.empty.email";
    public static final String VALIDATION_EMPTY_USER_PASSWORD = "validation.empty.user.password";
    public static final String VALIDATION_EMPTY_USER_PHONE = "validation.empty.user.phone";
    public static final String VALIDATION_TERMINATION_DATE_SHOULD_NOT_BE_BEFORE = "validation.termination.date.is.before";
    public static final String VALIDATION_INVALID_EMAIL_FORMAT = "validation.invalid.email";
    public static final String VALIDATION_INVALID_USER_NAME_FORMAT = "validation.invalid.user_name";
    public static final String VALIDATION_INVALID_PHONE_FORMAT = "validation.invalid.phone";
    public static final String VALIDATION_TRANSACTION_COMMENT_SIZE = "validation.transaction.comment.size";

    public static final String VALIDATE_PERSON_CONTACT_NOT_NUMBER = "validate.personContactId.not.number";
    public static final String VALIDATE_TRANSACTION_ID_NOT_NUMBER = "validate.transactionId.not.number";
    public static final String VALIDATE_AMOUNT_NOT_NUMBER = "validate.amount.not.number";
    public static final String VALIDATE_FINAL_DATE_NOT_DATE = "validate.finalDate.not.date";
}
