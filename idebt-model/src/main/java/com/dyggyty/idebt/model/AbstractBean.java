package com.dyggyty.idebt.model;

import java.io.Serializable;

/**
 * User: mobius
 * Date: 06.06.12
 * Time: 12:36
 */
public abstract class AbstractBean<ID extends Serializable> {

    private ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }
}
