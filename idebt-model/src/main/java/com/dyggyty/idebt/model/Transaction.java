package com.dyggyty.idebt.model;

import java.util.Date;

/**
 * User: mobius
 * Date: 03.06.12
 * Time: 4:42
 */
public class Transaction extends AbstractBean<Long> {
    private User owner;
    private PersonContact personContact;
    private Double amount;
    private Date created;
    private Date finalDate;
    private Boolean verified;
    private String confirmationCode;
    private Date closed;
    private Currency currency;
    private String comment;

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public PersonContact getPersonContact() {
        return personContact;
    }

    public void setPersonContact(PersonContact personContact) {
        this.personContact = personContact;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    public Date getClosed() {
        return closed;
    }

    public void setClosed(Date closed) {
        this.closed = closed;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
