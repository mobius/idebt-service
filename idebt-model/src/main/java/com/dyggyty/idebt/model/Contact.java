package com.dyggyty.idebt.model;

/**
 * User: 1
 * Date: 22.07.12
 * Time: 20:04
 */
public abstract class Contact extends AbstractBean<Long> {

    private String contactInfo;
    private Boolean verified;
    private Person person;
    private String confirmationCode;

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public void setConfirmationCode(String confirmationCode) {
        this.confirmationCode = confirmationCode;
    }
}
