package com.dyggyty.idebt.model;

import java.util.Set;

/**
 * User: mobius
 * Date: 03.06.12
 * Time: 4:48
 */
public abstract class Person extends AbstractBean<Long> {
    private String name;
    private Boolean deleted = Boolean.FALSE;
    private Set<Phone> phones;
    private Set<Email> emails;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Set<Email> getEmails() {
        return emails;
    }

    public void setEmails(Set<Email> emails) {
        this.emails = emails;
    }
}
