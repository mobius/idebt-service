package com.dyggyty.idebt.model;

/**
 * User: mobius
 * Date: 03.06.12
 * Time: 9:54
 */
public class Currency extends AbstractBean<String> {
    private String titleKey;

    public String getTitleKey() {
        return titleKey;
    }

    public void setTitleKey(String titleKey) {
        this.titleKey = titleKey;
    }
}
