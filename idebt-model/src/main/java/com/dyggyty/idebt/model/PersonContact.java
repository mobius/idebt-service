package com.dyggyty.idebt.model;

/**
 * User: mobius
 * Date: 20.10.12
 * Time: 11:50
 */
public class PersonContact extends Person{

    private User owner;
    private User linkedUser;
    private String externalId;

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public User getLinkedUser() {
        return linkedUser;
    }

    public void setLinkedUser(User linkedUser) {
        this.linkedUser = linkedUser;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}
