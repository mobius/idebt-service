package com.dyggyty.idebt.model;

/**
 * User: 1
 * Date: 22.07.12
 * Time: 20:25
 */
public class Debt extends AbstractBean<Long> {
    private PersonContact personContact;
    private Double amount;
    private Currency currency;
    private User owner;

    public PersonContact getPersonContact() {
        return personContact;
    }

    public void setPersonContact(PersonContact personContact) {
        this.personContact = personContact;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
